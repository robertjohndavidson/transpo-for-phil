const MajorStationsId = 23100015;
const MajorTowersId = 23100008;
const DomesticCoordinate = 1;
const TransborderCoordinate = 2;
const OtherIntCoordinate = 3;
const Stations = "stations";
const Towers = "towers";

const proxy = "https://cors-anywhere.herokuapp.com/";
const webAPI = "https://www150.statcan.gc.ca/t1/wds/rest/getDataFromCubePidCoordAndLatestNPeriods";
let returnObject = {};
let stationsData = {};
let towersData = {};


const numToGeographyMajorStations = {
  "2": "YBR",
  "3": "YBL",
  "4": "YCG",
  "5": "YYG",
  "6": "YYQ",
  "7": "YXC",
  "9": "YDF",
  "11": "YYE",
  "12": "YXJ",
  "14": "YQU",
  "15": "YOJ",
  "16": "YGR",
  "17": "YEV",
  "18": "YFB",
  "19": "YKA",
  "20": "YQK",
  "21": "YGK",
  "22": "YVP",
  "24": "YGL",
  "25": "YVC",
  "26": "YQL",
  "27": "YLL",
  "28": "YXH",
  "29": "YYY",
  "30": "YCD",
  "33": "YND",
  "34": "YPE",
  "35": "YYF",
  "36": "YZT",
  "37": "YPA",
  "39": "YRT",
  "40": "YQF",
  "42": "YUY",
  "43": "YSJ",
  "45": "YXL",
  "46": "YYD",
  "48": "YCM",
  "50": "YXT",
  "51": "YTH",
  "52": "YTS",
  "53": "YVO",
  "54": "YWH",
  "55": "YWK",
  "56": "YZU",
  "57": "YWL"
};

const numToGeographyMajorTowers = {
  "2": "YXX",
  "3": "YDT",
  "4": "YYC",
  "5": "1YSB",
  "6": "YRC",
  "8": "YEG",
  "9": "CZVL",
  "46": "YMM",
  "47": "YFC",
  "10": "YQX",
  "11": "YHZ",
  "12": "YHM",
  "13": "YLW",
  "14": "YKF",
  "15": "YLY",
  "16": "YXU",
  "17": "YQM",
  "19": "YUL",
  "20": "YHU",
  "21": "YOO",
  "22": "YOW",
  "23": "YPK",
  "24": "YXS",
  "25": "YQB",
  "26": "YQR",
  "27": "YXE",
  "28": "YAM",
  "29": "YYT",
  "30": "YJN",
  "32": "YQT",
  "34": "YTZ",
  "35": "YYZ",
  "36": "CXH",
  "37": "YVR",
  "38": "YYJ",
  "39": "YXY",
  "40": "YQG",
  "41": "YWG",
  "42": "2DCI",
  "43": "YZF"
};

const numToIndicator = {
  1: "domestic",
  2: "transborder",
  3: "international"
};

const statusCodes = {
  1: "..",
  2: "0s",
  3: "A",
  4: "B",
  5: "C",
  6: "D",
  7: "E",
  8: "F"
};

const qi_F = 8;

export default function(selectedDateRange) {
  return new Promise((resolve, reject) => {
    apiCall(Stations, selectedDateRange).then(function(majorData) {
      resolve(majorData);
    });
  });
}

function apiCall(product, selectedDateRange) {
  return new Promise((resolve, reject) => {
    // get coordinates for data
    let productID;
    let coordinateArray = [];
    const majorCallData = [];


    if (product === Stations) {
      productID = MajorStationsId;
      coordinateArray = coordinateTranslate("stations");
    } else {
      productID = MajorTowersId;
      coordinateArray = coordinateTranslate("towers");
    }

    for (let i =0; i< coordinateArray.length; i++ ) {
      majorCallData.push({"productId": productID, "coordinate": coordinateArray[i], "latestN": selectedDateRange.numPeriods});
    }

    $.support.cors = true;
    $.ajax({
      type: "post",
      url: webAPI,
      data: JSON.stringify(majorCallData),
      dataType: "json",
      contentType: "application/json",
      success: function(returnData, textStatus, jQxhr) {
        // do call for stations first then towers, after towers is done return to the application

        if (product === Stations) {
          stationsData = rebuild(returnData, selectedDateRange, product);
          apiCall(Towers, selectedDateRange).then(function(completeData) {
            resolve(completeData);
          });
        } else {
          towersData = rebuild(returnData, selectedDateRange, product);
          returnObject = {...stationsData, ...towersData};
          resolve(returnObject);
        }
      },
      error: function(jqXhr, textStatus, errorThrown) {
        reject(errorThrown);
      }
    });
  });
}

function rebuild(data, selectedDateRange, product) {
  const dataByAirport = {};
  const airportObject = {};
  let returnObject = {};

  for (let i = 0; i < data.length; i++) {
    const code = data[i].object.coordinate.split(".", 2)[1];
    if (product == Towers) {
      if (!dataByAirport.hasOwnProperty(numToGeographyMajorTowers[code])) {
        dataByAirport[numToGeographyMajorTowers[code]] = [];
      }
      dataByAirport[numToGeographyMajorTowers[code]].push(data[i]);
    } else {
      if (!dataByAirport.hasOwnProperty(numToGeographyMajorStations[code])) {
        dataByAirport[numToGeographyMajorStations[code]] = [];
      }
      dataByAirport[numToGeographyMajorStations[code]].push(data[i]);
    }
  }
  for (const airport in dataByAirport) {
    if (Object.prototype.hasOwnProperty.call(dataByAirport, airport)) {
      const rebuiltData = rebuildData(dataByAirport[airport], selectedDateRange);
      const dateOrganizedArray = [];
      for (const entry in rebuiltData) {
        rebuiltData[entry].date = entry;
        dateOrganizedArray.push(rebuiltData[entry]);
      }
      airportObject[airport] = dateOrganizedArray;
    }
  }
  returnObject = airportObject;
  return returnObject;
}

function rebuildData(data, selectedDateRange) {
  const returnObject = {};
  for (let i = 0; i < selectedDateRange.numPeriods; i++) {
    for (const j in data) {
      const datapoint = data[j].object.vectorDataPoint[i];
      const date = datapoint.refPer.substring(0, 7);
      if (!returnObject.hasOwnProperty(date)) {
        returnObject[date] = {};
      }

      const indicator = data[j].object.coordinate.split(".", 3)[2];
      if (datapoint.value == null) {
        returnObject[date][numToIndicator[indicator]] = "x";
      } else if (datapoint.statusCode != 1 && datapoint.securityLevelCode == 0 && datapoint.statusCode != qi_F) {
        returnObject[date][numToIndicator[indicator]] = datapoint.value;
      } else {
        returnObject[date][numToIndicator[indicator]] = statusCodes[datapoint.statusCode];
      }
    }
  }
  return returnObject;
}

function coordinateTranslate(product) {
  const returnArray = [];
  if (product == "towers") {
    for (const i in numToGeographyMajorTowers) {
      returnArray.push(`1.${i}.${DomesticCoordinate}.1.0.0.0.0.0.0`);
      returnArray.push(`1.${i}.${TransborderCoordinate}.1.0.0.0.0.0.0`);
      returnArray.push(`1.${i}.${OtherIntCoordinate}.1.0.0.0.0.0.0`);
    }
  } else {
    for (const i in numToGeographyMajorStations) {
      returnArray.push(`1.${i}.${DomesticCoordinate}.1.0.0.0.0.0.0`);
      returnArray.push(`1.${i}.${TransborderCoordinate}.1.0.0.0.0.0.0`);
      returnArray.push(`1.${i}.${OtherIntCoordinate}.1.0.0.0.0.0.0`);
    }
  }
  return returnArray;
}
