import passengerApiCall from "./air_passenger_api.js";
import majorApiCall from "./air_major_api.js";
const PassengerData = "passengers";
const MajorAirData = "major_airports";

export default function(selectedDateRange, selectedDataset) {
  return new Promise((resolve, reject) => {
    if (selectedDataset === PassengerData) {
      passengerApiCall(selectedDateRange).then(function(newData) {
        resolve(newData);
      });
    } else {
      majorApiCall(selectedDateRange).then(function(newData) {
        resolve(newData);
      });
    }
  });
}
