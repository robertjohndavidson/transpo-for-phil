(function () {
  'use strict';

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  function _defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, descriptor.key, descriptor);
    }
  }

  function _createClass(Constructor, protoProps, staticProps) {
    if (protoProps) _defineProperties(Constructor.prototype, protoProps);
    if (staticProps) _defineProperties(Constructor, staticProps);
    return Constructor;
  }

  function _defineProperty(obj, key, value) {
    if (key in obj) {
      Object.defineProperty(obj, key, {
        value: value,
        enumerable: true,
        configurable: true,
        writable: true
      });
    } else {
      obj[key] = value;
    }

    return obj;
  }

  function ownKeys(object, enumerableOnly) {
    var keys = Object.keys(object);

    if (Object.getOwnPropertySymbols) {
      var symbols = Object.getOwnPropertySymbols(object);
      if (enumerableOnly) symbols = symbols.filter(function (sym) {
        return Object.getOwnPropertyDescriptor(object, sym).enumerable;
      });
      keys.push.apply(keys, symbols);
    }

    return keys;
  }

  function _objectSpread2(target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i] != null ? arguments[i] : {};

      if (i % 2) {
        ownKeys(Object(source), true).forEach(function (key) {
          _defineProperty(target, key, source[key]);
        });
      } else if (Object.getOwnPropertyDescriptors) {
        Object.defineProperties(target, Object.getOwnPropertyDescriptors(source));
      } else {
        ownKeys(Object(source)).forEach(function (key) {
          Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key));
        });
      }
    }

    return target;
  }

  function _unsupportedIterableToArray(o, minLen) {
    if (!o) return;
    if (typeof o === "string") return _arrayLikeToArray(o, minLen);
    var n = Object.prototype.toString.call(o).slice(8, -1);
    if (n === "Object" && o.constructor) n = o.constructor.name;
    if (n === "Map" || n === "Set") return Array.from(o);
    if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen);
  }

  function _arrayLikeToArray(arr, len) {
    if (len == null || len > arr.length) len = arr.length;

    for (var i = 0, arr2 = new Array(len); i < len; i++) arr2[i] = arr[i];

    return arr2;
  }

  function _createForOfIteratorHelper(o, allowArrayLike) {
    var it;

    if (typeof Symbol === "undefined" || o[Symbol.iterator] == null) {
      if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") {
        if (it) o = it;
        var i = 0;

        var F = function () {};

        return {
          s: F,
          n: function () {
            if (i >= o.length) return {
              done: true
            };
            return {
              done: false,
              value: o[i++]
            };
          },
          e: function (e) {
            throw e;
          },
          f: F
        };
      }

      throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
    }

    var normalCompletion = true,
        didErr = false,
        err;
    return {
      s: function () {
        it = o[Symbol.iterator]();
      },
      n: function () {
        var step = it.next();
        normalCompletion = step.done;
        return step;
      },
      e: function (e) {
        didErr = true;
        err = e;
      },
      f: function () {
        try {
          if (!normalCompletion && it.return != null) it.return();
        } finally {
          if (didErr) throw err;
        }
      }
    };
  }

  var settingsInit = {
    alt: i18next.t("alt", {
      ns: "roadArea"
    }),
    ns: "roadArea",
    margin: {
      top: 50,
      left: 130,
      right: 30,
      bottom: 50
    },
    aspectRatio: 16 / 11,
    // creates variable d
    filterData: function filterData(data) {
      // Flag to check if data has already been padded (i.e. data file has never been loaded)
      var isOld = false;
      data.map(function (item) {
        if (item.isLast) {
          isOld = isOld || true;
        }
      }); // If not padded, pad out the last year out to Jan 10

      if (!isOld) {
        var padMonth = 0;
        var padDay = 10; // (year, month, date, hours, minutes, seconds, ms)

        data.push({
          date: new Date(data[data.length - 1].date, padMonth, padDay, 0, 0, 0, 0),
          diesel: data[data.length - 1].diesel,
          gas: data[data.length - 1].gas,
          lpg: data[data.length - 1].lpg,
          total: data[data.length - 1].total,
          isLast: true
        });
      }

      return data;
    },
    x: {
      label: i18next.t("x_label", {
        ns: "roadArea"
      }),
      getValue: function getValue(d, i) {
        return new Date(d.date);
      },
      getText: function getText(d) {
        return d.date;
      },
      ticks: 8,
      tickSizeOuter: 0
    },
    y: {
      label: i18next.t("y_label", {
        ns: "roadArea"
      }),
      getValue: function getValue(d, key) {
        if (typeof d[key] === "string" || d[key] instanceof String) {
          return 0;
        } else return d[key] * 1.0 / 1;
      },
      getTotal: function getTotal(d, index, data) {
        var total;
        var keys;
        var sett = this;

        if (!d[sett.y.totalProperty]) {
          keys = sett.z.getKeys.call(sett, data);
          total = 0;

          for (var k = 0; k < keys.length; k++) {
            total += sett.y.getValue.call(sett, d, keys[k], data) * 1; // keep in orig scale when summing
          }

          d[sett.y.totalProperty] = total;
        }

        return isNaN(Number(d[sett.y.totalProperty])) ? 0 : Number(d[sett.y.totalProperty]) * 1.0 / 1;
      },
      getText: function getText(d, key) {
        if (!d.isLast) {
          return isNaN(Number(d[key])) ? d[key] : this.formatNum(Number(d[key]));
        }
      },
      getTickText: function getTickText(d) {
        return this.formatNum(d);
      },
      ticks: 5,
      tickSizeOuter: 0
    },
    z: {
      label: i18next.t("z_label", {
        ns: "roadArea"
      }),
      getId: function getId(d) {
        if (d.key !== "isLast") {
          return d.key;
        }
      },
      getKeys: function getKeys(object) {
        var sett = this;
        var keys = Object.keys(object[0]);
        keys.splice(keys.indexOf("date"), 1);

        if (keys.indexOf(sett.y.totalProperty) !== -1) {
          keys.splice(keys.indexOf(sett.y.totalProperty), 1);
        }

        if (keys.indexOf("isLast") !== -1) {
          // temporary key to be removed
          keys.splice(keys.indexOf("isLast"), 1);
        }

        return keys;
      },
      origData: function origData(data) {
        // remove last point which was added in filterData (padded date)
        var origData = data.slice(0, -1);
        return origData;
      },
      getClass: function getClass() {
        for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
          args[_key] = arguments[_key];
        }

        return this.z.getId.apply(this, args);
      },
      getText: function getText(d) {
        return i18next.t(d.key, {
          ns: "roadArea"
        });
      }
    },
    datatable: true,
    tableTitle: "",
    dataTableTotal: true,
    // show total in data table
    transition: false,
    width: 1050
  };

  var NetGas = 1;
  var NetDiesel = 3;
  var NetLPG = 4;
  var RoadProductId = 23100066;
  var webAPI = "https://www150.statcan.gc.ca/t1/wds/rest/getDataFromCubePidCoordAndLatestNPeriods";
  var numToProvince = {
    1: "CANADA",
    2: "NL",
    3: "PE",
    4: "NS",
    5: "NB",
    6: "QC",
    7: "ON",
    8: "MB",
    9: "SK",
    10: "AB",
    11: "BC",
    12: "YT",
    14: "NT",
    15: "NU"
  };
  var statusCodes = {
    1: "..",
    2: "0s",
    3: "A",
    4: "B",
    5: "C",
    6: "D",
    7: "E",
    8: "F"
  };
  var qi_F = 8;
  function apiCall (maxYear, selectedYear, geography) {
    return new Promise(function (resolve, reject) {
      // get coordinates for data
      var coordinateArray = coordinateTranslate(geography);
      var yearRange = Number(maxYear) - Number(selectedYear) + 1;
      var returnArray = [];
      var myData = [];

      for (var i = 0; i < coordinateArray.length; i++) {
        myData.push({
          "productId": RoadProductId,
          "coordinate": coordinateArray[i],
          "latestN": yearRange
        });
      }

      $.support.cors = true;
      $.ajax({
        type: "post",
        url: webAPI,
        data: JSON.stringify(myData),
        dataType: "json",
        contentType: "application/json",
        success: function success(data, textStatus, jQxhr) {
          returnArray = rebuildAll(data, yearRange);
          resolve(returnArray);
        },
        error: function error(jqXhr, textStatus, errorThrown) {
          reject(errorThrown);
        }
      });
    });
  }

  function rebuildAll(data, yearRange) {
    var dataByProvince = {};
    var returnArray = [];
    var provinceCode;

    for (var i = 0; i < data.length; i++) {
      provinceCode = data[i].object.coordinate.split(".", 1)[0];

      if (!dataByProvince.hasOwnProperty(provinceCode)) {
        dataByProvince[provinceCode] = [];
      }

      dataByProvince[provinceCode].push(data[i]);
    }

    for (var province in dataByProvince) {
      for (var _i = 0; _i < yearRange; _i++) {
        returnArray.push(rebuildData(dataByProvince[province], numToProvince[province], _i));
      }
    }

    return returnArray;
  }

  function rebuildData(data, geography, year) {
    var returnObject = {};
    var datapoint;

    for (var i = 0; i < data.length; i++) {
      var returnType = Number(data[i].object.coordinate.split(".", 2)[1]);
      var returnValue = void 0;
      datapoint = data[i].object.vectorDataPoint[year];

      if (datapoint.statusCode != 1 && datapoint.securityLevelCode == 0 && datapoint.statusCode != qi_F) {
        returnValue = datapoint.value;
      } else {
        returnValue = statusCodes[datapoint.statusCode];
      }

      if (returnType === NetGas) {
        returnObject.gas = returnValue;
      } else if (returnType === NetDiesel) {
        returnObject.diesel = returnValue;
      } else if (returnType === NetLPG) {
        returnObject.lpg = returnValue;
      }
    }

    returnObject.date = datapoint.refPer.substring(0, 4);
    returnObject.province = geography;
    returnObject.annualTotal = (Number(returnObject.lpg) ? returnObject.lpg : 0) + (Number(returnObject.diesel) ? returnObject.diesel : 0) + (Number(returnObject.gas) ? returnObject.gas : 0);
    return returnObject;
  }

  function coordinateTranslate(geography) {
    var numGeo;

    switch (geography) {
      case "CANADA":
        numGeo = 1;
        break;

      case "NL":
        numGeo = 2;
        break;

      case "PE":
        numGeo = 3;
        break;

      case "NS":
        numGeo = 4;
        break;

      case "NB":
        numGeo = 5;
        break;

      case "QC":
        numGeo = 6;
        break;

      case "ON":
        numGeo = 7;
        break;

      case "MB":
        numGeo = 8;
        break;

      case "SK":
        numGeo = 9;
        break;

      case "AB":
        numGeo = 10;
        break;

      case "BC":
        numGeo = 11;
        break;

      case "YT":
        numGeo = 12;
        break;

      case "NT":
        numGeo = 14;
        break;

      case "NU":
        numGeo = 15;
        break;

      case "ALL":
        return ["".concat(1, ".", NetGas, ".0.0.0.0.0.0.0.0"), "".concat(1, ".", NetDiesel, ".0.0.0.0.0.0.0.0"), "".concat(1, ".", NetLPG, ".0.0.0.0.0.0.0.0"), "".concat(2, ".", NetGas, ".0.0.0.0.0.0.0.0"), "".concat(2, ".", NetDiesel, ".0.0.0.0.0.0.0.0"), "".concat(2, ".", NetLPG, ".0.0.0.0.0.0.0.0"), "".concat(3, ".", NetGas, ".0.0.0.0.0.0.0.0"), "".concat(3, ".", NetDiesel, ".0.0.0.0.0.0.0.0"), "".concat(3, ".", NetLPG, ".0.0.0.0.0.0.0.0"), "".concat(4, ".", NetGas, ".0.0.0.0.0.0.0.0"), "".concat(4, ".", NetDiesel, ".0.0.0.0.0.0.0.0"), "".concat(4, ".", NetLPG, ".0.0.0.0.0.0.0.0"), "".concat(5, ".", NetGas, ".0.0.0.0.0.0.0.0"), "".concat(5, ".", NetDiesel, ".0.0.0.0.0.0.0.0"), "".concat(5, ".", NetLPG, ".0.0.0.0.0.0.0.0"), "".concat(6, ".", NetGas, ".0.0.0.0.0.0.0.0"), "".concat(6, ".", NetDiesel, ".0.0.0.0.0.0.0.0"), "".concat(6, ".", NetLPG, ".0.0.0.0.0.0.0.0"), "".concat(7, ".", NetGas, ".0.0.0.0.0.0.0.0"), "".concat(7, ".", NetDiesel, ".0.0.0.0.0.0.0.0"), "".concat(7, ".", NetLPG, ".0.0.0.0.0.0.0.0"), "".concat(8, ".", NetGas, ".0.0.0.0.0.0.0.0"), "".concat(8, ".", NetDiesel, ".0.0.0.0.0.0.0.0"), "".concat(8, ".", NetLPG, ".0.0.0.0.0.0.0.0"), "".concat(9, ".", NetGas, ".0.0.0.0.0.0.0.0"), "".concat(9, ".", NetDiesel, ".0.0.0.0.0.0.0.0"), "".concat(9, ".", NetLPG, ".0.0.0.0.0.0.0.0"), "".concat(10, ".", NetGas, ".0.0.0.0.0.0.0.0"), "".concat(10, ".", NetDiesel, ".0.0.0.0.0.0.0.0"), "".concat(10, ".", NetLPG, ".0.0.0.0.0.0.0.0"), "".concat(11, ".", NetGas, ".0.0.0.0.0.0.0.0"), "".concat(11, ".", NetDiesel, ".0.0.0.0.0.0.0.0"), "".concat(11, ".", NetLPG, ".0.0.0.0.0.0.0.0"), "".concat(12, ".", NetGas, ".0.0.0.0.0.0.0.0"), "".concat(12, ".", NetDiesel, ".0.0.0.0.0.0.0.0"), "".concat(12, ".", NetLPG, ".0.0.0.0.0.0.0.0"), "".concat(14, ".", NetGas, ".0.0.0.0.0.0.0.0"), "".concat(14, ".", NetDiesel, ".0.0.0.0.0.0.0.0"), "".concat(14, ".", NetLPG, ".0.0.0.0.0.0.0.0"), "".concat(15, ".", NetGas, ".0.0.0.0.0.0.0.0"), "".concat(15, ".", NetDiesel, ".0.0.0.0.0.0.0.0"), "".concat(15, ".", NetLPG, ".0.0.0.0.0.0.0.0")];
    }

    return ["".concat(numGeo, ".").concat(NetGas, ".0.0.0.0.0.0.0.0"), "".concat(numGeo, ".").concat(NetDiesel, ".0.0.0.0.0.0.0.0"), "".concat(numGeo, ".").concat(NetLPG, ".0.0.0.0.0.0.0.0")];
  }

  var webAPI$1 = "https://www150.statcan.gc.ca/t1/wds/rest/getDataFromCubePidCoordAndLatestNPeriods";
  function dateRangeFn (minYear, frequency, productID, defaultCoord, granularity) {
    return new Promise(function (resolve, reject) {
      var mostRecentDate;
      var numberOfPeriods;
      var returnObject = {};
      var myData = [{
        "productId": productID,
        "coordinate": defaultCoord,
        "latestN": 1
      }];
      $.ajax({
        type: "post",
        url: webAPI$1,
        data: JSON.stringify(myData),
        dataType: "json",
        contentType: "application/json",
        success: function success(data, textStatus, jQxhr) {
          if (granularity === "year") {
            mostRecentDate = data[0].object.vectorDataPoint[0].refPer.substring(0, 4);
            numberOfPeriods = (Number(mostRecentDate) - minYear + 1) * frequency;
            returnObject.max = mostRecentDate;
            returnObject.numPeriods = numberOfPeriods;
          } else if (granularity === "month") {
            mostRecentDate = data[0].object.vectorDataPoint[0].refPer.substring(0, 7);
            numberOfPeriods = (Number(mostRecentDate.substring(0, 4)) - minYear) * frequency + Number(mostRecentDate.substring(5, 7));
            returnObject.max = mostRecentDate;
            returnObject.numPeriods = numberOfPeriods;
          }

          resolve(returnObject);
        },
        error: function error(jqXhr, textStatus, errorThrown) {
          reject(errorThrown);
        }
      });
    });
  }

  function mapColourScaleFn (svgCB, colourArray, dimExtent, numLevels, settings) {
    // Definitions
    // ---------------------------------------------------------------------------
    var rectDim = 35;
    var yRect = 20;
    var yText = 65;
    var yNaNText = yText + 7; // text labels (calculate cbValues)

    var delta = (dimExtent[1] - dimExtent[0]) / numLevels;
    var cbValues = [];
    cbValues[0] = dimExtent[0];

    for (var idx = 1; idx < numLevels; idx++) {
      cbValues.push(Math.round(idx * delta + dimExtent[0]));
    } // rect fill fn


    var getFill = function getFill(d, i) {
      return colourArray[i];
    }; // text fn


    var getText = function getText(i, j) {
      if (i < numLevels) {
        var s0 = settings.formatNum(cbValues[j]);

        if (numLevels === 1) {
          return s0;
        } else {
          return s0 + "+";
        }
      } else if (i === numLevels + 1) {
        return "x";
      }
    }; // tooltip for NaN box


    var divNaN = d3.select("body").append("div").attr("class", "tooltip").style("opacity", 0); // -----------------------------------------------------------------------------
    // g node for colourbar title (text is set in index.js)

    svgCB.append("g").attr("class", "colourbarTitle").attr("id", "cbTitle").append("text") // .text("test")
    .attr("transform", function (d, i) {
      return "translate(225, 15)";
    }).style("display", function () {
      return "inline";
    }); // Create the umbrella group

    var rectGroups = svgCB.attr("class", "mapCB").selectAll(".legend").data(Array.from(Array(colourArray.length).keys())); // Append g nodes (to be filled with a rect and a text) to umbrella group

    var newGroup = rectGroups.enter().append("g").attr("class", "legend").attr("id", function (d, i) {
      return "cb".concat(i);
    }); // add rects

    newGroup.append("rect").attr("width", rectDim).attr("height", rectDim).attr("y", yRect).attr("x", function (d, i) {
      return 135 + i * rectDim;
    }).attr("fill", getFill).attr("class", function (d, i) {
      if (i === numLevels + 1) {
        return "classNaN";
      }

      if (numLevels === 1) {
        return "zeroValue";
      }
    }); // hover over NaN rect only

    newGroup.selectAll(".legend rect").on("mouseover", function (d, i) {
      if (d3.select(this).attr("class") === "classNaN") {
        var line1 = i18next.t("NaNhover1", {
          ns: "airUI"
        });
        var line2 = i18next.t("NaNhover2", {
          ns: "airUI",
          escapeInterpolation: false
        });
        divNaN.style("opacity", 0.9).html("<br>" + line1 + "<br>" + line2 + "<br><br>").style("left", d3.event.pageX + 10 + "px").style("top", d3.event.pageY + 10 + "px");
      }
    }).on("mouseout", function () {
      divNaN.style("opacity", 0);
    }); // add text

    newGroup.append("text").text(getText).attr("text-anchor", "end").attr("transform", function (d, i) {
      if (i < numLevels) {
        return "translate(".concat(140 + i * (rectDim + 0), ", ").concat(yText, ") rotate(-45)");
      } else if (i === numLevels + 1) {
        // NaN box in legend
        return "translate(".concat(156 + i * (rectDim + 0), ", ").concat(yNaNText, ") ");
      }
    }).style("display", function () {
      return "inline";
    }); // Update rect fill for any new colour arrays passed in

    rectGroups.select("rect").attr("fill", getFill); // Update rect text for different year selections

    rectGroups.select("text").text(getText); // hack to get color bar cetered when value is 0

    if (numLevels === 1) {
      d3.select("#cb0").attr("transform", "translate(73,0)");
    } else {
      d3.select("#cb0").attr("transform", "translate(0,0)");
    }

    rectGroups.exit().remove();
  }

  function fillMapFn (data, colourArray, numLevels) {
    var nullColour = "#565656"; // data is an Array

    var thisData = data[0]; // Object

    var dimExtent = [];
    var totArray = [];
    totArray = Object.values(thisData);
    totArray.sort(function (a, b) {
      return a - b;
    });
    dimExtent = d3.extent(totArray); // colour map to take data value and map it to the colour of the level bin it belongs to

    var colourMap = d3.scaleQuantize().domain([dimExtent[0], dimExtent[1]]).range(colourArray.slice(0, numLevels));

    var _loop = function _loop(key) {
      if (thisData.hasOwnProperty(key)) {
        d3.select(".dashboard .map").select("." + key).style("fill", function () {
          return Number(thisData[key]) ? colourMap(thisData[key]) : nullColour;
        }); // Bruno : Unused code removed following no data message modification
      }
    };

    for (var key in thisData) {
      _loop(key);
    }

    return dimExtent;
  }

  function areaTooltip (settings, div, d) {
    var thisMonth = d.date.substring(5, 7) ? i18next.t(d.date.substring(5, 7), {
      ns: "months"
    }) : null;
    var thisYear = d.date.substring(0, 4);
    var line1 = thisMonth ? "".concat(i18next.t("hoverTitle", {
      ns: settings.ns
    }), ", ").concat(thisMonth, " ").concat(thisYear, ": ") : "".concat(i18next.t("hoverTitle", {
      ns: settings.ns
    }), ", ").concat(d.date, ": ");
    var keys = Object.keys(d); // remove unwanted keys

    keys.splice(keys.indexOf("date"), 1);

    if (keys.indexOf("total") !== -1) {
      keys.splice(keys.indexOf("total"), 1);
    }

    if (keys.indexOf("isLast") !== -1) {
      keys.splice(keys.indexOf("isLast"), 1);
    }

    var makeTable = function makeTable(line1) {
      var keyValues = [];

      for (var idx = 0; idx < keys.length; idx++) {
        var key = keys[idx];
        keyValues.push(settings.y.getText.call(settings, d, key));
      }

      var rtnTable = "<b>".concat(line1, "</b><br><br><table>");

      for (var _idx = 0; _idx < keys.length; _idx++) {
        rtnTable = rtnTable.concat("<tr><td><b>".concat(i18next.t(keys[_idx], {
          ns: settings.ns
        }), "</b>: ").concat(keyValues[_idx], "</td></tr>"));
      }

      rtnTable = rtnTable.concat("</table>");
      return rtnTable;
    };

    div.html(makeTable(line1)).style("opacity", .9).style("left", d3.event.pageX + 10 + "px").style("top", d3.event.pageY + 10 + "px").style("pointer-events", "none");
  }

  function createOverlay (chartObj, data, onMouseOverCb, onMouseOutCb) {
    // TEMP
    chartObj.svg.datum(chartObj);
    chartObj.data = data;
    var bisect = d3.bisector(function (d) {
      return chartObj.settings.x.getValue(d);
    }).left;
    var overlay = chartObj.svg.select(".data .overlay");
    var rect;
    var line;

    if (overlay.empty()) {
      overlay = chartObj.svg.select(".data").append("g").attr("class", "overlay");
      rect = overlay.append("rect").style("fill", "none").style("pointer-events", "all").attr("class", "overlay");
      line = overlay.append("line").attr("class", "hoverLine").style("display", "inline").style("visibility", "hidden");
    } else {
      rect = overlay.select("rect");
      line = overlay.select("line");
    }

    rect.attr("width", chartObj.settings.innerWidth).attr("height", chartObj.settings.innerHeight).on("mousemove", function (e) {
      var chartObj = d3.select(this.ownerSVGElement).datum();
      var x = d3.mouse(this)[0];
      var xD = chartObj.x.invert(x);
      var i = bisect(chartObj.data, xD);
      var d0 = chartObj.data[i - 1];
      var d1 = chartObj.data[i];
      var d;

      if (d0 && d1) {
        d = xD - chartObj.settings.x.getValue(d0) > chartObj.settings.x.getValue(d1) - xD ? d1 : d0;
      } else if (d0) {
        d = d0;
      } else {
        d = d1;
      }

      line.attr("x1", chartObj.x(chartObj.settings.x.getValue(d)));
      line.attr("x2", chartObj.x(chartObj.settings.x.getValue(d)));
      line.style("visibility", "visible");

      if (onMouseOverCb && typeof onMouseOverCb === "function") {
        onMouseOverCb(d);
      }
    }).on("mouseout", function () {
      line.style("visibility", "hidden");

      if (onMouseOutCb && typeof onMouseOutCb === "function") {
        onMouseOutCb();
      }
    });
    line.attr("x1", 0).attr("x2", 0).attr("y1", 0).attr("y2", chartObj.settings.innerHeight);
  }

  var CopyButton = /*#__PURE__*/function () {
    function CopyButton(pNode, options) {
      _classCallCheck(this, CopyButton);

      this.pNode = pNode ? pNode : document.createElement("div");
      this.options = options ? options : {};
      this.nodes = {};
      this.data = null;
      this.instanceNumber = ++CopyButton.n;
      this["class"] = this.options["class"] || "";
      /* this.data = shall be an array (i.e called rowsArray) of arrays (i.e each is called row).
        each array on rowsArray represents a row on the table.
        this.data must be set/updated by the code that uses this button
        [
          ["title"]
          ["columna1" , "columna2" ,..., "columnaN"]
          ["value1Row1", "value2Row1",..., "valueNRowN"]
          ["value1Row2", "value2Row2",..., "valueNRowN"]
        ]
      */
    }

    _createClass(CopyButton, [{
      key: "build",
      value: function build(options) {
        if (options) this.options = options; // workAround;

        if (options.pNode) this.pNode = options.pNode; // workAround;

        if (options["class"]) this["class"] = options["class"]; // workAround;

        this.root = document.createElement("div");
        this.root.setAttribute("class", "copy-button button-" + this.instanceNumber + " " + this["class"]);
        this.pNode.appendChild(this.root);
        this.nodes.btnCopy = document.createElement("button");
        this.nodes.btnCopy.setAttribute("type", "button");
        this.nodes.btnCopy.setAttribute("class", "btn btn-primary copy button-" + this.instanceNumber + " " + this["class"]);
        this.nodes.btnCopy.setAttribute("title", this.options.title || "");
        this.root.appendChild(this.nodes.btnCopy);
        var icon = document.createElement("span");
        icon.setAttribute("class", "fa fa-clipboard clipboard button-" + this.instanceNumber + " " + this["class"]);
        this.nodes.btnCopy.appendChild(icon);
        var accessibility = document.createElement("span");
        accessibility.setAttribute("class", "wb-inv button-" + this.instanceNumber + " " + this["class"]);
        accessibility.innerHTML = this.options.accessibility || "";
        this.nodes.btnCopy.appendChild(accessibility);
        this.nodes.msgCopyConfirm = document.createElement("div");
        this.nodes.msgCopyConfirm.setAttribute("class", "copy-confirm button-" + this.instanceNumber + " " + this["class"]);
        this.nodes.msgCopyConfirm.setAttribute("aria-live", "polite");
        this.nodes.msgCopyConfirm.innerHTML = this.options.msgCopyConfirm || "";
        this.root.appendChild(this.nodes.msgCopyConfirm);
        this.nodes.btnCopy.addEventListener("click", this.onBtnClick.bind(this));
      }
    }, {
      key: "onBtnClick",
      value: function onBtnClick(ev) {
        this.copyData(this.data);
      }
    }, {
      key: "copyData",
      value: function copyData(lines) {
        var linesTemp = lines ? lines : [];
        this.clipboard(this.toCSV("\t", linesTemp), this.root);
        this.fade(this.nodes.msgCopyConfirm, true);
        setTimeout(function (ev) {
          this.fade(this.nodes.msgCopyConfirm, false);
        }.bind(this), 1500);
      }
    }, {
      key: "toCSV",
      value: function toCSV(separator, lines) {
        var csv = lines.map(function (line) {
          return line.join(separator);
        });
        return csv.join("\r\n");
      }
    }, {
      key: "clipboard",
      value: function clipboard(string, target) {
        if (this.isIE()) window.clipboardData.setData("Text", string);else {
          // Copying the string
          var aux = document.createElement("textarea");
          aux.textContent = string;
          target.appendChild(aux);
          aux.select();
          document.execCommand("copy");
          target.removeChild(aux);
        }
      }
    }, {
      key: "fade",
      value: function fade(node, visible) {
        var clss = ["copy-confirm button-" + this.instanceNumber + " " + this["class"]];
        var add = visible ? "fadeIn" : "fadeOut";
        clss.push(add);
        node.setAttribute("class", clss.join(" "));
      } // work around for when tables are destroyed

    }, {
      key: "appendTo",
      value: function appendTo(pNode) {
        this.pNode = pNode;
        this.pNode.appendChild(this.root);
        this.nodes.msgCopyConfirm.setAttribute("class", "copy-confirm button-" + this.instanceNumber + " " + this["class"]);
      }
    }, {
      key: "isIE",
      value: function isIE() {
        var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE ");

        if (msie > 0) {
          // IE 10 or older => return version number
          return parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)), 10);
        }

        var trident = ua.indexOf("Trident/");

        if (trident > 0) {
          // IE 11 => return version number
          var rv = ua.indexOf("rv:");
          return parseInt(ua.substring(rv + 3, ua.indexOf(".", rv)), 10);
        }

        var edge = ua.indexOf("Edge/");

        if (edge > 0) {
          // Edge (IE 12+) => return version number
          return parseInt(ua.substring(edge + 5, ua.indexOf(".", edge)), 10);
        } // other browser


        return false;
      }
    }]);

    return CopyButton;
  }();
  CopyButton.n = 0;

  var timeoutTime = 3000;
  var RoadProductId$1 = 23100066;
  var data = {};
  var mapData = {};
  var areaData = {};
  var dateRange = {};
  var stackedArea; // stores areaChart() call

  var selectedRegion = "CANADA";
  var maxYear;
  var minYear = 2010;
  var selectedYear;
  var xlabelDY = 1.5; // spacing between areaChart xlabels and ticks
  // Add number formatter to stackedArea settings file
  // Add number formatter to stackedArea settings file

  var settingsAux = {
    _selfFormatter: i18n.getNumberFormatter(0),
    formatNum: function formatNum() {
      for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
        args[_key] = arguments[_key];
      }

      return this._selfFormatter.format(args);
    }
  };

  var settings = _objectSpread2(_objectSpread2({}, settingsInit), settingsAux);
  /* Copy Button */
  // -----------------------------------------------------------------------------


  var cButton = new CopyButton(); // -----------------------------------------------------------------------------

  /* SVGs */
  // Fuel sales stacked area chart

  var chart = d3.select(".data").append("svg").attr("id", "svgFuel"); // Canada map

  var map = d3.select(".dashboard .map").append("svg"); // Map colour bar

  var margin = {
    top: 20,
    right: 0,
    bottom: 10,
    left: 20
  };
  var width = 570 - margin.left - margin.right;
  var height = 150 - margin.top - margin.bottom;
  var svgCB = d3.select("#mapColourScale").select("svg").attr("class", "mapCB").attr("width", width).attr("height", height).style("vertical-align", "middle");
  /* -- shim all the SVGs (chart is already shimmed in component) -- */

  d3.stcExt.addIEShim(map, 387.1, 457.5);
  d3.stcExt.addIEShim(svgCB, height, width); // -----------------------------------------------------------------------------

  /* tooltip */

  var div = d3.select("body").append("div").attr("class", "tooltip").style("opacity", 0);
  var divArea = d3.select("body").append("div").attr("class", "tooltip").style("opacity", 0); // -----------------------------------------------------------------------------

  /* -- Map interactions -- */

  map.on("mousemove", function () {
    if (d3.select(d3.event.target).attr("class")) {
      // const classes = d3.event.target.classList;
      var classes = (d3.select(d3.event.target).attr("class") || "").split(" "); // IE-compatible

      if (classes[0] !== "svg-shimmed") {
        // Highlight map region
        var selectedPath = d3.select(".dashboard .map").select("." + classes[0]);
        selectedPath.classed("roadMapHighlight", true);
        selectedPath.moveToFront(); // Tooltip

        var key = i18next.t(classes[0], {
          ns: "geography"
        });
        var value = settings.formatNum(mapData[selectedYear][classes[0]]);
        div.style("opacity", .9);
        div.html("<b>" + key + " (" + i18next.t("units", {
          ns: "road"
        }) + ")</b>" + "<br><br>" + "<table>" + "<tr>" + "<td><b>" + value + "</td>" + "</tr>" + "</table>");
        div.style("left", d3.event.pageX + 10 + "px").style("top", d3.event.pageY + 10 + "px");
      } else {
        // clear tooltip for IE
        div.style("opacity", 0);
      }
    }
  });
  map.on("mouseout", function () {
    div.style("opacity", 0);

    if (selectedRegion) {
      d3.select(".map").selectAll("path:not(." + selectedRegion + ")").classed("roadMapHighlight", false);
    } else {
      d3.select(".map").selectAll("path").classed("roadMapHighlight", false);
    }
  });
  map.on("click", function () {
    // clear any previous clicks
    d3.select(".map").selectAll("path").classed("roadMapHighlight", false);

    if (d3.select(d3.event.target).attr("class") && d3.select(d3.event.target).attr("class").indexOf("svg-shimmed") === -1) {
      var classes = (d3.select(d3.event.target).attr("class") || "").split(" "); // IE-compatible

      selectedRegion = classes[0];
      d3.select(".dashboard .map").select("." + classes[0]).classed("roadMapHighlight", true).moveToFront();
      updateTitles(); // Display selected region in stacked area chart

      loadData(selectedRegion, function () {
        showAreaData();
      }); // update region displayed in dropdown menu

      d3.select("#groups")._groups[0][0].value = selectedRegion;
    } else {
      // reset area chart to Canada
      selectedRegion = "CANADA";
      updateTitles();
      showAreaData(); // update region displayed in dropdown menu

      d3.select("#groups")._groups[0][0].value = selectedRegion;
    }
  }); // -----------------------------------------------------------------------------

  /* FNS */

  function createDropdown() {
    // check available month/year combinations
    var yearId = "#".concat("year");
    var yearDropdown = $(yearId); // date dropdown creation

    yearDropdown.empty();

    for (var i = dateRange.min; i <= dateRange.max; i++) {
      yearDropdown.append($("<option></option>").attr("value", i).html(i));
    }

    d3.select(yearId)._groups[0][0].value = selectedYear;
  }

  function colorMap() {
    // store map data in array and plot colour
    var thisTotalArray = [];
    thisTotalArray.push(mapData[selectedYear]);
    var colourArray = ["#AFE2FF", "#72C2FF", "#bc9dff", "#894FFF", "#5D0FBC"];
    var numLevels = colourArray.length; // colour map with fillMapFn and output dimExtent for colour bar scale

    var dimExtent = fillMapFn(thisTotalArray, colourArray, numLevels); // colour bar scale and add label

    mapColourScaleFn(svgCB, colourArray, dimExtent, colourArray.length, settings); // Colourbar label (need be plotted only once)

    var mapScaleLabel = i18next.t("units", {
      ns: "road"
    });
    var xTitle = document.getElementsByTagName("html")[0].getAttribute("lang") === "en" ? 168 : 177;
    d3.select("#cbTitle").select("text").text(mapScaleLabel).attr("transform", function (d, i) {
      return "translate(".concat(xTitle, ", 15)");
    });
  }
  /* -- display areaChart -- */


  function showAreaData() {
    if (!areaData.hasOwnProperty(selectedRegion)) {
      areaData[selectedRegion] = [];

      for (var year in data[selectedRegion]) {
        // convert to expected area chart format
        var areaObj = {};
        areaObj.gas = data[selectedRegion][year].gas;
        areaObj.diesel = data[selectedRegion][year].diesel;
        areaObj.lpg = data[selectedRegion][year].lpg;
        areaObj.date = data[selectedRegion][year].date;
        areaData[selectedRegion].push(areaObj);
      }
    }

    stackedArea = areaChart(chart, settings, areaData[selectedRegion]);
    d3.select("#svgFuel").select(".x.axis").select("text").attr("display", "none");
    d3.select("#svgFuel").select(".x.axis").selectAll(".tick text").attr("dy", "".concat(xlabelDY, "em"));
    createOverlay(stackedArea, areaData[selectedRegion], function (d) {
      areaTooltip(stackedArea.settings, divArea, d);
    }, function () {
      divArea.style("opacity", 0);
    }); // Highlight region selected from menu on map

    d3.select(".dashboard .map").select("." + selectedRegion).classed("roadMapHighlight", true).moveToFront();
    updateTitles();
    cButton.appendTo(document.getElementById("copy-button-container"));
    dataCopyButton(areaData[selectedRegion]);
  }
  /* -- update map and areaChart titles -- */


  function updateTitles() {
    var geography = i18next.t(selectedRegion, {
      ns: "geography"
    });
    d3.select("#areaTitleRoad").text(i18next.t("chartTitle", {
      ns: "road"
    }) + ", " + geography);
    settings.tableTitle = i18next.t("tableTitle", {
      ns: "roadArea",
      geo: geography
    });
  } // -----------------------------------------------------------------------------

  /* load data fn */


  var loadData = function loadData(selectedRegion, cb) {
    if (!data[selectedRegion]) {
      d3.json("data/road/" + selectedRegion + ".json", function (err, filedata) {
        data[selectedRegion] = filedata;
        cb();
      });
    } else {
      cb();
    }
  };
  /* uiHandler*/


  function uiHandler(event) {
    if (event.target.id === "groups") {
      selectedRegion = document.getElementById("groups").value; // clear any map region that is highlighted

      d3.select(".map").selectAll("path").classed("roadMapHighlight", false); // Chart titles

      updateTitles();
      showAreaData();
    }

    if (event.target.id === "year") {
      selectedYear = document.getElementById("year").value;
      d3.select("#mapTitleRoad").text(i18next.t("mapTitle", {
        ns: "road",
        year: selectedYear
      }));
      colorMap();
    }
  } // -----------------------------------------------------------------------------

  /* Copy Button*/


  function dataCopyButton(cButtondataFull) {
    var lines = [];
    var geography = i18next.t(selectedRegion, {
      ns: "geography"
    });
    var title = [i18next.t("tableTitle", {
      ns: "roadArea",
      geo: geography
    })];
    var columnTitles = ["date", "gas", "diesel", "lpg", "total"];
    var columns = []; // filter out extra row added for data padding to areaChart

    var cButtondata = JSON.parse(JSON.stringify(cButtondataFull));
    cButtondata = cButtondataFull.filter(function (item) {
      if (item.isLast === undefined) {
        return item;
      }
    });

    for (var concept in columnTitles) {
      columns.push(i18next.t(columnTitles[concept], {
        ns: "roadArea"
      }));
    }

    lines.push(title, [], columns);

    for (var row in cButtondata) {
      if (Object.prototype.hasOwnProperty.call(cButtondata, row)) {
        var auxRow = [];

        for (var column in columnTitles) {
          var value = cButtondata[row][columnTitles[column]];
          auxRow.push(value);
        }

        lines.push(auxRow);
      }
    }

    cButton.data = lines;
  }

  function createMapData() {
    for (var i = dateRange.min; i <= dateRange.max; i++) {
      if (!mapData[i]) {
        mapData[i] = {};

        for (var province in data) {
          if (province != "CANADA") {
            mapData[i][province] = data[province][i].annualTotal;
          }
        }
      }
    }

    return;
  } // -----------------------------------------------------------------------------

  /* Initial page load */


  function pageInitWithData() {
    createMapData();
    getCanadaMap(map).on("loaded", function () {
      colorMap();
    });
    d3.select("#mapTitleRoad").text(i18next.t("mapTitle", {
      ns: "road",
      year: selectedYear
    })); // copy button options

    var cButtonOptions = {
      pNode: document.getElementById("copy-button-container"),
      title: i18next.t("CopyButton_Title", {
        ns: "CopyButton"
      }),
      msgCopyConfirm: i18next.t("CopyButton_Confirm", {
        ns: "CopyButton"
      }),
      accessibility: i18next.t("CopyButton_Title", {
        ns: "CopyButton"
      })
    }; // build nodes on copy button

    cButton.build(cButtonOptions);
    d3.select("#symbolLink").html("<a href=".concat(i18next.t("linkURL", {
      ns: "symbolLink"
    }), ">").concat(i18next.t("linkText", {
      ns: "symbolLink"
    }), "</a>"));
    createDropdown();
    showAreaData();
    updateTitles();
    d3.select("#loading-gif").style("visibility", "hidden");
  }

  var timeoutError = Symbol();

  var timeout = function timeout(prom, time, exception) {
    var timer;
    return Promise.race([prom, new Promise(function (_r, rej) {
      return timer = setTimeout(rej, time, exception);
    })])["finally"](function () {
      return clearTimeout(timer);
    });
  };

  i18n.load(["src/i18n"], function () {
    settings.x.label = i18next.t("x_label", {
      ns: "roadArea"
    }), settings.y.label = i18next.t("y_label", {
      ns: "roadArea"
    }), settings.tableTitle = i18next.t("tableTitle", {
      ns: "roadArea",
      geo: i18next.t(selectedRegion, {
        ns: "geography"
      })
    });
    timeout(dateRangeFn(minYear, 1, RoadProductId$1, "1.1.0.0.0.0.0.0.0.0", "year"), timeoutTime, timeoutError).then(function (result) {
      dateRange.min = minYear;
      dateRange.max = Number(result.max);
      dateRange.numPeriods = result.numPeriods;
      maxYear = result.max;
      selectedYear = maxYear;
      apiCall(maxYear, minYear, "ALL").then(function (mapData) {
        var _iterator = _createForOfIteratorHelper(mapData),
            _step;

        try {
          for (_iterator.s(); !(_step = _iterator.n()).done;) {
            var geo = _step.value;

            if (!data[geo.province]) {
              var yearObj = {};
              data[geo.province] = yearObj;
            }

            data[geo.province][geo.date] = geo;
          }
        } catch (err) {
          _iterator.e(err);
        } finally {
          _iterator.f();
        }

        pageInitWithData();
      });
    });
  });
  $(document).on("change", uiHandler);

  d3.selection.prototype.moveToFront = function () {
    return this.each(function () {
      this.parentNode.appendChild(this);
    });
  };

  d3.selection.prototype.moveToBack = function () {
    return this.each(function () {
      var firstChild = this.parentNode.firstChild;

      if (firstChild) {
        this.parentNode.insertBefore(this, firstChild);
      }
    });
  };

}());
