(function () {
  'use strict';

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  function _defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, descriptor.key, descriptor);
    }
  }

  function _createClass(Constructor, protoProps, staticProps) {
    if (protoProps) _defineProperties(Constructor.prototype, protoProps);
    if (staticProps) _defineProperties(Constructor, staticProps);
    return Constructor;
  }

  function _defineProperty(obj, key, value) {
    if (key in obj) {
      Object.defineProperty(obj, key, {
        value: value,
        enumerable: true,
        configurable: true,
        writable: true
      });
    } else {
      obj[key] = value;
    }

    return obj;
  }

  function ownKeys(object, enumerableOnly) {
    var keys = Object.keys(object);

    if (Object.getOwnPropertySymbols) {
      var symbols = Object.getOwnPropertySymbols(object);
      if (enumerableOnly) symbols = symbols.filter(function (sym) {
        return Object.getOwnPropertyDescriptor(object, sym).enumerable;
      });
      keys.push.apply(keys, symbols);
    }

    return keys;
  }

  function _objectSpread2(target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i] != null ? arguments[i] : {};

      if (i % 2) {
        ownKeys(Object(source), true).forEach(function (key) {
          _defineProperty(target, key, source[key]);
        });
      } else if (Object.getOwnPropertyDescriptors) {
        Object.defineProperties(target, Object.getOwnPropertyDescriptors(source));
      } else {
        ownKeys(Object(source)).forEach(function (key) {
          Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key));
        });
      }
    }

    return target;
  }

  function _unsupportedIterableToArray(o, minLen) {
    if (!o) return;
    if (typeof o === "string") return _arrayLikeToArray(o, minLen);
    var n = Object.prototype.toString.call(o).slice(8, -1);
    if (n === "Object" && o.constructor) n = o.constructor.name;
    if (n === "Map" || n === "Set") return Array.from(o);
    if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen);
  }

  function _arrayLikeToArray(arr, len) {
    if (len == null || len > arr.length) len = arr.length;

    for (var i = 0, arr2 = new Array(len); i < len; i++) arr2[i] = arr[i];

    return arr2;
  }

  function _createForOfIteratorHelper(o, allowArrayLike) {
    var it;

    if (typeof Symbol === "undefined" || o[Symbol.iterator] == null) {
      if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") {
        if (it) o = it;
        var i = 0;

        var F = function () {};

        return {
          s: F,
          n: function () {
            if (i >= o.length) return {
              done: true
            };
            return {
              done: false,
              value: o[i++]
            };
          },
          e: function (e) {
            throw e;
          },
          f: F
        };
      }

      throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
    }

    var normalCompletion = true,
        didErr = false,
        err;
    return {
      s: function () {
        it = o[Symbol.iterator]();
      },
      n: function () {
        var step = it.next();
        normalCompletion = step.done;
        return step;
      },
      e: function (e) {
        didErr = true;
        err = e;
      },
      f: function () {
        try {
          if (!normalCompletion && it.return != null) it.return();
        } finally {
          if (didErr) throw err;
        }
      }
    };
  }

  var settingsInit = {
    alt: i18next.t("alt", {
      ns: "airPassengers"
    }),
    ns: "airPassengers",
    margin: {
      top: 50,
      left: 130,
      right: 30,
      bottom: 50
    },
    aspectRatio: 16 / 11,
    filterData: function filterData(data) {
      // clone data object
      var dataClone = JSON.parse(JSON.stringify(data));
      var maxYears = dataClone.length; // extend previous year

      var padFullMonth = 11;
      var padFullDay = 31; // pad out the last year out to Jan 10

      var padMonth = 0;
      var padDay = 10; // (year, month, date, hours, minutes, seconds, ms)

      dataClone.push({
        date: new Date(dataClone[dataClone.length - 1].date.substring(0, 4), padMonth, padDay, 0, 0, 0, 0),
        domestic: dataClone[dataClone.length - 1].domestic,
        international: dataClone[dataClone.length - 1].international,
        transborder: dataClone[dataClone.length - 1].transborder,
        total: dataClone[dataClone.length - 1].total
      }); // Assign flag = 0 if all of the sectors are defined
      // else flag = 1 if at least one of the sectors is defined
      // else flag = -999 if none of the sectors is defined

      dataClone.filter(function (item) {
        if (parseFloat(item.domestic) && parseFloat(item.transborder) && parseFloat(item.international)) {
          item.flag = 0;
        } else if (parseFloat(item.domestic) || parseFloat(item.transborder) || parseFloat(item.international)) {
          item.flag = 1;
        } else if (!parseFloat(item.domestic) && !parseFloat(item.transborder) && !parseFloat(item.international)) {
          item.flag = -999;
        }
      });
      var count = 0;
      dataClone.filter(function (item) {
        item.isCopy = null;

        if (item.flag === 1 || item.flag === -999) {
          var prevIdx = count - 1 >= 0 ? count - 1 : 0; // counter for previous item

          if (item.flag === 1 && dataClone[prevIdx].flag === 0) {
            // Don't add previous item if 2 of the attributes are 0
            var prevSum = Number(dataClone[prevIdx].total);
            var partSum = Number(dataClone[prevIdx].domestic) + Number(dataClone[prevIdx].transborder) + Number(dataClone[prevIdx].international);

            if (prevSum !== partSum) {
              var decDate = new Date(dataClone[prevIdx].date, padFullMonth, padFullDay, 0, 0, 0, 0);
              dataClone.push({
                date: decDate,
                domestic: dataClone[prevIdx].domestic,
                international: dataClone[prevIdx].international,
                transborder: dataClone[prevIdx].transborder,
                total: dataClone[prevIdx].total,
                flag: dataClone[prevIdx].flag,
                isCopy: true
              });
            }
          } else if (item.flag === 1 || dataClone[prevIdx].flag === 1) {
            var sumDomestic = parseFloat(item.domestic) + parseFloat(dataClone[prevIdx].domestic);
            var sumTrans = parseFloat(item.transborder) + parseFloat(dataClone[prevIdx].transborder);
            var sumIntl = parseFloat(item.internationa) + parseFloat(dataClone[prevIdx].international);
            var thisMonth;
            var thisDay;

            if (!sumDomestic && !sumTrans && !sumIntl) {
              // this is last year therefore extend only out to Jan 31 (e.g. Manitoba)
              if (count >= maxYears) {
                thisMonth = padMonth;
                thisDay = 31;
              } else {
                // extend previous year
                thisMonth = padFullMonth;
                thisDay = padFullDay;
              }

              var _decDate = new Date(dataClone[prevIdx].date, thisMonth, thisDay, 0, 0, 0, 0);

              dataClone.push({
                date: _decDate,
                domestic: dataClone[prevIdx].domestic,
                international: dataClone[prevIdx].international,
                transborder: dataClone[prevIdx].transborder,
                total: dataClone[prevIdx].total,
                flag: dataClone[prevIdx].flag,
                isCopy: true
              });
            }
          } else if (item.flag === -999 && dataClone[prevIdx].flag !== -999) {
            var _decDate2 = new Date(dataClone[prevIdx].date, padFullMonth, padFullDay, 0, 0, 0, 0);

            dataClone.push({
              date: _decDate2,
              domestic: dataClone[prevIdx].domestic,
              international: dataClone[prevIdx].international,
              transborder: dataClone[prevIdx].transborder,
              total: dataClone[prevIdx].total,
              flag: dataClone[prevIdx].flag,
              isCopy: true
            });
          }
        }

        count++;
      });
      dataClone.sort(function (a, b) {
        return new Date(a.date) - new Date(b.date);
      });
      return baseDateFilter(dataClone);
    },
    x: {
      getLabel: function getLabel() {
        return i18next.t("x_label", {
          ns: "airPassengers"
        });
      },
      getValue: function getValue(d, i) {
        return new Date(d.date);
      },
      getText: function getText(d) {
        return d.date.substring(0, 4);
      },
      ticks: 7,
      tickSizeOuter: 0
    },
    y: {
      label: i18next.t("y_label", {
        ns: "airPassengers"
      }),
      getLabel: function getLabel() {
        return i18next.t("y_label", {
          ns: "airPassengers"
        });
      },
      getValue: function getValue(d, key) {
        if (d[key] === "x" || d[key] === "..") {
          return undefined;
        } else return Number(d[key]) * 1.0 / 1;
      },
      getTotal: function getTotal(d, index, data) {
        var total;
        var keys;
        var sett = this;

        if (!d[sett.y.totalProperty]) {
          keys = sett.z.getKeys.call(sett, data);
          total = 0;

          for (var k = 0; k < keys.length; k++) {
            total += sett.y.getValue.call(sett, d, keys[k], data);
          }

          d[sett.y.totalProperty] = total;
        }

        return isNaN(Number(d[sett.y.totalProperty])) ? 0 : Number(d[sett.y.totalProperty]) * 1.0 / 1;
      },
      getText: function getText(d, key) {
        return isNaN(Number(d[key])) ? d[key] : this.formatNum(Number(d[key]));
      },
      getTickText: function getTickText(d) {
        return this.formatNum(d);
      },
      ticks: 5,
      tickSizeOuter: 0
    },
    z: {
      label: i18next.t("z_label", {
        ns: "airPassengers"
      }),
      getId: function getId(d) {
        if (d.key !== "flag" && d.key !== "isCopy") {
          return d.key;
        }
      },
      getKeys: function getKeys(object) {
        var sett = this;
        var keys = Object.keys(object[0]); // remove unwanted keys

        keys.splice(keys.indexOf("date"), 1);

        if (keys.indexOf("flag") !== -1) {
          // temporary key to be removed
          keys.splice(keys.indexOf("flag"), 1);
        }

        if (keys.indexOf("isCopy") !== -1) {
          // temporary key to be removed
          keys.splice(keys.indexOf("isCopy"), 1);
        }

        if (keys.indexOf(sett.y.totalProperty) !== -1) {
          keys.splice(keys.indexOf(sett.y.totalProperty), 1);
        }

        return keys; // return keys.sort();
        // return ["local", "Remaining_local", "itinerant", "Remaining_itinerant"];
      },
      origData: function origData(data) {
        return baseDateFilter(data);
      },
      getClass: function getClass() {
        for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
          args[_key] = arguments[_key];
        }

        return this.z.getId.apply(this, args);
      },
      getText: function getText(d) {
        return i18next.t(d.key, {
          ns: "airPassengers"
        });
      }
    },
    datatable: true,
    dataTableTotal: true,
    // show total in data table
    areaTableID: "areaTable",
    // summaryId: "chrt-dt-tbl",
    transition: true,
    width: 1050
  };

  var baseDateFilter = function baseDateFilter(data) {
    var minDate = new Date("2010");
    var newData = [];

    for (var s = 0; s < data.length; s++) {
      var date = new Date(data[s].date);

      if (date >= minDate) {
        newData.push(data[s]);
      }
    }

    return newData;
  };

  var settingsMajorAirportsInit = {
    ns: "airMajorAirports",
    margin: {
      top: 50,
      left: 90,
      right: 30,
      bottom: 50
    },
    aspectRatio: 16 / 11,
    filterData: function filterData(data) {
      var count = 0;
      data.filter(function (item) {
        item.isLast = count === data.length - 1 ? true : false;
        count++;
      });
      return baseDateFilter$1(data);
    },
    x: {
      getLabel: function getLabel() {
        return i18next.t("x_label", {
          ns: "airMajorAirports"
        });
      },
      getValue: function getValue(d, i) {
        // return new Date(d.date + "-01") for all years except last year
        // for last year, pad with some extra days so that vertical line can reach it
        if (d.isLast) {
          var lastDate = new Date(d.date);
          var addDays = 31;
          return lastDate.setDate(lastDate.getDate() + addDays);
        } else {
          return new Date(d.date + "-01");
        }
      },
      getText: function getText(d) {
        return d.date;
      },
      ticks: 7 * 6,
      tickSizeOuter: 0
    },
    y: {
      label: i18next.t("y_label", {
        ns: "airMajorAirports"
      }),
      getLabel: function getLabel() {
        return i18next.t("y_label", {
          ns: "airMajorAirports"
        });
      },
      getValue: function getValue(d, key) {
        if (d[key] === "x" || d[key] === "..") {
          return 0;
        } else return Number(d[key]);
      },
      getTotal: function getTotal(d, index, data) {
        var total;
        var keys;
        var sett = this;

        if (!d[sett.y.totalProperty]) {
          keys = sett.z.getKeys.call(sett, data);
          total = 0;

          for (var k = 0; k < keys.length; k++) {
            total += sett.y.getValue.call(sett, d, keys[k], data);
          }

          d[sett.y.totalProperty] = total;
        }

        return d[sett.y.totalProperty];
      },
      getText: function getText(d, key) {
        // if (d[key]=== "x" || d[key]=== "..") {
        //   return d[key];
        // } else return Number(d[key]);
        return isNaN(Number(d[key])) ? d[key] : this.formatNum(Number(d[key]));
      },
      getTickText: function getTickText(d) {
        return this.formatNum(d);
      },
      ticks: 5,
      tickSizeOuter: 0
    },
    z: {
      // label: i18next.t("z_label", {ns: "airMajorAirports"}),
      getId: function getId(d) {
        if (d.key !== "isLast") {
          return d.key;
        }
      },
      getKeys: function getKeys(object) {
        var sett = this;
        var keys = Object.keys(object[0]);
        keys.splice(keys.indexOf("date"), 1);

        if (keys.indexOf(sett.y.totalProperty) !== -1) {
          keys.splice(keys.indexOf(sett.y.totalProperty), 1);
        }

        if (keys.indexOf("isLast") !== -1) {
          // temporary key to be removed
          keys.splice(keys.indexOf("isLast"), 1);
        }

        return keys;
      },
      origData: function origData(data) {
        var dataClone = JSON.parse(JSON.stringify(data)); // Format date as monthName YYYY

        dataClone.filter(function (item) {
          item.date = "".concat(i18next.t(item.date.substring(5, 7), {
            ns: "months"
          }), " ").concat(item.date.substring(0, 4));
        });
        return dataClone;
      },
      getClass: function getClass() {
        for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
          args[_key] = arguments[_key];
        }

        return this.z.getId.apply(this, args);
      },
      getText: function getText(d) {
        return i18next.t(d.key, {
          ns: "airMajorAirports"
        });
      }
    },
    datatable: true,
    dataTableTotal: true,
    // show total in data table
    areaTableID: "areaTable",
    // summaryId: "chrt-dt-tbl",
    transition: true,
    width: 1050
  };

  var baseDateFilter$1 = function baseDateFilter(data) {
    var minDate = new Date("2010");
    var newData = [];

    for (var s = 0; s < data.length; s++) {
      var date = new Date(data[s].date);

      if (date >= minDate) {
        newData.push(data[s]);
      }
    }

    return newData;
  };

  function mapColourScaleFn (svgCB, colourArray, dimExtent, numLevels, settings) {
    // Definitions
    // ---------------------------------------------------------------------------
    var rectDim = 35;
    var yRect = 20;
    var yText = 65;
    var yNaNText = yText + 7; // text labels (calculate cbValues)

    var delta = (dimExtent[1] - dimExtent[0]) / numLevels;
    var cbValues = [];
    cbValues[0] = dimExtent[0];

    for (var idx = 1; idx < numLevels; idx++) {
      cbValues.push(Math.round(idx * delta + dimExtent[0]));
    } // rect fill fn


    var getFill = function getFill(d, i) {
      return colourArray[i];
    }; // text fn


    var getText = function getText(i, j) {
      if (i < numLevels) {
        var s0 = settings.formatNum(cbValues[j]);

        if (numLevels === 1) {
          return s0;
        } else {
          return s0 + "+";
        }
      } else if (i === numLevels + 1) {
        return "x";
      }
    }; // tooltip for NaN box


    var divNaN = d3.select("body").append("div").attr("class", "tooltip").style("opacity", 0); // -----------------------------------------------------------------------------
    // g node for colourbar title (text is set in index.js)

    svgCB.append("g").attr("class", "colourbarTitle").attr("id", "cbTitle").append("text") // .text("test")
    .attr("transform", function (d, i) {
      return "translate(225, 15)";
    }).style("display", function () {
      return "inline";
    }); // Create the umbrella group

    var rectGroups = svgCB.attr("class", "mapCB").selectAll(".legend").data(Array.from(Array(colourArray.length).keys())); // Append g nodes (to be filled with a rect and a text) to umbrella group

    var newGroup = rectGroups.enter().append("g").attr("class", "legend").attr("id", function (d, i) {
      return "cb".concat(i);
    }); // add rects

    newGroup.append("rect").attr("width", rectDim).attr("height", rectDim).attr("y", yRect).attr("x", function (d, i) {
      return 135 + i * rectDim;
    }).attr("fill", getFill).attr("class", function (d, i) {
      if (i === numLevels + 1) {
        return "classNaN";
      }

      if (numLevels === 1) {
        return "zeroValue";
      }
    }); // hover over NaN rect only

    newGroup.selectAll(".legend rect").on("mouseover", function (d, i) {
      if (d3.select(this).attr("class") === "classNaN") {
        var line1 = i18next.t("NaNhover1", {
          ns: "airUI"
        });
        var line2 = i18next.t("NaNhover2", {
          ns: "airUI",
          escapeInterpolation: false
        });
        divNaN.style("opacity", 0.9).html("<br>" + line1 + "<br>" + line2 + "<br><br>").style("left", d3.event.pageX + 10 + "px").style("top", d3.event.pageY + 10 + "px");
      }
    }).on("mouseout", function () {
      divNaN.style("opacity", 0);
    }); // add text

    newGroup.append("text").text(getText).attr("text-anchor", "end").attr("transform", function (d, i) {
      if (i < numLevels) {
        return "translate(".concat(140 + i * (rectDim + 0), ", ").concat(yText, ") rotate(-45)");
      } else if (i === numLevels + 1) {
        // NaN box in legend
        return "translate(".concat(156 + i * (rectDim + 0), ", ").concat(yNaNText, ") ");
      }
    }).style("display", function () {
      return "inline";
    }); // Update rect fill for any new colour arrays passed in

    rectGroups.select("rect").attr("fill", getFill); // Update rect text for different year selections

    rectGroups.select("text").text(getText); // hack to get color bar cetered when value is 0

    if (numLevels === 1) {
      d3.select("#cb0").attr("transform", "translate(73,0)");
    } else {
      d3.select("#cb0").attr("transform", "translate(0,0)");
    }

    rectGroups.exit().remove();
  }

  function fillMapFn (data, colourArray, numLevels) {
    var nullColour = "#565656"; // data is an Array

    var thisData = data[0]; // Object

    var dimExtent = [];
    var totArray = [];
    totArray = Object.values(thisData);
    totArray.sort(function (a, b) {
      return a - b;
    });
    dimExtent = d3.extent(totArray); // colour map to take data value and map it to the colour of the level bin it belongs to

    var colourMap = d3.scaleQuantize().domain([dimExtent[0], dimExtent[1]]).range(colourArray.slice(0, numLevels));

    var _loop = function _loop(key) {
      if (thisData.hasOwnProperty(key)) {
        d3.select(".dashboard .map").select("." + key).style("fill", function () {
          return Number(thisData[key]) ? colourMap(thisData[key]) : nullColour;
        }); // Bruno : Unused code removed following no data message modification
      }
    };

    for (var key in thisData) {
      _loop(key);
    }

    return dimExtent;
  }

  function areaTooltip (settings, div, d) {
    var thisMonth = d.date.substring(5, 7) ? i18next.t(d.date.substring(5, 7), {
      ns: "months"
    }) : null;
    var thisYear = d.date.substring(0, 4);
    var line1 = thisMonth ? "".concat(i18next.t("hoverTitle", {
      ns: settings.ns
    }), ", ").concat(thisMonth, " ").concat(thisYear, ": ") : "".concat(i18next.t("hoverTitle", {
      ns: settings.ns
    }), ", ").concat(d.date, ": ");
    var keys = Object.keys(d); // remove unwanted keys

    keys.splice(keys.indexOf("date"), 1);

    if (keys.indexOf("total") !== -1) {
      keys.splice(keys.indexOf("total"), 1);
    }

    if (keys.indexOf("isLast") !== -1) {
      keys.splice(keys.indexOf("isLast"), 1);
    }

    var makeTable = function makeTable(line1) {
      var keyValues = [];

      for (var idx = 0; idx < keys.length; idx++) {
        var key = keys[idx];
        keyValues.push(settings.y.getText.call(settings, d, key));
      }

      var rtnTable = "<b>".concat(line1, "</b><br><br><table>");

      for (var _idx = 0; _idx < keys.length; _idx++) {
        rtnTable = rtnTable.concat("<tr><td><b>".concat(i18next.t(keys[_idx], {
          ns: settings.ns
        }), "</b>: ").concat(keyValues[_idx], "</td></tr>"));
      }

      rtnTable = rtnTable.concat("</table>");
      return rtnTable;
    };

    div.html(makeTable(line1)).style("opacity", .9).style("left", d3.event.pageX + 10 + "px").style("top", d3.event.pageY + 10 + "px").style("pointer-events", "none");
  }

  function createOverlay (chartObj, data, onMouseOverCb, onMouseOutCb) {
    // TEMP
    chartObj.svg.datum(chartObj);
    chartObj.data = data;
    var bisect = d3.bisector(function (d) {
      return chartObj.settings.x.getValue(d);
    }).left;
    var overlay = chartObj.svg.select(".data .overlay");
    var rect;
    var line;

    if (overlay.empty()) {
      overlay = chartObj.svg.select(".data").append("g").attr("class", "overlay");
      rect = overlay.append("rect").style("fill", "none").style("pointer-events", "all").attr("class", "overlay");
      line = overlay.append("line").attr("class", "hoverLine").style("display", "inline").style("visibility", "hidden");
    } else {
      rect = overlay.select("rect");
      line = overlay.select("line");
    }

    rect.attr("width", chartObj.settings.innerWidth).attr("height", chartObj.settings.innerHeight).on("mousemove", function (e) {
      var chartObj = d3.select(this.ownerSVGElement).datum();
      var x = d3.mouse(this)[0];
      var xD = chartObj.x.invert(x);
      var i = bisect(chartObj.data, xD);
      var d0 = chartObj.data[i - 1];
      var d1 = chartObj.data[i];
      var d;

      if (d0 && d1) {
        d = xD - chartObj.settings.x.getValue(d0) > chartObj.settings.x.getValue(d1) - xD ? d1 : d0;
      } else if (d0) {
        d = d0;
      } else {
        d = d1;
      }

      line.attr("x1", chartObj.x(chartObj.settings.x.getValue(d)));
      line.attr("x2", chartObj.x(chartObj.settings.x.getValue(d)));
      line.style("visibility", "visible");

      if (onMouseOverCb && typeof onMouseOverCb === "function") {
        onMouseOverCb(d);
      }
    }).on("mouseout", function () {
      line.style("visibility", "hidden");

      if (onMouseOutCb && typeof onMouseOutCb === "function") {
        onMouseOutCb();
      }
    });
    line.attr("x1", 0).attr("x2", 0).attr("y1", 0).attr("y2", chartObj.settings.innerHeight);
  }

  function dropdownCheck (yearId, monthId, dateRange, selectedYear, selectedMonth, months) {
    var yearDropdown = $(yearId); // date dropdown creation

    yearDropdown.empty();

    if (months) {
      for (var i = Number(dateRange.min.substring(0, 4)); i <= Number(dateRange.max.substring(0, 4)); i++) {
        yearDropdown.append($("<option></option>").attr("value", i).html(i));
      }

      d3.select(yearId)._groups[0][0].value = selectedYear;
      var maxMonth = Number(dateRange.max.substring(5, 7));
      var maxYear = Number(dateRange.max.substring(0, 4)); // Disable months in dropdown menu that do not exist for selectedYear

      if (Number(selectedYear) === maxYear) {
        $("".concat(monthId, " > option")).each(function () {
          if (Number(this.value) > maxMonth) {
            this.disabled = true;
          }
        });
        var currentMonth = Number(d3.select(monthId)._groups[0][0].value);

        if (currentMonth > maxMonth) {
          selectedMonth = dateRange.max.substring(5, 7);
          d3.select(monthId)._groups[0][0].value = selectedMonth;
        }
      } else {
        // Enable all months
        d3.selectAll("".concat(monthId, " > option")).property("disabled", false);
      }
    } else {
      for (var _i = Number(dateRange.min); _i <= Number(dateRange.max); _i++) {
        yearDropdown.append($("<option></option>").attr("value", _i).html(_i));
      }

      d3.select(yearId)._groups[0][0].value = selectedYear;
    }

    return selectedMonth;
  }

  var webAPI = "https://www150.statcan.gc.ca/t1/wds/rest/getDataFromCubePidCoordAndLatestNPeriods";
  function dateRangeFn (minYear, frequency, productID, defaultCoord, granularity) {
    return new Promise(function (resolve, reject) {
      var mostRecentDate;
      var numberOfPeriods;
      var returnObject = {};
      var myData = [{
        "productId": productID,
        "coordinate": defaultCoord,
        "latestN": 1
      }];
      $.ajax({
        type: "post",
        url: webAPI,
        data: JSON.stringify(myData),
        dataType: "json",
        contentType: "application/json",
        success: function success(data, textStatus, jQxhr) {
          if (granularity === "year") {
            mostRecentDate = data[0].object.vectorDataPoint[0].refPer.substring(0, 4);
            numberOfPeriods = (Number(mostRecentDate) - minYear + 1) * frequency;
            returnObject.max = mostRecentDate;
            returnObject.numPeriods = numberOfPeriods;
          } else if (granularity === "month") {
            mostRecentDate = data[0].object.vectorDataPoint[0].refPer.substring(0, 7);
            numberOfPeriods = (Number(mostRecentDate.substring(0, 4)) - minYear) * frequency + Number(mostRecentDate.substring(5, 7));
            returnObject.max = mostRecentDate;
            returnObject.numPeriods = numberOfPeriods;
          }

          resolve(returnObject);
        },
        error: function error(jqXhr, textStatus, errorThrown) {
          reject(errorThrown);
        }
      });
    });
  }

  var PassengerId = 23100253;
  var TotalCoordinate = 1;
  var EnplanedCoordinate = 2;
  var DeplanedCoordinate = 3;
  var DomesticCoordinate = 5;
  var TransborderCoordinate = 6;
  var OtherIntCoordinate = 7;
  var webAPI$1 = "https://www150.statcan.gc.ca/t1/wds/rest/getDataFromCubePidCoordAndLatestNPeriods";
  var numToGeographyPassenger = {
    100000: "CANADA",
    101000: "NL",
    102000: "PE",
    103000: "NS",
    104000: "NB",
    105000: "QC",
    106000: "ON",
    107000: "MB",
    108000: "SK",
    109000: "AB",
    110000: "BC",
    111000: "YT",
    112000: "NT",
    113000: "NU"
  };
  var numToAirportPassenger = {
    72: "YDF",
    27: "YQX",
    117: "YYT",
    63: "YYG",
    21: "YHZ",
    37: "YFC",
    91: "YQM",
    87: "YUL",
    141: "YQB",
    120: "YZV",
    23: "YHM",
    96: "YXU",
    76: "YOW",
    179: "YSB",
    178: "YQT",
    161: "YYZ",
    160: "YQG",
    180: "YTS",
    163: "YWG",
    136: "YQR",
    126: "YXE",
    13: "YYC",
    52: "YEG",
    43: "YMM",
    1: "YXX",
    68: "YXC",
    36: "YXJ",
    100: "YLW",
    153: "YXS",
    169: "YVR",
    171: "YYJ",
    187: "YZF"
  };
  var numToIndicator = {
    1: "total",
    2: "enplaned",
    3: "deplaned",
    5: "domestic",
    6: "transborder",
    7: "international"
  };
  var statusCodes = {
    1: "..",
    2: "0s",
    3: "A",
    4: "B",
    5: "C",
    6: "D",
    7: "E",
    8: "F"
  };
  var qi_F = 8;
  function passengerApiCall (selectedDateRange) {
    return new Promise(function (resolve, reject) {
      // get coordinates for data
      var coordinateArray = [];
      coordinateArray = coordinateTranslate();
      var yearRange = Number(selectedDateRange.max) - Number(selectedDateRange.min) + 1;
      var returnObject = {};
      var myData = [];

      for (var i = 0; i < coordinateArray.length; i++) {
        myData.push({
          "productId": PassengerId,
          "coordinate": coordinateArray[i],
          "latestN": yearRange
        });
      }

      $.support.cors = true;
      $.ajax({
        type: "post",
        url: webAPI$1,
        data: JSON.stringify(myData),
        dataType: "json",
        contentType: "application/json",
        success: function success(data, textStatus, jQxhr) {
          returnObject = rebuild(data, selectedDateRange);
          resolve(returnObject);
        },
        error: function error(jqXhr, textStatus, errorThrown) {
          reject(errorThrown);
        }
      });
    });
  }

  function rebuild(data, selectedDateRange, selectedRegion) {
    var dataByProvince = {};
    var dataByAirport = {};
    var provinceObject = {};
    var airortForAreaChartObject = {};
    var airportObject = {};
    var returnObject = {};

    for (var i = 0; i < data.length; i++) {
      var code = data[i].object.coordinate.split(".", 1);

      if (numToGeographyPassenger.hasOwnProperty(code)) {
        if (!dataByProvince.hasOwnProperty(numToGeographyPassenger[code])) {
          dataByProvince[numToGeographyPassenger[code]] = [];
        }

        dataByProvince[numToGeographyPassenger[code]].push(data[i]);
      } else {
        if (!dataByAirport.hasOwnProperty(numToAirportPassenger[code])) {
          dataByAirport[numToAirportPassenger[code]] = [];
        }

        dataByAirport[numToAirportPassenger[code]].push(data[i]);
      }
    }

    for (var province in dataByProvince) {
      if (Object.prototype.hasOwnProperty.call(dataByProvince, province)) {
        var rebuiltData = rebuildData(dataByProvince[province], selectedDateRange);
        var dateOrganizedArray = [];

        for (var entry in rebuiltData) {
          rebuiltData[entry].date = entry;
          dateOrganizedArray.push(rebuiltData[entry]);
        }

        provinceObject[province] = dateOrganizedArray;
      }
    }

    for (var airport in dataByAirport) {
      if (Object.prototype.hasOwnProperty.call(dataByAirport, airport)) {
        var _rebuiltData = rebuildData(dataByAirport[airport], selectedDateRange);

        var _dateOrganizedArray = [];

        for (var _entry in _rebuiltData) {
          _rebuiltData[_entry].date = _entry;

          _dateOrganizedArray.push(_rebuiltData[_entry]);
        }

        airortForAreaChartObject[airport] = removeEnplanedDeplaned(_dateOrganizedArray);
        airportObject[airport] = _dateOrganizedArray;
      }
    }

    returnObject.provinces = _objectSpread2(_objectSpread2({}, provinceObject), airortForAreaChartObject);
    returnObject.airports = airportObject;
    return returnObject;
  }

  function rebuildData(data, selectedDateRange) {
    var returnObject = {};

    for (var i = 0; i < selectedDateRange.numPeriods; i++) {
      for (var j in data) {
        var datapoint = data[j].object.vectorDataPoint[i];
        var date = datapoint.refPer.substring(0, 7);

        if (!returnObject.hasOwnProperty(date)) {
          returnObject[date] = {};
        }

        var indicator = data[j].object.coordinate.split(".", 2)[1];

        if (datapoint.value == null) {
          returnObject[date][numToIndicator[indicator]] = "x";
        } else if (datapoint.statusCode != 1 && datapoint.securityLevelCode == 0 && datapoint.statusCode != qi_F) {
          returnObject[date][numToIndicator[indicator]] = datapoint.value;
        } else {
          returnObject[date][numToIndicator[indicator]] = statusCodes[datapoint.statusCode];
        }
      }
    }

    return returnObject;
  }

  function coordinateTranslate() {
    var returnArray = [];

    for (var i in numToGeographyPassenger) {
      returnArray.push("".concat(i, ".").concat(TotalCoordinate, ".0.0.0.0.0.0.0.0"));
      returnArray.push("".concat(i, ".").concat(DomesticCoordinate, ".0.0.0.0.0.0.0.0"));
      returnArray.push("".concat(i, ".").concat(TransborderCoordinate, ".0.0.0.0.0.0.0.0"));
      returnArray.push("".concat(i, ".").concat(OtherIntCoordinate, ".0.0.0.0.0.0.0.0"));
    }

    for (var _i in numToAirportPassenger) {
      returnArray.push("".concat(_i, ".").concat(TotalCoordinate, ".0.0.0.0.0.0.0.0"));
      returnArray.push("".concat(_i, ".").concat(DomesticCoordinate, ".0.0.0.0.0.0.0.0"));
      returnArray.push("".concat(_i, ".").concat(EnplanedCoordinate, ".0.0.0.0.0.0.0.0"));
      returnArray.push("".concat(_i, ".").concat(DeplanedCoordinate, ".0.0.0.0.0.0.0.0"));
      returnArray.push("".concat(_i, ".").concat(TransborderCoordinate, ".0.0.0.0.0.0.0.0"));
      returnArray.push("".concat(_i, ".").concat(OtherIntCoordinate, ".0.0.0.0.0.0.0.0"));
    }

    return returnArray;
  }

  function removeEnplanedDeplaned(inputArray) {
    var returnArray = [];

    for (var i in inputArray) {
      var returnObject = {};

      for (var j in inputArray[i]) {
        if (j !== "enplaned" && j !== "deplaned") {
          returnObject[j] = inputArray[i][j];
        }
      }

      returnArray.push(returnObject);
    }

    return returnArray;
  }

  var MajorStationsId = 23100015;
  var MajorTowersId = 23100008;
  var DomesticCoordinate$1 = 1;
  var TransborderCoordinate$1 = 2;
  var OtherIntCoordinate$1 = 3;
  var Stations = "stations";
  var Towers = "towers";
  var webAPI$2 = "https://www150.statcan.gc.ca/t1/wds/rest/getDataFromCubePidCoordAndLatestNPeriods";
  var returnObject = {};
  var stationsData = {};
  var towersData = {};
  var numToGeographyMajorStations = {
    "2": "YBR",
    "3": "YBL",
    "4": "YCG",
    "5": "YYG",
    "6": "YYQ",
    "7": "YXC",
    "9": "YDF",
    "11": "YYE",
    "12": "YXJ",
    "14": "YQU",
    "15": "YOJ",
    "16": "YGR",
    "17": "YEV",
    "18": "YFB",
    "19": "YKA",
    "20": "YQK",
    "21": "YGK",
    "22": "YVP",
    "24": "YGL",
    "25": "YVC",
    "26": "YQL",
    "27": "YLL",
    "28": "YXH",
    "29": "YYY",
    "30": "YCD",
    "33": "YND",
    "34": "YPE",
    "35": "YYF",
    "36": "YZT",
    "37": "YPA",
    "39": "YRT",
    "40": "YQF",
    "42": "YUY",
    "43": "YSJ",
    "45": "YXL",
    "46": "YYD",
    "48": "YCM",
    "50": "YXT",
    "51": "YTH",
    "52": "YTS",
    "53": "YVO",
    "54": "YWH",
    "55": "YWK",
    "56": "YZU",
    "57": "YWL"
  };
  var numToGeographyMajorTowers = {
    "2": "YXX",
    "3": "YDT",
    "4": "YYC",
    "5": "1YSB",
    "6": "YRC",
    "8": "YEG",
    "9": "CZVL",
    "46": "YMM",
    "47": "YFC",
    "10": "YQX",
    "11": "YHZ",
    "12": "YHM",
    "13": "YLW",
    "14": "YKF",
    "15": "YLY",
    "16": "YXU",
    "17": "YQM",
    "19": "YUL",
    "20": "YHU",
    "21": "YOO",
    "22": "YOW",
    "23": "YPK",
    "24": "YXS",
    "25": "YQB",
    "26": "YQR",
    "27": "YXE",
    "28": "YAM",
    "29": "YYT",
    "30": "YJN",
    "32": "YQT",
    "34": "YTZ",
    "35": "YYZ",
    "36": "CXH",
    "37": "YVR",
    "38": "YYJ",
    "39": "YXY",
    "40": "YQG",
    "41": "YWG",
    "42": "2DCI",
    "43": "YZF"
  };
  var numToIndicator$1 = {
    1: "domestic",
    2: "transborder",
    3: "international"
  };
  var statusCodes$1 = {
    1: "..",
    2: "0s",
    3: "A",
    4: "B",
    5: "C",
    6: "D",
    7: "E",
    8: "F"
  };
  var qi_F$1 = 8;
  function majorApiCall (selectedDateRange) {
    return new Promise(function (resolve, reject) {
      apiCall(Stations, selectedDateRange).then(function (majorData) {
        resolve(majorData);
      });
    });
  }

  function apiCall(product, selectedDateRange) {
    return new Promise(function (resolve, reject) {
      // get coordinates for data
      var productID;
      var coordinateArray = [];
      var majorCallData = [];

      if (product === Stations) {
        productID = MajorStationsId;
        coordinateArray = coordinateTranslate$1("stations");
      } else {
        productID = MajorTowersId;
        coordinateArray = coordinateTranslate$1("towers");
      }

      for (var i = 0; i < coordinateArray.length; i++) {
        majorCallData.push({
          "productId": productID,
          "coordinate": coordinateArray[i],
          "latestN": selectedDateRange.numPeriods
        });
      }

      $.support.cors = true;
      $.ajax({
        type: "post",
        url: webAPI$2,
        data: JSON.stringify(majorCallData),
        dataType: "json",
        contentType: "application/json",
        success: function success(returnData, textStatus, jQxhr) {
          // do call for stations first then towers, after towers is done return to the application
          if (product === Stations) {
            stationsData = rebuild$1(returnData, selectedDateRange, product);
            apiCall(Towers, selectedDateRange).then(function (completeData) {
              resolve(completeData);
            });
          } else {
            towersData = rebuild$1(returnData, selectedDateRange, product);
            returnObject = _objectSpread2(_objectSpread2({}, stationsData), towersData);
            resolve(returnObject);
          }
        },
        error: function error(jqXhr, textStatus, errorThrown) {
          reject(errorThrown);
        }
      });
    });
  }

  function rebuild$1(data, selectedDateRange, product) {
    var dataByAirport = {};
    var airportObject = {};
    var returnObject = {};

    for (var i = 0; i < data.length; i++) {
      var code = data[i].object.coordinate.split(".", 2)[1];

      if (product == Towers) {
        if (!dataByAirport.hasOwnProperty(numToGeographyMajorTowers[code])) {
          dataByAirport[numToGeographyMajorTowers[code]] = [];
        }

        dataByAirport[numToGeographyMajorTowers[code]].push(data[i]);
      } else {
        if (!dataByAirport.hasOwnProperty(numToGeographyMajorStations[code])) {
          dataByAirport[numToGeographyMajorStations[code]] = [];
        }

        dataByAirport[numToGeographyMajorStations[code]].push(data[i]);
      }
    }

    for (var airport in dataByAirport) {
      if (Object.prototype.hasOwnProperty.call(dataByAirport, airport)) {
        var rebuiltData = rebuildData$1(dataByAirport[airport], selectedDateRange);
        var dateOrganizedArray = [];

        for (var entry in rebuiltData) {
          rebuiltData[entry].date = entry;
          dateOrganizedArray.push(rebuiltData[entry]);
        }

        airportObject[airport] = dateOrganizedArray;
      }
    }

    returnObject = airportObject;
    return returnObject;
  }

  function rebuildData$1(data, selectedDateRange) {
    var returnObject = {};

    for (var i = 0; i < selectedDateRange.numPeriods; i++) {
      for (var j in data) {
        var datapoint = data[j].object.vectorDataPoint[i];
        var date = datapoint.refPer.substring(0, 7);

        if (!returnObject.hasOwnProperty(date)) {
          returnObject[date] = {};
        }

        var indicator = data[j].object.coordinate.split(".", 3)[2];

        if (datapoint.value == null) {
          returnObject[date][numToIndicator$1[indicator]] = "x";
        } else if (datapoint.statusCode != 1 && datapoint.securityLevelCode == 0 && datapoint.statusCode != qi_F$1) {
          returnObject[date][numToIndicator$1[indicator]] = datapoint.value;
        } else {
          returnObject[date][numToIndicator$1[indicator]] = statusCodes$1[datapoint.statusCode];
        }
      }
    }

    return returnObject;
  }

  function coordinateTranslate$1(product) {
    var returnArray = [];

    if (product == "towers") {
      for (var i in numToGeographyMajorTowers) {
        returnArray.push("1.".concat(i, ".").concat(DomesticCoordinate$1, ".1.0.0.0.0.0.0"));
        returnArray.push("1.".concat(i, ".").concat(TransborderCoordinate$1, ".1.0.0.0.0.0.0"));
        returnArray.push("1.".concat(i, ".").concat(OtherIntCoordinate$1, ".1.0.0.0.0.0.0"));
      }
    } else {
      for (var _i in numToGeographyMajorStations) {
        returnArray.push("1.".concat(_i, ".").concat(DomesticCoordinate$1, ".1.0.0.0.0.0.0"));
        returnArray.push("1.".concat(_i, ".").concat(TransborderCoordinate$1, ".1.0.0.0.0.0.0"));
        returnArray.push("1.".concat(_i, ".").concat(OtherIntCoordinate$1, ".1.0.0.0.0.0.0"));
      }
    }

    return returnArray;
  }

  var PassengerData = "passengers";
  function apiCall$1 (selectedDateRange, selectedDataset) {
    return new Promise(function (resolve, reject) {
      if (selectedDataset === PassengerData) {
        passengerApiCall(selectedDateRange).then(function (newData) {
          resolve(newData);
        });
      } else {
        majorApiCall(selectedDateRange).then(function (newData) {
          resolve(newData);
        });
      }
    });
  }

  var CopyButton = /*#__PURE__*/function () {
    function CopyButton(pNode, options) {
      _classCallCheck(this, CopyButton);

      this.pNode = pNode ? pNode : document.createElement("div");
      this.options = options ? options : {};
      this.nodes = {};
      this.data = null;
      this.instanceNumber = ++CopyButton.n;
      this["class"] = this.options["class"] || "";
      /* this.data = shall be an array (i.e called rowsArray) of arrays (i.e each is called row).
        each array on rowsArray represents a row on the table.
        this.data must be set/updated by the code that uses this button
        [
          ["title"]
          ["columna1" , "columna2" ,..., "columnaN"]
          ["value1Row1", "value2Row1",..., "valueNRowN"]
          ["value1Row2", "value2Row2",..., "valueNRowN"]
        ]
      */
    }

    _createClass(CopyButton, [{
      key: "build",
      value: function build(options) {
        if (options) this.options = options; // workAround;

        if (options.pNode) this.pNode = options.pNode; // workAround;

        if (options["class"]) this["class"] = options["class"]; // workAround;

        this.root = document.createElement("div");
        this.root.setAttribute("class", "copy-button button-" + this.instanceNumber + " " + this["class"]);
        this.pNode.appendChild(this.root);
        this.nodes.btnCopy = document.createElement("button");
        this.nodes.btnCopy.setAttribute("type", "button");
        this.nodes.btnCopy.setAttribute("class", "btn btn-primary copy button-" + this.instanceNumber + " " + this["class"]);
        this.nodes.btnCopy.setAttribute("title", this.options.title || "");
        this.root.appendChild(this.nodes.btnCopy);
        var icon = document.createElement("span");
        icon.setAttribute("class", "fa fa-clipboard clipboard button-" + this.instanceNumber + " " + this["class"]);
        this.nodes.btnCopy.appendChild(icon);
        var accessibility = document.createElement("span");
        accessibility.setAttribute("class", "wb-inv button-" + this.instanceNumber + " " + this["class"]);
        accessibility.innerHTML = this.options.accessibility || "";
        this.nodes.btnCopy.appendChild(accessibility);
        this.nodes.msgCopyConfirm = document.createElement("div");
        this.nodes.msgCopyConfirm.setAttribute("class", "copy-confirm button-" + this.instanceNumber + " " + this["class"]);
        this.nodes.msgCopyConfirm.setAttribute("aria-live", "polite");
        this.nodes.msgCopyConfirm.innerHTML = this.options.msgCopyConfirm || "";
        this.root.appendChild(this.nodes.msgCopyConfirm);
        this.nodes.btnCopy.addEventListener("click", this.onBtnClick.bind(this));
      }
    }, {
      key: "onBtnClick",
      value: function onBtnClick(ev) {
        this.copyData(this.data);
      }
    }, {
      key: "copyData",
      value: function copyData(lines) {
        var linesTemp = lines ? lines : [];
        this.clipboard(this.toCSV("\t", linesTemp), this.root);
        this.fade(this.nodes.msgCopyConfirm, true);
        setTimeout(function (ev) {
          this.fade(this.nodes.msgCopyConfirm, false);
        }.bind(this), 1500);
      }
    }, {
      key: "toCSV",
      value: function toCSV(separator, lines) {
        var csv = lines.map(function (line) {
          return line.join(separator);
        });
        return csv.join("\r\n");
      }
    }, {
      key: "clipboard",
      value: function clipboard(string, target) {
        if (this.isIE()) window.clipboardData.setData("Text", string);else {
          // Copying the string
          var aux = document.createElement("textarea");
          aux.textContent = string;
          target.appendChild(aux);
          aux.select();
          document.execCommand("copy");
          target.removeChild(aux);
        }
      }
    }, {
      key: "fade",
      value: function fade(node, visible) {
        var clss = ["copy-confirm button-" + this.instanceNumber + " " + this["class"]];
        var add = visible ? "fadeIn" : "fadeOut";
        clss.push(add);
        node.setAttribute("class", clss.join(" "));
      } // work around for when tables are destroyed

    }, {
      key: "appendTo",
      value: function appendTo(pNode) {
        this.pNode = pNode;
        this.pNode.appendChild(this.root);
        this.nodes.msgCopyConfirm.setAttribute("class", "copy-confirm button-" + this.instanceNumber + " " + this["class"]);
      }
    }, {
      key: "isIE",
      value: function isIE() {
        var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE ");

        if (msie > 0) {
          // IE 10 or older => return version number
          return parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)), 10);
        }

        var trident = ua.indexOf("Trident/");

        if (trident > 0) {
          // IE 11 => return version number
          var rv = ua.indexOf("rv:");
          return parseInt(ua.substring(rv + 3, ua.indexOf(".", rv)), 10);
        }

        var edge = ua.indexOf("Edge/");

        if (edge > 0) {
          // Edge (IE 12+) => return version number
          return parseInt(ua.substring(edge + 5, ua.indexOf(".", edge)), 10);
        } // other browser


        return false;
      }
    }]);

    return CopyButton;
  }();
  CopyButton.n = 0;

  var timeoutTime = 3000;
  var PassengerId$1 = 23100253;
  var MajorTowersId$1 = 23100008;
  var PassengerData$1 = "passengers";
  var MajorAirData = "major_airports"; // flag to say if the api has been called to load the major airport data yet

  var majorDataLoadedFlag = false;
  /* Copy Button */
  // -----------------------------------------------------------------------------

  var cButton = new CopyButton(); // -----------------------------------------------------------------------------

  var xlabelDY = 1.5; // spacing between areaChart xlabels and ticks
  // Add number formatter to stackedArea settings file

  var settingsAux = {
    _selfFormatter: i18n.getNumberFormatter(0),
    formatNum: function formatNum() {
      for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
        args[_key] = arguments[_key];
      }

      return this._selfFormatter.format(args);
    }
  };

  var settings = _objectSpread2(_objectSpread2({}, settingsInit), settingsAux);

  var settingsMajorAirports = _objectSpread2(_objectSpread2({}, settingsMajorAirportsInit), settingsAux);

  var data = {
    "passengers": {},
    "major_airports": {}
  };
  var passengerDropdownData = [{
    "code": "CANADA",
    "type": "geo"
  }, {
    "code": "NL",
    "type": "geo"
  }, {
    "code": "YYT",
    "type": "airport"
  }, {
    "code": "PE",
    "type": "geo"
  }, {
    "code": "NS",
    "data": "no",
    "type": "geo"
  }, {
    "code": "YHZ",
    "type": "airport"
  }, {
    "code": "NB",
    "data": "no",
    "type": "geo"
  }, {
    "code": "YQM",
    "type": "airport"
  }, {
    "code": "QC",
    "type": "geo"
  }, {
    "code": "YUL",
    "type": "airport"
  }, {
    "code": "YQB",
    "type": "airport"
  }, {
    "code": "ON",
    "type": "geo"
  }, {
    "code": "YOW",
    "type": "airport"
  }, {
    "code": "YYZ",
    "type": "airport"
  }, {
    "code": "MB",
    "type": "geo"
  }, {
    "code": "YWG",
    "type": "airport"
  }, {
    "code": "SK",
    "type": "geo"
  }, {
    "code": "AB",
    "type": "geo"
  }, {
    "code": "YYC",
    "type": "airport"
  }, {
    "code": "YEG",
    "type": "airport"
  }, {
    "code": "BC",
    "type": "geo"
  }, {
    "code": "YVR",
    "type": "airport"
  }, {
    "code": "YYJ",
    "type": "airport"
  }, {
    "code": "YT",
    "data": "no",
    "type": "geo"
  }, {
    "code": "NT",
    "type": "geo"
  }, {
    "code": "NU",
    "type": "geo"
  }];
  var majorDropdownData = [{
    "code": "YYT",
    "type": "airport"
  }, {
    "code": "YYG",
    "type": "airport"
  }, {
    "code": "YHZ",
    "type": "airport"
  }, {
    "code": "YQM",
    "type": "airport"
  }, {
    "code": "YFC",
    "type": "airport"
  }, {
    "code": "YSJ",
    "type": "airport"
  }, {
    "code": "YUL",
    "type": "airport"
  }, {
    "code": "YQB",
    "type": "airport"
  }, {
    "code": "YOW",
    "type": "airport"
  }, {
    "code": "YYZ",
    "type": "airport"
  }, {
    "code": "YQT",
    "type": "airport"
  }, {
    "code": "YXU",
    "type": "airport"
  }, {
    "code": "YWG",
    "type": "airport"
  }, {
    "code": "YQR",
    "type": "airport"
  }, {
    "code": "YXE",
    "type": "airport"
  }, {
    "code": "YYC",
    "type": "airport"
  }, {
    "code": "YEG",
    "type": "airport"
  }, {
    "code": "YVR",
    "type": "airport"
  }, {
    "code": "YYJ",
    "type": "airport"
  }, {
    "code": "YLW",
    "type": "airport"
  }, {
    "code": "YXS",
    "type": "airport"
  }, {
    "code": "YXY",
    "type": "airport"
  }, {
    "code": "YZF",
    "type": "airport"
  }, {
    "code": "YFB",
    "type": "airport"
  }]; // because some airports dont have data all the time passenger is limited to these

  var activePassengerAirports = ["YDF", "YEG", "YFC", "YHM", "YHZ", "YLW", "YMM", "YOW", "YQB", "YQG", "YQM", "YQR", "YQT", "YQX", "YSB", "YT", "YTS", "YUL", "YVR", "YWG", "YXC", "YXE", "YXJ", "YXS", "YXU", "YXX", "YYC", "YYG", "YYJ", "YYT", "YYZ", "YZF", "YZV"];
  var activeMajorAirports = ["YBR", "YEG", "YBL", "YCG", "YYG", "YYQ", "YXC", "YDF", "YYE", "YXJ", "YQU", "YOJ", "YGR", "YEV", "YFB", "YKA", "YQK", "YGK", "YVP", "YGL", "YVC", "YQL", "YLL", "YXH", "YYY", "YCD", "YND", "YPE", "YYF", "YZT", "YPA", "YRT", "YQF", "YUY", "YSJ", "YXL", "YYD", "YCM", "YXT", "YTH", "YTS", "YVO", "YWH", "YWK", "YZU", "YWL", "YXX", "YDT", "YYC", "1YSB", "YRC", "CZVL", "YMM", "YFC", "YQX", "YHZ", "YHM", "YLW", "YKF", "YLY", "YXU", "YQM", "YUL", "YHU", "YOO", "YOW", "YPK", "YXS", "YQB", "YQR", "YXE", "YAM", "YYT", "YJN", "YQT", "YTZ", "YYZ", "CXH", "YVR", "YYJ", "YXY", "YQG", "YWG", "2DCI", "YZF"]; // -- vars that change with dropdown menu selections and toggle button -- //

  var selectedDropdown = passengerDropdownData;
  var totals;
  var passengerTotals;
  var canadaMap;
  var majorDateRange = {};
  var passengerDateRange = {};
  var lineDataPassenger = {};
  var lineDataMajor = {};
  var lineData = lineDataPassenger; // -- store default values for selections -- //

  var defaultRegion = "CANADA";
  var selectedDataset = PassengerData$1;
  var selectedYear;
  var selectedMonth;
  var minYear = 2010;
  var minDate = "2010-01";
  var maxYear;
  var selectedDate = selectedYear;
  var selectedRegion = "CANADA";
  var selectedSettings = settings;
  var selectedDateRange = {};
  var selectedAirpt; // NB: NEEDS TO BE DEFINED AFTER canadaMap; see colorMap()

  var stackedArea; // stores areaChart() call
  // -----------------------------------------------------------------------------

  /* SVGs */

  var map = d3.select(".dashboard .map").attr("id", "map").append("svg");
  var movementsButton = d3.select("#major");
  var passengerButton = d3.select("#movements");
  var monthDropdown = d3.select("#months"); // map colour bar

  var margin = {
    top: 0,
    right: 0,
    bottom: 10,
    left: 20
  };
  var width = 570 - margin.left - margin.right;
  var height = 150 - margin.top - margin.bottom;
  var svgCB = d3.select("#mapColourScale").select("svg").attr("class", "airCB").attr("width", width).attr("height", height).style("vertical-align", "middle");
  var chart = d3.select(".data").append("svg").attr("id", "svg_areaChartAir");
  /* -- shim all the SVGs -- */

  d3.stcExt.addIEShim(map, 387.1, 457.5);
  d3.stcExt.addIEShim(svgCB, height, width); // -----------------------------------------------------------------------------

  /* letiables */
  // For map circles

  var path;
  var defaultPointRadius = 1.3;
  var zoomedPointRadius = 0.9; // const airportGroup = map.append("g");

  var airportGroup;
  var allAirports; // -----------------------------------------------------------------------------

  /* tooltip */

  /* -- for map -- */

  var div = d3.select("body").append("div").attr("class", "tooltip").style("opacity", 0);
  /* -- for areaChart 1 -- */

  var divArea = d3.select("body").append("div").attr("class", "tooltip").style("pointer-events", "none").style("opacity", 0); // -----------------------------------------------------------------------------

  /* UI Handler */

  $(".data_set_selector").on("click", function (event) {
    // Reset to defaults
    d3.select(".map").selectAll("path").classed("airMapHighlight", false);
    selectedRegion = defaultRegion;
    d3.select("#groups")._groups[0][0].value = selectedRegion;
    d3.select("#yearSelector")._groups[0][0].value = selectedYear;

    if (event.target.id === "major") {
      if (majorDataLoadedFlag == false) {
        d3.select("#loading-gif").style("visibility", "visible");
        checkMajorDataFlag();
      } else {
        majorAirportSelected();
      }
    }

    if (event.target.id === "movements") {
      passengerMovementsSelected();
    }
  });

  function checkMajorDataFlag() {
    if (majorDataLoadedFlag == false) {
      window.setTimeout(checkMajorDataFlag, 100);
      /* this checks the flag every 100 milliseconds*/
    } else {
      d3.select("#loading-gif").style("visibility", "hidden");
      majorDataLoadedFlag = true;
      majorAirportSelected();
    }
  }

  function passengerMovementsSelected() {
    movementsButton.attr("class", "btn btn-default major data_set_selector").attr("aria-pressed", false);
    passengerButton.attr("class", "btn btn-primary movements data_set_selector").attr("aria-pressed", true);
    monthDropdown.style("visibility", "hidden");
    selectedDataset = PassengerData$1;
    selectedDropdown = passengerDropdownData;
    selectedSettings = settings;
    selectedDateRange = passengerDateRange;
    selectedDate = selectedDateRange.max;
    selectedYear = selectedDate;
    d3.select("#yearSelector")._groups[0][0].value = selectedYear;
    lineData = lineDataPassenger;
    createDropdown();
    totals = passengerTotals;
    resetZoom();
    showAreaData();
    colorMap();
    refreshMap();
  }

  function majorAirportSelected() {
    movementsButton.attr("class", "btn btn-primary major data_set_selector").attr("aria-pressed", true);
    passengerButton.attr("class", "btn btn-default movements data_set_selector").attr("aria-pressed", false);
    monthDropdown.style("visibility", "visible");
    selectedDataset = MajorAirData;
    selectedDropdown = majorDropdownData;
    selectedSettings = settingsMajorAirports;
    selectedRegion = "YYT";
    selectedDateRange = majorDateRange;
    selectedDate = selectedDateRange.max;
    selectedMonth = selectedDate.substring(5, 7);
    selectedYear = selectedDate.substring(0, 4);
    d3.select("#yearSelector")._groups[0][0].value = selectedYear;
    d3.select("#monthSelector")._groups[0][0].value = selectedMonth;
    lineData = lineDataMajor;
    createDropdown();
    resetZoom();
    showAreaData();
    colorMap();
    refreshMap();
  }

  function uiHandler(event) {
    if (event.target.id === "groups") {
      // clear any map region that is highlighted
      d3.select(".map").selectAll("path").classed("airMapHighlight", false);
      selectedRegion = document.getElementById("groups").value;

      if (d3.select("#airport".concat(selectedRegion))._groups[0][0]) {
        // menu selection is an airport
        var zoomTo = d3.select("#airport".concat(selectedRegion)).attr("class").split(" ")[1];
        canadaMap.zoom(zoomTo);
      } else if (selectedRegion === "CANADA") {
        resetZoom();
      } else {
        // zoom to selectedRegion
        canadaMap.zoom(selectedRegion);
      }

      showAreaData();
    }

    if (event.target.id === "yearSelector") {
      selectedYear = document.getElementById("yearSelector").value; // d3.select("#airportYQB")

      if (selectedDataset === MajorAirData) {
        var yearId = "#".concat("yearSelector");
        var monthId = "#".concat("monthSelector");
        selectedMonth = dropdownCheck(yearId, monthId, selectedDateRange, selectedYear, selectedMonth, true);
        selectedDate = selectedYear + "-" + selectedMonth;
      } else {
        selectedDate = selectedYear;
      }

      colorMap();
      refreshMap();
      updateTitles();
    }

    if (event.target.id === "monthSelector") {
      selectedMonth = document.getElementById("monthSelector").value;
      selectedDate = selectedYear + "-" + selectedMonth;
      colorMap();
      refreshMap();
      updateTitles();
    }
  } // -----------------------------------------------------------------------------

  /* Interactions */

  /* -- Map interactions -- */


  map.on("mousemove", function () {
    if (d3.select(d3.event.target).attr("class")) {
      // const classes = d3.event.target.classList;
      var classes = (d3.select(d3.event.target).attr("class") || "").split(" "); // IE-compatible
      // if (classes[0] !== "svg-shimmed" && classes.indexOf("classNaN") === -1) {

      if (classes[0] !== "svg-shimmed") {
        var key = i18next.t(classes[0], {
          ns: "geography"
        });

        if (key !== "airport") {
          // Highlight map region
          d3.select(".dashboard .map").select("." + classes[0]).classed("airMapHighlight", true).moveToFront(); // Tooltip

          var value = "";
          var line2 = "";

          if (selectedDataset == MajorAirData) {
            div.style("opacity", 0);
          } else {
            div.style("opacity", .9);

            if (Number(totals[selectedDate][classes[0]])) {
              value = selectedSettings.formatNum(totals[selectedDate][classes[0]]);
              line2 = selectedDataset === PassengerData$1 ? "".concat(value, " ").concat(i18next.t("units", {
                ns: "airPassengers"
              })) : "".concat(value, " ").concat(i18next.t("units", {
                ns: "airMajorAirports"
              }));
            } else {
              value = totals[selectedDate][classes[0]]; // "x"

              line2 = "".concat(value);
            }
          }

          div.html("<b> ".concat(key, " </b> <br><br>\n              <table>\n                <tr>\n                  <td><b> ").concat(line2, " </td>\n                </tr>\n              </table>")).style("pointer-events", "none");
          div.style("left", d3.event.pageX + 10 + "px").style("top", d3.event.pageY + 10 + "px");
        }
      }
    }
  });
  map.on("mouseout", function () {
    div.style("opacity", 0);

    if (selectedRegion) {
      d3.select(".map").selectAll("path:not(." + selectedRegion + ")").classed("airMapHighlight", false);
    } else {
      d3.select(".map").selectAll("path").classed("airMapHighlight", false);
    }
  });
  map.on("mousedown", function () {
    if (!d3.select(d3.event.target).attr("class") || d3.select(d3.event.target).attr("class") === "svg-shimmed") {
      toCanada();
    } // Bruno : Minor modification here 2019-04-02
    else if (d3.select(d3.event.target).attr("class")) {
        // Do not allow NaN region to be clicked
        // clear any previous clicks
        d3.select(".map").selectAll("path").classed("airMapHighlight", false); // User clicks on region

        if (d3.select(d3.event.target).attr("class") && d3.select(d3.event.target).attr("class").indexOf("svg-shimmed") === -1) {
          var classes = (d3.select(d3.event.target).attr("class") || "").split(" "); // IE-compatible

          if (classes[0] !== "airport") {
            // to avoid zooming airport cirlces
            // ---------------------------------------------------------------------
            // Region highlight
            if (selectedDataset === PassengerData$1) {
              selectedRegion = classes[0]; // Display selected region in stacked area chart

              showAreaData(); // upsdate region displayed in dropdown menu

              d3.select("#groups")._groups[0][0].value = selectedRegion;
            } // ---------------------------------------------------------------------
            // zoom


            if (classes[0] !== "airport") {
              // to avoid zooming airport cirlces
              if (classes[1] === "zoomed" || classes.length === 0) {
                // return circles to original size
                path.pointRadius(function (d, i) {
                  return defaultPointRadius;
                });
                return canadaMap.zoom();
              }

              path.pointRadius(function (d, i) {
                return zoomedPointRadius;
              });
              canadaMap.zoom(classes[0]);
            } // Chart titles


            updateTitles();
          }
        }
      }
  }); // -----------------------------------------------------------------------------

  var toCanada = function toCanada() {
    if (selectedDataset == PassengerData$1) {
      // reset area chart to Canada
      selectedRegion = "CANADA";
      showAreaData(); // update region displayed in dropdown menu

      d3.select("#groups")._groups[0][0].value = selectedRegion; // Chart titles

      updateTitles();
    }

    resetZoom();
  };
  /* -- map-related -- */


  var resetZoom = function resetZoom() {
    // clear any previous clicks
    d3.select(".map").selectAll("path").classed("airMapHighlight", false); // reset circle size

    path.pointRadius(function (d, i) {
      return defaultPointRadius;
    });

    if (d3.select("." + selectedRegion + ".zoomed")) {
      // clear zoom
      return canadaMap.zoom();
    }
  };
  /* -- plot circles on map -- */


  var refreshMap = function refreshMap() {
    // when circles are properly labeled add functionality to move grey dots to the background
    d3.selectAll(".airport").remove();
    path = d3.geoPath().projection(canadaMap.settings.projection).pointRadius(defaultPointRadius);
    airportGroup.selectAll("path").data(allAirports.features).enter().append("path").attr("d", path).attr("id", function (d, i) {
      return "airport" + d.properties.id;
    }).attr("class", function (d, i) {
      if (selectedDataset == PassengerData$1) {
        if (activePassengerAirports.includes(d.properties.id) === false) {
          return "airport ".concat(d.properties.province, " ").concat(selectedDataset, " dontShow");
        } else {
          return "airport ".concat(d.properties.province, " ").concat(selectedDataset, " hasData");
        }
      } else {
        if (activeMajorAirports.includes(d.properties.id) === false) {
          return "airport ".concat(d.properties.province, " ").concat(selectedDataset, " dontShow");
        } else {
          return "airport ".concat(d.properties.province, " ").concat(selectedDataset, " hasData");
        }
      }
    }).on("mouseover", function (d) {
      selectedAirpt = d.properties.id;
      airportHover();
    }); // d3.selectAll(".noData").moveToBack();
  };

  function colorMap() {
    // last 2 colours for blank and NaN box
    var geographies = ["NL", "PE", "NS", "NB", "QC", "ON", "MB", "SK", "AB", "BC", "YT", "NT", "NU"]; // now only color map for passenger data as agreed

    if (selectedDataset === PassengerData$1) {
      d3.select("#mapColourScale").style("opacity", 100);
      var colourArray = [];

      if (selectedDataset === PassengerData$1) {
        colourArray.push("#AFE2FF", "#72C2FF", "#bc9dff", "#894FFF", "#5D0FBC", "#F9F9F9", "#565656");
      } else {
        colourArray.push("#AFE2FF", "#72C2FF", "#bc9dff", "#894FFF", "#5D0FBC");
      }

      var numLevels = 5; // number of levels to divide colourbar into

      var totArr = [];
      totArr.push(totals[selectedDate]); // colour map to take data value and map it to the colour of the level bin it belongs to

      var dimExtent = fillMapFn(totArr, colourArray, numLevels); // colour bar scale and add label

      mapColourScaleFn(svgCB, colourArray, dimExtent, numLevels, selectedSettings);
    } else {
      d3.select("#mapColourScale").style("opacity", 0);

      for (var value in geographies) {
        d3.select(".dashboard .map").select("." + geographies[value]).style("fill", "#587792");
      }
    } // DEFINE AIRPORTGROUP HERE, AFTER CANADA MAP IS FINISHED, OTHERWISE
    // CIRCLES WILL BE PLOTTED UNDERNEATH THE MAP PATHS!


    airportGroup = map.append("g");
  }
  /* -- stackedArea chart for Passenger or Major Airports data -- */


  function showAreaData() {
    updateTitles();

    var showChart = function showChart() {
      // Bruno : My new stuff on 2019-04-02
      var d = data[selectedDataset][selectedRegion];
      var allX = true;

      for (var i = 0; i < d.length; i++) {
        allX = allX && isNaN(d[i].domestic) && isNaN(d[i].transborder) && isNaN(d[i].international);
      }

      d3.select("#annualTimeseries").style("display", allX ? "none" : "");
      d3.select("#areaLegend").style("display", allX ? "none" : "");
      d3.select("#warning").style("display", allX ? "" : "none"); // Bruno : End of my new stuff on 2019-04-02

      stackedArea = areaChart(chart, selectedSettings, data[selectedDataset][selectedRegion]); // areaChart hoverLine and tooltip

      createOverlay(stackedArea, data[selectedDataset][selectedRegion], function (d) {
        areaTooltip(stackedArea.settings, divArea, d);
      }, function () {
        divArea.style("opacity", 0);
      });
      d3.selectAll(".flag").style("opacity", 0);
      d3.select("#svg_areaChartAir").select(".x.axis").selectAll(".tick text").attr("dy", "".concat(xlabelDY, "em")); // Add css class for month tick lines

      if (selectedDataset === MajorAirData) {
        d3.select("#svg_areaChartAir .x.axis").selectAll("g.tick").each(function (d, i) {
          var thisMonth = d.getMonth();

          if (thisMonth !== 0) {
            d3.select(this).attr("class", "tick notJan");
          } else {
            d3.select(this).attr("class", "tick Jan");
          }
        });
      } else {
        d3.select("#svg_areaChartAir .x.axis").selectAll("g.tick").attr("class", "tick");
      } // Highlight region selected from menu on map


      d3.select(".dashboard .map").select("." + selectedRegion).classed("airMapHighlight", true).moveToFront(); // ------------------copy button---------------------------------
      // need to re-apend the button since table is being re-build

      if (cButton.pNode) cButton.appendTo(document.getElementById("copy-button-container"));
      dataCopyButton(data[selectedDataset][selectedRegion]); // ---------------------------------------------------------------
    };

    showChart();
  }

  function createDropdown() {
    var geoDropdown = $("#groups");
    geoDropdown.empty(); // remove old options
    // check available month/year combinations

    var yearId = "#".concat("yearSelector");
    var monthId = "#".concat("monthSelector");

    if (selectedDataset === MajorAirData) {
      selectedMonth = dropdownCheck(yearId, monthId, selectedDateRange, selectedYear, selectedMonth, true);
    } else {
      selectedMonth = dropdownCheck(yearId, monthId, selectedDateRange, selectedYear, selectedMonth, false);
    } // indent airports under each geographic region


    var indent = "&numsp;&numsp;&numsp;";
    var prefix;

    var _iterator = _createForOfIteratorHelper(selectedDropdown),
        _step;

    try {
      for (_iterator.s(); !(_step = _iterator.n()).done;) {
        var geo = _step.value;

        if (geo.type === "airport" && selectedDataset === PassengerData$1) {
          prefix = indent;
        } else {
          prefix = "";
        }

        if (geo.data && geo.data === "no") {
          geoDropdown.append($("<option disabled></option>").attr("value", geo.code).html(prefix + i18next.t(geo.code, {
            ns: "geography"
          })));
        } else {
          geoDropdown.append($("<option></option>").attr("value", geo.code).html(prefix + i18next.t(geo.code, {
            ns: "geography"
          })));
        }
      }
    } catch (err) {
      _iterator.e(err);
    } finally {
      _iterator.f();
    }
  }
  /* -- stackedArea chart for airports -- */


  function filterDates(data) {
    if (selectedDataset === PassengerData$1) {
      for (var year in data) {
        if (data[year].date.substring(0, 4) === selectedDate.toString()) {
          return data[year];
        }
      }
    } else {
      for (var _year in data) {
        if (data[_year].date === selectedDate) {
          return data[_year];
        }
      }
    }
  }

  function airportHover() {
    var divData = filterDates(lineData[selectedAirpt]);
    div.style("opacity", .9);

    if (selectedDataset === PassengerData$1) {
      var thisEnplaned = Number(divData.enplaned) ? selectedSettings.formatNum(divData.enplaned) : divData.enplaned;
      var thisDeplaned = Number(divData.deplaned) ? selectedSettings.formatNum(divData.deplaned) : divData.deplaned;
      var showUnits = Number(divData.enplaned) ? i18next.t("units", {
        ns: "airPassengers"
      }) : "";
      div.html("<b> ".concat(i18next.t(selectedAirpt, {
        ns: "geography"
      }), ", ").concat(divData.date, ":</b> <br><br>\n          <table>\n            <tr>\n              <td><b> ").concat(i18next.t("enplaned", {
        ns: "airPassengers"
      }), ": </b> ").concat(thisEnplaned, " ").concat(showUnits, " </td>\n            </tr>\n              <td><b> ").concat(i18next.t("deplaned", {
        ns: "airPassengers"
      }), ": </b> ").concat(thisDeplaned, " ").concat(showUnits, " </td>\n            </tr>\n         </table>")).style("pointer-events", "none");
    } else {
      var thisDomestic = selectedSettings.formatNum(divData.domestic);
      var thisTrans = selectedSettings.formatNum(divData.transborder);
      var thisInter = selectedSettings.formatNum(divData.international);
      var divDate = "".concat(i18next.t(divData.date.substring(5, 7), {
        ns: "months"
      }), " ").concat(divData.date.substring(0, 4));
      div.html("<b> ".concat(i18next.t(selectedAirpt, {
        ns: "geography"
      }), ", ").concat(divDate, ":</b> <br><br>\n          <table>\n            <tr>\n              <td><b> ").concat(i18next.t("domestic", {
        ns: "airPassengers"
      }), " </b>: ").concat(thisDomestic, " </td>\n            </tr>\n            <tr>\n              <td><b> ").concat(i18next.t("transborder", {
        ns: "airPassengers"
      }), " </b>: ").concat(thisTrans, " </td>\n            </tr>\n          <tr>\n            <td><b> ").concat(i18next.t("international", {
        ns: "airPassengers"
      }), " </b>: ").concat(thisInter, " </td>\n          </tr>\n        </table>")).style("pointer-events", "none");
    } // airport chart title


    d3.select("#svg_aptChart").select(".areaChartTitle").text(i18next.t(selectedAirpt, {
      ns: "geography"
    }));
  }
  /* -- update map and areaChart titles -- */


  function updateTitles() {
    var geography = i18next.t(selectedRegion, {
      ns: "geography"
    });
    var mapTitle = selectedDataset === PassengerData$1 ? "".concat(i18next.t("mapTitle", {
      ns: "airPassengers"
    }), ", ").concat(selectedDate) : "".concat(i18next.t("mapTitle", {
      ns: "airMajorAirports"
    }), ", ").concat(i18next.t(selectedMonth, {
      ns: "months"
    }), " ").concat(selectedYear);
    var areaTitle = selectedDataset === PassengerData$1 ? "".concat(i18next.t("chartTitle", {
      ns: "airPassengers"
    }), ", ").concat(geography) : "".concat(i18next.t("chartTitle", {
      ns: "airMajorAirports"
    }), ", ").concat(geography);
    var tableTitle = selectedDataset === PassengerData$1 ? "".concat(i18next.t("tableTitle", {
      ns: "airPassengers"
    }), ", ").concat(geography) : "".concat(i18next.t("tableTitle", {
      ns: "airMajorAirports"
    }), ", ").concat(geography);
    d3.select("#mapTitleAir").text(mapTitle);
    d3.select("#areaTitleAir").text(areaTitle);
    selectedSettings.tableTitle = tableTitle;
  }

  function dateInitPassenger(dateFnResult) {
    passengerDateRange.min = minYear;
    passengerDateRange.max = Number(dateFnResult.max);
    passengerDateRange.numPeriods = dateFnResult.numPeriods;
    maxYear = dateFnResult.max;
    selectedYear = maxYear;
    var yearDropdown = $("#yearSelector");

    for (var i = passengerDateRange.min; i <= passengerDateRange.max; i++) {
      yearDropdown.append($("<option></option>").attr("value", i).html(i));
    }

    selectedYear = passengerDateRange.max;
    d3.select("#yearSelector")._groups[0][0].value = selectedYear;
  }

  function dateInitMajor(dateFnResult) {
    majorDateRange.min = minDate;
    majorDateRange.max = dateFnResult.max;
    majorDateRange.numPeriods = dateFnResult.numPeriods;
  } // ------------------------------------------------------------------------------


  function isIE() {
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");

    if (msie > 0) {
      // IE 10 or older => return version number
      return parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)), 10);
    }

    var trident = ua.indexOf("Trident/");

    if (trident > 0) {
      // IE 11 => return version number
      var rv = ua.indexOf("rv:");
      return parseInt(ua.substring(rv + 3, ua.indexOf(".", rv)), 10);
    }

    var edge = ua.indexOf("Edge/");

    if (edge > 0) {
      // Edge (IE 12+) => return version number
      return parseInt(ua.substring(edge + 5, ua.indexOf(".", edge)), 10);
    } // other browser


    return false;
  }

  function ieWorkAround() {
    var summaryNode = document.getElementById("chrt-dt-tbl");
    summaryNode.addEventListener("keydown", function (ev) {
      if (ev.which == 13 || ev.which == 32) toggle(ev);
    });
    summaryNode.addEventListener("click", function (ev) {
      toggle(ev);
    });

    function toggle(ev) {
      ev.preventDefault();
      ev.stopPropagation();
      var nodedetails = document.getElementsByClassName("chart-data-table")[0];
      var isOpen = nodedetails.hasAttribute("open");
      if (isOpen) nodedetails.removeAttribute("open");else nodedetails.setAttribute("open", "open");
    }
  } // -----------------------------------------------------------------------------

  /* Copy Button*/


  function dataCopyButton(cButtondata) {
    var lines = [];
    var geography = i18next.t(selectedRegion, {
      ns: "geography"
    });
    var title = [i18next.t("tableTitle", {
      ns: "airMajorAirports",
      geo: geography
    })];
    var columnTitles = ["date", "domestic", "transborder", "international", "total"];
    var columns = [];

    for (var concept in columnTitles) {
      columns.push(i18next.t(columnTitles[concept], {
        ns: "airPassengers"
      }));
    }

    lines.push(title, [], columns);

    for (var row in cButtondata) {
      if (Object.prototype.hasOwnProperty.call(cButtondata, row)) {
        var auxRow = [];

        for (var column in columnTitles) {
          if (columnTitles[column] == "date" && selectedDataset === PassengerData$1) {
            if (Object.prototype.hasOwnProperty.call(cButtondata[row], columnTitles[column])) {
              var value = cButtondata[row][columnTitles[column]];
              auxRow.push(value.substring(0, 4));
            }
          } else if (column !== "isLast") {
            if (Object.prototype.hasOwnProperty.call(cButtondata[row], columnTitles[column])) {
              var _value = cButtondata[row][columnTitles[column]];
              auxRow.push(_value);
            }
          }
        }

        lines.push(auxRow);
      }
    }

    cButton.data = lines;
  } // -----------------------------------------------------------------------------


  function totalsCalculation(provinceSortedData) {
    var totalsObject = {};

    for (var province in provinceSortedData) {
      if (province !== "CANADA") {
        for (var entry in provinceSortedData[province]) {
          var entryDate = provinceSortedData[province][entry]["date"].substring(0, 4);

          if (!totalsObject.hasOwnProperty(entryDate)) {
            totalsObject[entryDate] = {};
          }

          totalsObject[entryDate][province] = provinceSortedData[province][entry].total;
        }
      }
    }

    return totalsObject;
  }

  function pageInitWithData(initData) {
    passengerTotals = totalsCalculation(initData.provinces);
    totals = passengerTotals;
    data[selectedDataset] = initData.provinces;
    lineDataPassenger = initData.airports;
    lineData = lineDataPassenger;
    createDropdown();
    d3.queue().defer(d3.json, "geojson/vennAirport_with_dataFlag.geojson")["await"](function (error, airports) {
      canadaMap = getCanadaMap(map).on("loaded", function () {
        allAirports = airports;
        colorMap();
        map.style("visibility", "visible").style("pointer-events", "visible");
        d3.select(".canada-map");
        refreshMap();
      });
    }); // copy button options

    var cButtonOptions = {
      pNode: document.getElementById("copy-button-container"),
      title: i18next.t("CopyButton_Title", {
        ns: "CopyButton"
      }),
      msgCopyConfirm: i18next.t("CopyButton_Confirm", {
        ns: "CopyButton"
      }),
      accessibility: i18next.t("CopyButton_Title", {
        ns: "CopyButton"
      })
    }; // build nodes on copy button

    cButton.build(cButtonOptions);
    d3.select("#symbolLink").html("<a href=".concat(i18next.t("linkURL", {
      ns: "symbolLink"
    }), ">").concat(i18next.t("linkText", {
      ns: "symbolLink"
    }), "</a>"));
    showAreaData(); // Show chart titles based on default menu options

    updateTitles();
    d3.select("#loading-gif").style("visibility", "hidden");
    loadMajorAirportData();
    if (isIE()) ieWorkAround();
  } // function to load data for major airports in background


  function loadMajorAirportData() {
    dateRangeFn(minYear, 12, MajorTowersId$1, "1.1.1.1.0.0.0.0.0.0", "month").then(function (result) {
      dateInitMajor(result);
      apiCall$1(majorDateRange, MajorAirData).then(function (returnData) {
        majorDataLoadedFlag = true;
        data[MajorAirData] = returnData;
        lineDataMajor = returnData;
      });
    });
  }

  var timeoutError = Symbol();

  var timeout = function timeout(prom, time, exception) {
    var timer;
    return Promise.race([prom, new Promise(function (_r, rej) {
      return timer = setTimeout(rej, time, exception);
    })])["finally"](function () {
      return clearTimeout(timer);
    });
  };

  i18n.load(["src/i18n"], function () {
    settings.x.label = i18next.t("x_label", {
      ns: "airPassengers"
    }), settings.y.label = i18next.t("y_label", {
      ns: "airPassengers"
    }), settingsMajorAirports.x.label = i18next.t("x_label", {
      ns: "airMajorAirports"
    }), settingsMajorAirports.y.label = i18next.t("y_label", {
      ns: "airMajorAirports"
    });
    timeout(dateRangeFn(minYear, 1, PassengerId$1, "1.1.0.0.0.0.0.0.0.0", "year"), timeoutTime, timeoutError).then(function (result) {
      dateInitPassenger(result);
      selectedDateRange = passengerDateRange;
      selectedDate = selectedDateRange.max;
      apiCall$1(selectedDateRange, selectedDataset).then(function (initData) {
        pageInitWithData(initData);
      });
    })["catch"](function (e) {
      d3.select("#loading-gif").style("display", "none");
      d3.select("#pageContents").style("display", "none");
      d3.select("#errorDiv").style("display", "inline-block");
    });
  });
  $(document).on("change", uiHandler);

  d3.selection.prototype.moveToFront = function () {
    return this.each(function () {
      this.parentNode.appendChild(this);
    });
  };

  d3.selection.prototype.moveToBack = function () {
    return this.each(function () {
      var firstChild = this.parentNode.firstChild;

      if (firstChild) {
        this.parentNode.insertBefore(this, firstChild);
      }
    });
  };

}());
