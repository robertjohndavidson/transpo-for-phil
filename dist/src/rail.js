(function () {
  'use strict';

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  function _defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, descriptor.key, descriptor);
    }
  }

  function _createClass(Constructor, protoProps, staticProps) {
    if (protoProps) _defineProperties(Constructor.prototype, protoProps);
    if (staticProps) _defineProperties(Constructor, staticProps);
    return Constructor;
  }

  function _defineProperty(obj, key, value) {
    if (key in obj) {
      Object.defineProperty(obj, key, {
        value: value,
        enumerable: true,
        configurable: true,
        writable: true
      });
    } else {
      obj[key] = value;
    }

    return obj;
  }

  function ownKeys(object, enumerableOnly) {
    var keys = Object.keys(object);

    if (Object.getOwnPropertySymbols) {
      var symbols = Object.getOwnPropertySymbols(object);
      if (enumerableOnly) symbols = symbols.filter(function (sym) {
        return Object.getOwnPropertyDescriptor(object, sym).enumerable;
      });
      keys.push.apply(keys, symbols);
    }

    return keys;
  }

  function _objectSpread2(target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i] != null ? arguments[i] : {};

      if (i % 2) {
        ownKeys(Object(source), true).forEach(function (key) {
          _defineProperty(target, key, source[key]);
        });
      } else if (Object.getOwnPropertyDescriptors) {
        Object.defineProperties(target, Object.getOwnPropertyDescriptors(source));
      } else {
        ownKeys(Object(source)).forEach(function (key) {
          Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key));
        });
      }
    }

    return target;
  }

  function _toConsumableArray(arr) {
    return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread();
  }

  function _arrayWithoutHoles(arr) {
    if (Array.isArray(arr)) return _arrayLikeToArray(arr);
  }

  function _iterableToArray(iter) {
    if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter);
  }

  function _unsupportedIterableToArray(o, minLen) {
    if (!o) return;
    if (typeof o === "string") return _arrayLikeToArray(o, minLen);
    var n = Object.prototype.toString.call(o).slice(8, -1);
    if (n === "Object" && o.constructor) n = o.constructor.name;
    if (n === "Map" || n === "Set") return Array.from(o);
    if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen);
  }

  function _arrayLikeToArray(arr, len) {
    if (len == null || len > arr.length) len = arr.length;

    for (var i = 0, arr2 = new Array(len); i < len; i++) arr2[i] = arr[i];

    return arr2;
  }

  function _nonIterableSpread() {
    throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
  }

  function _createForOfIteratorHelper(o, allowArrayLike) {
    var it;

    if (typeof Symbol === "undefined" || o[Symbol.iterator] == null) {
      if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") {
        if (it) o = it;
        var i = 0;

        var F = function () {};

        return {
          s: F,
          n: function () {
            if (i >= o.length) return {
              done: true
            };
            return {
              done: false,
              value: o[i++]
            };
          },
          e: function (e) {
            throw e;
          },
          f: F
        };
      }

      throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
    }

    var normalCompletion = true,
        didErr = false,
        err;
    return {
      s: function () {
        it = o[Symbol.iterator]();
      },
      n: function () {
        var step = it.next();
        normalCompletion = step.done;
        return step;
      },
      e: function (e) {
        didErr = true;
        err = e;
      },
      f: function () {
        try {
          if (!normalCompletion && it.return != null) it.return();
        } finally {
          if (didErr) throw err;
        }
      }
    };
  }

  var settingsBar = {
    aspectRatio: 16 / 13,
    margin: {
      top: 50,
      left: 50,
      bottom: 50
    },
    x: {
      label: i18next.t("x_label", {
        ns: "railBar"
      }),
      getId: function getId(d) {
        return d.year;
      },
      getValue: function getValue() {
        for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
          args[_key] = arguments[_key];
        }

        return this.x.getId.apply(this, args);
      },
      getClass: function getClass() {
        for (var _len2 = arguments.length, args = new Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
          args[_key2] = arguments[_key2];
        }

        return this.x.getId.apply(this, args);
      },
      getTickText: function getTickText(val) {
        return i18next.t(val, {
          ns: "railBar"
        });
      }
    },
    y: {
      label: i18next.t("y_label", {
        ns: "railBar"
      }),
      getValue: function getValue(d) {
        return d.value;
      },
      getText: function getText(d) {
        return d.value;
      },
      getTickText: function getTickText(d) {
        return this.formatNum(d);
      },
      ticks: 10,
      tickSizeOuter: 0
    },
    z: {
      label: i18next.t("z_label", {
        ns: "railTable"
      }),
      getId: function getId(d) {
        return d.category;
      },
      getKeys: function getKeys(object) {
        var keys = Object.keys(object[0]);
        keys.splice(keys.indexOf("category"), 1);
        return keys;
      },
      formatData: function formatData(data) {
        return data[0].values;
      },
      getClass: function getClass() {
        for (var _len3 = arguments.length, args = new Array(_len3), _key3 = 0; _key3 < _len3; _key3++) {
          args[_key3] = arguments[_key3];
        }

        return this.z.getId.apply(this, args);
      },
      getDataPoints: function getDataPoints(d) {
        return d.values;
      },
      getText: function getText(d) {
        return i18next.t(d.key, {
          ns: "rail"
        });
      },
      getHeaderText: function getHeaderText(d) {
        return i18next.t(d.key[0], {
          ns: "rail"
        }) + " " + i18next.t("to", {
          ns: "rail"
        }) + " " + i18next.t("to" + d.key[1], {
          ns: "rail"
        });
      }
    },
    _selfFormatter: i18n.getNumberFormatter(0),
    formatNum: function formatNum() {
      for (var _len4 = arguments.length, args = new Array(_len4), _key4 = 0; _key4 < _len4; _key4++) {
        args[_key4] = arguments[_key4];
      }

      return this._selfFormatter.format(args);
    },
    width: 800,
    datatable: false,
    tableTitle: ""
  };

  function drawTable (data, settings, origin) {
    var sett = settings;
    var thisSVG = d3.select("#railTable"); // .select("svg");
    // const filteredData = (sett.filterData && typeof sett.filterData === "function") ?
    //     sett.filterData(data, "table") : data;
    // use original data, not array returned by filteredData which may contain inserted year-end datapts

    var filteredData = filterData(data);
    var details = thisSVG.select(".chart-data-table");
    var keys = ["AT", "QC", "ON", "MB", "SK", "AB", "BC", "USA-MX", "All"];
    var table;
    var tableDiv;
    var header;
    var body;
    var dataRows;
    var dataRow;
    var k;

    if (sett.dataTableTotal) {
      keys.push("total");
    }

    if (!details.empty()) {
      // details.remove();
      details.remove();
    } // if (details.empty()) {


    details = thisSVG.append("div").attr("class", "chart-data-table"); // ----Copy Button Container ---------------------------------------------

    var copyButtonId = "copy-button-container"; // let copyButton = document.createElement("div");
    // copyButton.setAttribute("id", copyButtonId);
    // details.append(copyButton);

    details.append("div").attr("id", copyButtonId);
    tableDiv = details.append("div").classed("table-responsive", true); // .attr("id", summaryId)

    table = tableDiv.append("table").attr("class", "table");
    table.append("caption") // .text(sett.datatable.title);
    .attr("class", "tableCaption").text(sett.tableTitle);
    header = table.append("thead").attr("id", "tblHeader").append("tr").attr("id", "tblHeaderTR");
    body = table.append("tbody").attr("id", "tblBody");
    header.append("td").attr("id", "thead_h0").text(filterYear(sett.x.label)); //  debugger

    for (k = 0; k < keys.length; k++) {
      header.append("th").attr("id", "thead_h" + (k + 1)).style("text-align", "right").text(sett.z.getHeaderText.bind(sett)({
        key: [origin, keys[k]]
      }));
    }

    dataRows = body.selectAll("tr").data(filteredData);
    dataRow = dataRows.enter().append("tr").attr("id", function (d, i) {
      return "row" + i;
    });
    dataRow.append("th").attr("id", function (d, i) {
      return "row" + i + "_h0";
    }).text((sett.x.getText || sett.x.getValue).bind(sett));

    for (k = 0; k < keys.length; k++) {
      dataRow.append("td").attr("headers", function (d, i) {
        return "row" + i + "_h0" + " thead_h" + (k + 1);
      }).text(function (d) {
        return sett.formatNum(d[keys[k]]);
      }).style("text-align", "right");
    }

    if ($ || wb) {
      $(".chart-data-table summary").trigger("wb-init.wb-details");
    }
  }

  function filterYear(key) {
    if (key !== "Year") {
      return key;
    } else {
      return "";
    }
  }

  function filterData(originalData) {
    var returnArray = [];

    for (var year in originalData) {
      var entry = {};
      entry.year = year;

      for (var geo in originalData[year]) {
        entry[geo] = originalData[year][geo];
      }

      returnArray.push(entry);
    }

    return returnArray;
  }

  function mapColourScaleFn (svgCB, colourArray, dimExtent, numLevels, settings) {
    // Definitions
    // ---------------------------------------------------------------------------
    var rectDim = 35;
    var yRect = 20;
    var yText = 65;
    var yNaNText = yText + 7; // text labels (calculate cbValues)

    var delta = (dimExtent[1] - dimExtent[0]) / numLevels;
    var cbValues = [];
    cbValues[0] = dimExtent[0];

    for (var idx = 1; idx < numLevels; idx++) {
      cbValues.push(Math.round(idx * delta + dimExtent[0]));
    } // rect fill fn


    var getFill = function getFill(d, i) {
      return colourArray[i];
    }; // text fn


    var getText = function getText(i, j) {
      if (i < numLevels) {
        var s0 = settings.formatNum(cbValues[j]);

        if (numLevels === 1) {
          return s0;
        } else {
          return s0 + "+";
        }
      } else if (i === numLevels + 1) {
        return "x";
      }
    }; // tooltip for NaN box


    var divNaN = d3.select("body").append("div").attr("class", "tooltip").style("opacity", 0); // -----------------------------------------------------------------------------
    // g node for colourbar title (text is set in index.js)

    svgCB.append("g").attr("class", "colourbarTitle").attr("id", "cbTitle").append("text") // .text("test")
    .attr("transform", function (d, i) {
      return "translate(225, 15)";
    }).style("display", function () {
      return "inline";
    }); // Create the umbrella group

    var rectGroups = svgCB.attr("class", "mapCB").selectAll(".legend").data(Array.from(Array(colourArray.length).keys())); // Append g nodes (to be filled with a rect and a text) to umbrella group

    var newGroup = rectGroups.enter().append("g").attr("class", "legend").attr("id", function (d, i) {
      return "cb".concat(i);
    }); // add rects

    newGroup.append("rect").attr("width", rectDim).attr("height", rectDim).attr("y", yRect).attr("x", function (d, i) {
      return 135 + i * rectDim;
    }).attr("fill", getFill).attr("class", function (d, i) {
      if (i === numLevels + 1) {
        return "classNaN";
      }

      if (numLevels === 1) {
        return "zeroValue";
      }
    }); // hover over NaN rect only

    newGroup.selectAll(".legend rect").on("mouseover", function (d, i) {
      if (d3.select(this).attr("class") === "classNaN") {
        var line1 = i18next.t("NaNhover1", {
          ns: "airUI"
        });
        var line2 = i18next.t("NaNhover2", {
          ns: "airUI",
          escapeInterpolation: false
        });
        divNaN.style("opacity", 0.9).html("<br>" + line1 + "<br>" + line2 + "<br><br>").style("left", d3.event.pageX + 10 + "px").style("top", d3.event.pageY + 10 + "px");
      }
    }).on("mouseout", function () {
      divNaN.style("opacity", 0);
    }); // add text

    newGroup.append("text").text(getText).attr("text-anchor", "end").attr("transform", function (d, i) {
      if (i < numLevels) {
        return "translate(".concat(140 + i * (rectDim + 0), ", ").concat(yText, ") rotate(-45)");
      } else if (i === numLevels + 1) {
        // NaN box in legend
        return "translate(".concat(156 + i * (rectDim + 0), ", ").concat(yNaNText, ") ");
      }
    }).style("display", function () {
      return "inline";
    }); // Update rect fill for any new colour arrays passed in

    rectGroups.select("rect").attr("fill", getFill); // Update rect text for different year selections

    rectGroups.select("text").text(getText); // hack to get color bar cetered when value is 0

    if (numLevels === 1) {
      d3.select("#cb0").attr("transform", "translate(73,0)");
    } else {
      d3.select("#cb0").attr("transform", "translate(0,0)");
    }

    rectGroups.exit().remove();
  }

  function fillMapFn (data, colourArray, numLevels) {
    //const nullColour = colourArray.slice(-1)[0];
    // data is an Array
    var thisData = data[0]; // Object

    var dimExtent = [];
    var totArray = [];
    var levels = numLevels;
    totArray = Object.values(thisData);
    totArray.sort(function (a, b) {
      return a - b;
    });
    dimExtent = d3.extent(totArray);

    if (dimExtent[1] === 0) {
      levels = 1;
    } // colour map to take data value and map it to the colour of the level bin it belongs to


    var colourMap = d3.scaleQuantize().domain([dimExtent[0], dimExtent[1]]).range(colourArray.slice(0, levels));

    var _loop = function _loop(key) {
      if (thisData.hasOwnProperty(key)) {
        d3.select(".dashboard .map").selectAll("." + key).style("fill", function () {
          return colourMap(thisData[key]);
        }); // Bruno : Unused code removed following no data message modification
      }
    };

    for (var key in thisData) {
      _loop(key);
    }

    return dimExtent;
  }

  var webAPI = "https://www150.statcan.gc.ca/t1/wds/rest/getDataFromCubePidCoordAndLatestNPeriods";
  function dateRangeFn (minYear, frequency, productID, defaultCoord, granularity) {
    return new Promise(function (resolve, reject) {
      var mostRecentDate;
      var numberOfPeriods;
      var returnObject = {};
      var myData = [{
        "productId": productID,
        "coordinate": defaultCoord,
        "latestN": 1
      }];
      $.ajax({
        type: "post",
        url: webAPI,
        data: JSON.stringify(myData),
        dataType: "json",
        contentType: "application/json",
        success: function success(data, textStatus, jQxhr) {
          if (granularity === "year") {
            mostRecentDate = data[0].object.vectorDataPoint[0].refPer.substring(0, 4);
            numberOfPeriods = (Number(mostRecentDate) - minYear + 1) * frequency;
            returnObject.max = mostRecentDate;
            returnObject.numPeriods = numberOfPeriods;
          } else if (granularity === "month") {
            mostRecentDate = data[0].object.vectorDataPoint[0].refPer.substring(0, 7);
            numberOfPeriods = (Number(mostRecentDate.substring(0, 4)) - minYear) * frequency + Number(mostRecentDate.substring(5, 7));
            returnObject.max = mostRecentDate;
            returnObject.numPeriods = numberOfPeriods;
          }

          resolve(returnObject);
        },
        error: function error(jqXhr, textStatus, errorThrown) {
          reject(errorThrown);
        }
      });
    });
  }

  var ProductId = 23100062;
  var webAPI$1 = "https://www150.statcan.gc.ca/t1/wds/rest/getDataFromCubePidCoordAndLatestNPeriods";
  var numToProvince = {
    1: "All",
    2: "AT",
    3: "QC",
    4: "ON",
    5: "MB",
    6: "SK",
    7: "AB",
    8: "BC",
    11: "USA-MX"
  };
  var provinceToNum = {
    "All": 1,
    "AT": 2,
    "QC": 3,
    "ON": 4,
    "MB": 5,
    "SK": 6,
    "AB": 7,
    "BC": 8,
    "USA-MX": 11
  };
  var numToComm = {
    1: "total",
    2: "wheat",
    6: "canola",
    21: "ores",
    27: "coal",
    29: "oils",
    35: "chems",
    36: "potash",
    42: "lumber",
    44: "pulp",
    64: "mixed"
  };
  var statusCodes = {
    1: "..",
    2: "0s",
    3: "A",
    4: "B",
    5: "C",
    6: "D",
    7: "E",
    8: "F"
  };
  var qi_F = 8;
  function apiCall (maxYear, minYear, origin) {
    return new Promise(function (resolve, reject) {
      // get coordinates for data
      var coordinateArray = coordinateTranslate(origin);
      var yearRange = Number(maxYear) - Number(minYear) + 1;
      var returnArray = [];
      var myData = [];

      for (var i = 0; i < coordinateArray.length; i++) {
        myData.push({
          "productId": ProductId,
          "coordinate": coordinateArray[i],
          "latestN": yearRange
        });
      }

      $.support.cors = true;
      $.ajax({
        type: "post",
        url: webAPI$1,
        data: JSON.stringify(myData),
        dataType: "json",
        contentType: "application/json",
        success: function success(data, textStatus, jQxhr) {
          returnArray = rebuild(data, yearRange, origin);
          resolve(returnArray);
        },
        error: function error(jqXhr, textStatus, errorThrown) {
          reject(errorThrown);
        }
      });
    });
  }

  function rebuild(data, yearRange, origin) {
    var dataByProvince = {};
    var returnArray = [];
    var provinceCode;

    for (var i = 0; i < data.length; i++) {
      provinceCode = data[i].object.coordinate.split(".", 2)[1];

      if (!dataByProvince.hasOwnProperty(provinceCode)) {
        dataByProvince[provinceCode] = [];
      }

      dataByProvince[provinceCode].push(data[i]);
    }

    for (var province in dataByProvince) {
      if (Object.prototype.hasOwnProperty.call(dataByProvince, province)) {
        for (var _i = 0; _i < yearRange; _i++) {
          var allOtherCalculationArray = [];

          for (var j = 0; j < Object.keys(numToComm).length; j++) {
            allOtherCalculationArray.push(rebuildData(dataByProvince[province][j], origin, numToProvince[province], _i));
          }

          var itemArray = calculateAllOther(allOtherCalculationArray);
          returnArray = returnArray.concat(itemArray);
        }
      }
    }

    return returnArray;
  } // because we need to show all other commodities, we must subtract
  // the top 10 comodities from the total


  function calculateAllOther(data) {
    var totalVal;
    var allOtherval;
    var returnArray = [];
    var allOtherObject = {};

    var _iterator = _createForOfIteratorHelper(data),
        _step;

    try {
      for (_iterator.s(); !(_step = _iterator.n()).done;) {
        var item = _step.value;

        if (item.comm === "total") {
          totalVal = item.value;
          allOtherObject.comm = "other";
          allOtherObject.date = item.date;
          allOtherObject.origin = item.origin;
          allOtherObject.dest = item.dest;
        }
      }
    } catch (err) {
      _iterator.e(err);
    } finally {
      _iterator.f();
    }

    allOtherval = totalVal;

    var _iterator2 = _createForOfIteratorHelper(data),
        _step2;

    try {
      for (_iterator2.s(); !(_step2 = _iterator2.n()).done;) {
        var _item = _step2.value;

        if (_item.comm !== "total") {
          allOtherval -= _item.value;
          returnArray.push(_item);
        }
      }
    } catch (err) {
      _iterator2.e(err);
    } finally {
      _iterator2.f();
    }

    allOtherObject.value = allOtherval;
    returnArray.push(allOtherObject);
    return returnArray;
  }

  function rebuildData(data, origin, desitination, year) {
    var returnObject = {};
    var datapoint;
    var returnType = Number(data.object.coordinate.split(".", 3)[2]);
    var returnValue;
    datapoint = data.object.vectorDataPoint[year];

    if (datapoint.statusCode != 1 && datapoint.securityLevelCode == 0 && datapoint.statusCode != qi_F) {
      returnValue = datapoint.value;
    } else {
      returnValue = statusCodes[datapoint.statusCode];
    }

    returnObject.value = returnValue;
    returnObject.comm = numToComm[returnType];
    returnObject.date = datapoint.refPer.substring(0, 4);
    returnObject.origin = origin;
    returnObject.dest = desitination;
    return returnObject;
  }

  function coordinateTranslate(geography) {
    var numGeo = provinceToNum[geography];
    var returnArray = [];

    for (var i in numToProvince) {
      for (var j in numToComm) {
        returnArray.push("".concat(numGeo, ".").concat(i, ".").concat(j, ".0.0.0.0.0.0.0"));
      }
    }

    return returnArray;
  }

  var CopyButton = /*#__PURE__*/function () {
    function CopyButton(pNode, options) {
      _classCallCheck(this, CopyButton);

      this.pNode = pNode ? pNode : document.createElement("div");
      this.options = options ? options : {};
      this.nodes = {};
      this.data = null;
      this.instanceNumber = ++CopyButton.n;
      this["class"] = this.options["class"] || "";
      /* this.data = shall be an array (i.e called rowsArray) of arrays (i.e each is called row).
        each array on rowsArray represents a row on the table.
        this.data must be set/updated by the code that uses this button
        [
          ["title"]
          ["columna1" , "columna2" ,..., "columnaN"]
          ["value1Row1", "value2Row1",..., "valueNRowN"]
          ["value1Row2", "value2Row2",..., "valueNRowN"]
        ]
      */
    }

    _createClass(CopyButton, [{
      key: "build",
      value: function build(options) {
        if (options) this.options = options; // workAround;

        if (options.pNode) this.pNode = options.pNode; // workAround;

        if (options["class"]) this["class"] = options["class"]; // workAround;

        this.root = document.createElement("div");
        this.root.setAttribute("class", "copy-button button-" + this.instanceNumber + " " + this["class"]);
        this.pNode.appendChild(this.root);
        this.nodes.btnCopy = document.createElement("button");
        this.nodes.btnCopy.setAttribute("type", "button");
        this.nodes.btnCopy.setAttribute("class", "btn btn-primary copy button-" + this.instanceNumber + " " + this["class"]);
        this.nodes.btnCopy.setAttribute("title", this.options.title || "");
        this.root.appendChild(this.nodes.btnCopy);
        var icon = document.createElement("span");
        icon.setAttribute("class", "fa fa-clipboard clipboard button-" + this.instanceNumber + " " + this["class"]);
        this.nodes.btnCopy.appendChild(icon);
        var accessibility = document.createElement("span");
        accessibility.setAttribute("class", "wb-inv button-" + this.instanceNumber + " " + this["class"]);
        accessibility.innerHTML = this.options.accessibility || "";
        this.nodes.btnCopy.appendChild(accessibility);
        this.nodes.msgCopyConfirm = document.createElement("div");
        this.nodes.msgCopyConfirm.setAttribute("class", "copy-confirm button-" + this.instanceNumber + " " + this["class"]);
        this.nodes.msgCopyConfirm.setAttribute("aria-live", "polite");
        this.nodes.msgCopyConfirm.innerHTML = this.options.msgCopyConfirm || "";
        this.root.appendChild(this.nodes.msgCopyConfirm);
        this.nodes.btnCopy.addEventListener("click", this.onBtnClick.bind(this));
      }
    }, {
      key: "onBtnClick",
      value: function onBtnClick(ev) {
        this.copyData(this.data);
      }
    }, {
      key: "copyData",
      value: function copyData(lines) {
        var linesTemp = lines ? lines : [];
        this.clipboard(this.toCSV("\t", linesTemp), this.root);
        this.fade(this.nodes.msgCopyConfirm, true);
        setTimeout(function (ev) {
          this.fade(this.nodes.msgCopyConfirm, false);
        }.bind(this), 1500);
      }
    }, {
      key: "toCSV",
      value: function toCSV(separator, lines) {
        var csv = lines.map(function (line) {
          return line.join(separator);
        });
        return csv.join("\r\n");
      }
    }, {
      key: "clipboard",
      value: function clipboard(string, target) {
        if (this.isIE()) window.clipboardData.setData("Text", string);else {
          // Copying the string
          var aux = document.createElement("textarea");
          aux.textContent = string;
          target.appendChild(aux);
          aux.select();
          document.execCommand("copy");
          target.removeChild(aux);
        }
      }
    }, {
      key: "fade",
      value: function fade(node, visible) {
        var clss = ["copy-confirm button-" + this.instanceNumber + " " + this["class"]];
        var add = visible ? "fadeIn" : "fadeOut";
        clss.push(add);
        node.setAttribute("class", clss.join(" "));
      } // work around for when tables are destroyed

    }, {
      key: "appendTo",
      value: function appendTo(pNode) {
        this.pNode = pNode;
        this.pNode.appendChild(this.root);
        this.nodes.msgCopyConfirm.setAttribute("class", "copy-confirm button-" + this.instanceNumber + " " + this["class"]);
      }
    }, {
      key: "isIE",
      value: function isIE() {
        var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE ");

        if (msie > 0) {
          // IE 10 or older => return version number
          return parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)), 10);
        }

        var trident = ua.indexOf("Trident/");

        if (trident > 0) {
          // IE 11 => return version number
          var rv = ua.indexOf("rv:");
          return parseInt(ua.substring(rv + 3, ua.indexOf(".", rv)), 10);
        }

        var edge = ua.indexOf("Edge/");

        if (edge > 0) {
          // Edge (IE 12+) => return version number
          return parseInt(ua.substring(edge + 5, ua.indexOf(".", edge)), 10);
        } // other browser


        return false;
      }
    }]);

    return CopyButton;
  }();
  CopyButton.n = 0;

  var RailProductId = 23100062;
  /* Copy Button */
  // -----------------------------------------------------------------------------

  var cButton = new CopyButton(); // -----------------------------------------------------------------------------
  // import createLegend from "./createLegend.js";

  var timeoutTime = 3000;
  var dateRange = {};
  var defaultOrig = "AT";
  var defaultDest = "QC";
  var defaultComm = "mixed";
  var selectedOrig;
  var selectedDest;
  var selectedComm;
  var maxYear;
  var minYear = 2010;
  var dataTag; // stores `${selectedOrig}_${selectedComm}`;

  var xlabelDY = 0.71; // spacing between areaChart xlabels and ticks

  var usaMexicoImageLocation = "lib/usamexico.png";
  var origin = "Origin";
  var data = {}; // stores data for barChart

  var selectedYear; // ---------------------------------------------------------------------

  /* SVGs */
  // Canada map

  var map = d3.select(".dashboard .map").append("svg").attr("focusable", "false");
  var div = d3.select("body").append("div").attr("class", "tooltip").attr("id", "railTooltip").style("opacity", 0); // Map colour bar

  var margin = {
    top: 20,
    right: 0,
    bottom: 10,
    left: 20
  };
  var width = 570 - margin.left - margin.right;
  var height = 150 - margin.top - margin.bottom;
  var svgCB = d3.select("#mapColourScale").select("svg").attr("focusable", "false").attr("class", "mapCB").attr("width", width).attr("height", height).style("vertical-align", "middle");
  /* -- shim all the SVGs (chart is already shimmed in component) -- */

  d3.stcExt.addIEShim(map, 387.1, 457.5);
  d3.select("body").append("div").attr("class", "tooltip").style("opacity", 0);
  var chart = d3.select(".data.raildata").append("svg").attr("focusable", "false").attr("id", "svgBar");
  d3.select("#commgrid").append("svg").attr("id", "svg_commgrid"); // ---------------------------------------------------------------------

  /* load data fn */
  // ---------------------------------------------------------------------

  function uiHandler(event) {
    if (event.target.id === "commodity") {
      setCommodity(document.getElementById("commodity").value);
      updatePage();
    }

    if (event.target.id === "originGeo") {
      setOrigin(document.getElementById("originGeo").value);
      updatePage();
    }

    if (event.target.id === "destGeo") {
      setDest(document.getElementById("destGeo").value);
      updatePage();
    }

    if (event.target.id === "yearSelector") {
      setYear(document.getElementById("yearSelector").value);
      updatePage();
    }
  } // -----------------------------------------------------------------------------

  /* -- Map interactions -- */


  map.on("mousemove", function () {
    if (d3.select(d3.event.target).attr("id")) {
      // Tooltip
      var key = d3.event.target.id;
      var value;

      if (!isNaN(data[dataTag][selectedYear][key.substring(0, key.length - 4)])) {
        value = settingsBar.formatNum(data[dataTag][selectedYear][key.substring(0, key.length - 4)]) + " " + i18next.t("units", {
          ns: "rail"
        });
      } else {
        value = i18next.t("hoverNA", {
          ns: "rail"
        });
      }

      div.style("opacity", .9);
      div.html("<b>" + i18next.t("hoverText", {
        ns: "rail",
        origin: i18next.t(selectedOrig, {
          ns: "rail"
        }),
        dest: i18next.t(key.substring(0, key.length - 4), {
          ns: "rail"
        })
      }) + "</b>" + "<br><br>" + "<table>" + "<tr>" + "<td><b>" + value + "</td>" + "</tr>" + "</table>");
      div.style("left", d3.event.pageX + 10 + "px").style("top", d3.event.pageY + 10 + "px");
    }
  });
  map.on("mouseout", function () {
    div.style("opacity", 0);
  });
  map.on("mousedown", function () {
    if (event.target.id !== "YT_map" && event.target.id !== "NU_map" && event.target.id !== "NT_map" && event.target.id !== "") {
      document.getElementById("originGeo").value = d3.event.target.id.substring(0, event.target.id.length - 4);
      setOrigin(d3.event.target.id.substring(0, event.target.id.length - 4));
      updatePage();
    }
  }); // -----------------------------------------------------------------------------

  /* FNS */

  function updatePage() {
    if (!data[dataTag]) {
      d3.select("#loading-gif").style("visibility", "visible");
      apiCall(maxYear, minYear, selectedOrig).then(function (newData) {
        buildData(newData);
        showBarChartData();
        colorMap();
        drawTable(data[dataTag], settingsBar, selectedOrig);
        d3.select("#loading-gif").style("visibility", "hidden"); // ------------------copy button---------------------------------
        // need to re-apend the button since table is being re-build

        if (cButton.pNode) cButton.appendTo(document.getElementById("copy-button-container"));
        dataCopyButton(data[dataTag]); // ---------------------------------------------------------------
      });
    } else {
      showBarChartData();
      colorMap();
      drawTable(data[dataTag], settingsBar, selectedOrig); // ------------------copy button---------------------------------
      // need to re-apend the button since table is being re-build

      if (cButton.pNode) cButton.appendTo(document.getElementById("copy-button-container"));
      dataCopyButton(data[dataTag]); // ---------------------------------------------------------------
    }
  }

  function setYear(newYear) {
    selectedYear = newYear;
  }

  function setCommodity(newComm) {
    selectedComm = newComm;
    dataTag = "".concat(selectedOrig, "_").concat(selectedComm);
  }

  function setOrigin(newOrig) {
    selectedOrig = newOrig;
    dataTag = "".concat(selectedOrig, "_").concat(selectedComm); // Highlight region selected from menu on map

    highlightMap(newOrig, origin);
  }

  function setDest(newDest) {
    selectedDest = newDest;
  }

  function highlightMap(selection, mode) {
    d3.selectAll(".dashboard .map .rail".concat(mode, "MapHighlight")).classed("rail".concat(mode, "MapHighlight"), false).classed("railMapHighlight", false);
    d3.selectAll(".dashboard .map #".concat(selection, "_map")).classed("rail".concat(mode, "MapHighlight"), true).classed("railMapHighlight", true);
    d3.selectAll(".dashboard .map .railMapHighlight").moveToFront();
  }

  function colorMap() {
    // store map data in array and plot
    var thisTotalObject = {};
    var thisTotalArray = [];

    for (var key in data[dataTag][selectedYear]) {
      if (key !== "All") {
        thisTotalObject[key] = data[dataTag][selectedYear][key];
      }
    }

    thisTotalArray.push(thisTotalObject);
    var colourArray = ["#AFE2FF", "#72C2FF", "#bc9dff", "#894FFF", "#5D0FBC"];
    var numLevels = colourArray.length; // colour map with fillMapFn and output dimExtent for colour bar scale

    var dimExtent = fillMapFn(thisTotalArray, colourArray, numLevels); // colour bar scale and add label
    // ADD LOGIC FOR 0 VALUE

    if (dimExtent[1] === 0) {
      mapColourScaleFn(svgCB, [colourArray[0]], dimExtent, 1, settingsBar);
    } else {
      mapColourScaleFn(svgCB, colourArray, dimExtent, colourArray.length, settingsBar);
    } // Colourbar label (need be plotted only once)


    var mapScaleLabel = i18next.t("units", {
      ns: "rail"
    });
    d3.select("#cbTitle").select("text").text(mapScaleLabel).attr("transform", function (d, i) {
      return "translate(203, 15)";
    });
  } // ---------------------------------------------------------------------

  /* -- display barChart -- */


  function filterDataBar() {
    var _this = this;

    var d = data[dataTag];
    return [{
      category: "".concat(this.selectedOrig),
      values: Object.keys(d).map(function (p) {
        return {
          year: p,
          value: d[p][_this.selectedDest]
        };
      })
    }];
  }

  function showBarChartData() {
    barChart(chart, _objectSpread2(_objectSpread2({}, aditionalBarSettings), {}, {
      selectedOrig: selectedOrig,
      selectedDest: selectedDest
    }));
    d3.select("#svgBar").select(".x.axis").selectAll(".tick text").attr("dy", "".concat(xlabelDY, "em"));
    updateTitles();
  }
  /* -- update map and areaChart titles -- */


  function updateTitles() {
    var thisComm = i18next.t(selectedComm, {
      ns: "rail"
    });
    var tableComm = i18next.t(selectedComm, {
      ns: "railTable"
    });
    i18next.t(selectedOrig, {
      ns: "geography"
    });
    i18next.t(selectedDest, {
      ns: "geography"
    });
    d3.select("#railTitleBarChart").text(i18next.t("barChartTitle", {
      ns: "rail",
      commodity: thisComm,
      geo: i18next.t("from" + selectedOrig, {
        ns: "rail"
      }),
      dest: i18next.t("to" + selectedDest, {
        ns: "rail"
      })
    }));
    d3.select("#mapTitleRail").text(i18next.t("mapTitle", {
      ns: "rail",
      commodity: thisComm,
      geo: i18next.t("from" + selectedOrig, {
        ns: "rail"
      }),
      year: selectedYear
    }));
    settingsBar.tableTitle = i18next.t("tableTitle", {
      ns: "rail",
      comm: tableComm
    });
    drawTable(data[dataTag], settingsBar, selectedOrig);
  }

  var aditionalBarSettings = _objectSpread2(_objectSpread2({}, settingsBar), {}, {
    filterData: filterDataBar
  });

  function dataCopyButton(cButtondata) {
    var thisTilteOrigin = i18next.t("from" + selectedOrig, {
      ns: "rail"
    });
    var finalArray = []; // for first data table

    var dataArray = [];
    var tableComm = i18next.t(selectedComm, {
      ns: "railTable"
    });
    var title = i18next.t("dataTableTitle", {
      ns: "rail",
      comm: tableComm,
      geo: thisTilteOrigin
    });
    var firstTitle = [title];

    for (var year in cButtondata) {
      var entry = {};
      entry.year = year;

      for (var geo in cButtondata[year]) {
        entry[geo] = cButtondata[year][geo];
      }

      dataArray.push(entry);
    }

    var mainData = formatForSpreadsheet(dataArray, firstTitle);
    finalArray.push.apply(finalArray, _toConsumableArray(mainData));
    finalArray.push([]);
    cButton.data = finalArray;
  }

  function formatForSpreadsheet(dataArray, title) {
    var lines = [];
    var columns = [""];

    for (var concept in dataArray[0]) {
      if (concept != "year") {
        if (concept !== "isLast") {
          columns.push(i18next.t(concept, {
            ns: "rail"
          }));
        }
      }
    }

    lines.push(title, [], columns);

    for (var row in dataArray) {
      if (Object.prototype.hasOwnProperty.call(dataArray, row)) {
        var auxRow = [];

        for (var column in dataArray[row]) {
          if (column !== "isLast") {
            if (Object.prototype.hasOwnProperty.call(dataArray[row], column)) {
              var value = dataArray[row][column];

              if (column === "year") {
                auxRow.unshift(value);
              } else {
                auxRow.push(value);
              }
            }
          }
        }

        lines.push(auxRow);
      }
    }

    return lines;
  } // ---------------------------------------------------------------------


  function pageInitWithData() {
    setOrigin(defaultOrig);
    setDest(defaultDest);
    setCommodity(defaultComm);
    getCanadaMap(map).on("loaded", function () {
      // USA-MEXICO SVG
      // Place under alberta
      var usaMexOffset = document.getElementById("AB_map").getBBox(); // create rectangle

      var usMex = map.append("g").attr("id", "usa-mex-group");
      usMex.append("rect").attr("width", 35).attr("height", 11).attr("x", usaMexOffset.x).attr("y", usaMexOffset.height + usaMexOffset.y + 18).attr("class", "USA-MX").attr("id", "USA-MX_map"); // create image

      usMex.append("image").attr("width", 35).attr("height", 15).attr("x", usaMexOffset.x).attr("y", usaMexOffset.height + usaMexOffset.y + 5).attr("xlink:href", usaMexicoImageLocation).attr("id", "USA-MX_map");
      d3.select("#mapColourScale").classed("moveMap", true);
      d3.select(".map").classed("moveMap", true);
      highlightMap(defaultOrig, origin);
      colorMap();
    }); // copy button options

    var cButtonOptions = {
      pNode: document.getElementById("copy-button-container"),
      title: i18next.t("CopyButton_Title", {
        ns: "CopyButton"
      }),
      msgCopyConfirm: i18next.t("CopyButton_Confirm", {
        ns: "CopyButton"
      }),
      accessibility: i18next.t("CopyButton_Title", {
        ns: "CopyButton"
      })
    }; // build nodes on copy button

    cButton.build(cButtonOptions);
    d3.select("#mapTitleRail").text(i18next.t("mapTitle", {
      ns: "rail",
      commodity: i18next.t(selectedComm, {
        ns: "rail"
      }),
      geo: i18next.t("map" + selectedOrig, {
        ns: "rail"
      }),
      year: selectedYear
    }));
    d3.select("#symbolLink").html("<a href=".concat(i18next.t("linkURL", {
      ns: "symbolLink"
    }), ">").concat(i18next.t("linkText", {
      ns: "symbolLink"
    }), "</a>"));
    updatePage();
    updateTitles();
    d3.select("#loading-gif").style("visibility", "hidden");
  }

  function buildData(returnData) {
    var _iterator = _createForOfIteratorHelper(returnData),
        _step;

    try {
      for (_iterator.s(); !(_step = _iterator.n()).done;) {
        var item = _step.value;

        if (!data["".concat(item.origin, "_").concat(item.comm)]) {
          var origCommPair = {};
          data["".concat(item.origin, "_").concat(item.comm)] = origCommPair;
        }

        if (!data["".concat(item.origin, "_").concat(item.comm)][item.date]) {
          var date = {};
          data["".concat(item.origin, "_").concat(item.comm)][item.date] = date;
        }

        data["".concat(item.origin, "_").concat(item.comm)][item.date][item.dest] = item.value;
      }
    } catch (err) {
      _iterator.e(err);
    } finally {
      _iterator.f();
    }
  }

  function dateInit(dateFnResult) {
    dateRange.min = minYear;
    dateRange.max = Number(dateFnResult.max);
    dateRange.numPeriods = dateFnResult.numPeriods;
    maxYear = dateFnResult.max;
    selectedYear = maxYear;
    var yearDropdown = $("#yearSelector");

    for (var i = dateRange.min; i <= dateRange.max; i++) {
      yearDropdown.append($("<option></option>").attr("value", i).html(i));
    }

    selectedYear = dateRange.max;
    d3.select("#yearSelector")._groups[0][0].value = selectedYear;
  }

  var timeoutError = Symbol();

  var timeout = function timeout(prom, time, exception) {
    var timer;
    return Promise.race([prom, new Promise(function (_r, rej) {
      return timer = setTimeout(rej, time, exception);
    })])["finally"](function () {
      return clearTimeout(timer);
    });
  }; // Landing page displays


  i18n.load(["src/i18n"], function () {
    settingsBar.x.label = i18next.t("x_label", {
      ns: "railBar"
    }), settingsBar.y.label = i18next.t("y_label", {
      ns: "railBar"
    }), settingsBar.z.label = i18next.t("z_label", {
      ns: "railTable"
    });
    timeout(dateRangeFn(minYear, 1, RailProductId, "1.1.1.0.0.0.0.0.0.0", "year"), timeoutTime, timeoutError).then(function (result) {
      dateInit(result);
      apiCall(maxYear, minYear, defaultOrig).then(function (returnedData) {
        buildData(returnedData);
        pageInitWithData();
      });
    });
  });
  $(document).on("change", uiHandler);
  $(document).on("change", uiHandler);

  d3.selection.prototype.moveToFront = function () {
    return this.each(function () {
      this.parentNode.appendChild(this);
    });
  };

  d3.selection.prototype.moveToBack = function () {
    return this.each(function () {
      var firstChild = this.parentNode.firstChild;

      if (firstChild) {
        this.parentNode.insertBefore(this, firstChild);
      }
    });
  };

}());
