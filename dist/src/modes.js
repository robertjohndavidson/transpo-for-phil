(function () {
  'use strict';

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  function _defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, descriptor.key, descriptor);
    }
  }

  function _createClass(Constructor, protoProps, staticProps) {
    if (protoProps) _defineProperties(Constructor.prototype, protoProps);
    if (staticProps) _defineProperties(Constructor, staticProps);
    return Constructor;
  }

  function _defineProperty(obj, key, value) {
    if (key in obj) {
      Object.defineProperty(obj, key, {
        value: value,
        enumerable: true,
        configurable: true,
        writable: true
      });
    } else {
      obj[key] = value;
    }

    return obj;
  }

  function ownKeys(object, enumerableOnly) {
    var keys = Object.keys(object);

    if (Object.getOwnPropertySymbols) {
      var symbols = Object.getOwnPropertySymbols(object);
      if (enumerableOnly) symbols = symbols.filter(function (sym) {
        return Object.getOwnPropertyDescriptor(object, sym).enumerable;
      });
      keys.push.apply(keys, symbols);
    }

    return keys;
  }

  function _objectSpread2(target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i] != null ? arguments[i] : {};

      if (i % 2) {
        ownKeys(Object(source), true).forEach(function (key) {
          _defineProperty(target, key, source[key]);
        });
      } else if (Object.getOwnPropertyDescriptors) {
        Object.defineProperties(target, Object.getOwnPropertyDescriptors(source));
      } else {
        ownKeys(Object(source)).forEach(function (key) {
          Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key));
        });
      }
    }

    return target;
  }

  function _slicedToArray(arr, i) {
    return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest();
  }

  function _arrayWithHoles(arr) {
    if (Array.isArray(arr)) return arr;
  }

  function _iterableToArrayLimit(arr, i) {
    if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return;
    var _arr = [];
    var _n = true;
    var _d = false;
    var _e = undefined;

    try {
      for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) {
        _arr.push(_s.value);

        if (i && _arr.length === i) break;
      }
    } catch (err) {
      _d = true;
      _e = err;
    } finally {
      try {
        if (!_n && _i["return"] != null) _i["return"]();
      } finally {
        if (_d) throw _e;
      }
    }

    return _arr;
  }

  function _unsupportedIterableToArray(o, minLen) {
    if (!o) return;
    if (typeof o === "string") return _arrayLikeToArray(o, minLen);
    var n = Object.prototype.toString.call(o).slice(8, -1);
    if (n === "Object" && o.constructor) n = o.constructor.name;
    if (n === "Map" || n === "Set") return Array.from(o);
    if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen);
  }

  function _arrayLikeToArray(arr, len) {
    if (len == null || len > arr.length) len = arr.length;

    for (var i = 0, arr2 = new Array(len); i < len; i++) arr2[i] = arr[i];

    return arr2;
  }

  function _nonIterableRest() {
    throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
  }

  function _createForOfIteratorHelper(o, allowArrayLike) {
    var it;

    if (typeof Symbol === "undefined" || o[Symbol.iterator] == null) {
      if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") {
        if (it) o = it;
        var i = 0;

        var F = function () {};

        return {
          s: F,
          n: function () {
            if (i >= o.length) return {
              done: true
            };
            return {
              done: false,
              value: o[i++]
            };
          },
          e: function (e) {
            throw e;
          },
          f: F
        };
      }

      throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
    }

    var normalCompletion = true,
        didErr = false,
        err;
    return {
      s: function () {
        it = o[Symbol.iterator]();
      },
      n: function () {
        var step = it.next();
        normalCompletion = step.done;
        return step;
      },
      e: function (e) {
        didErr = true;
        err = e;
      },
      f: function () {
        try {
          if (!normalCompletion && it.return != null) it.return();
        } finally {
          if (didErr) throw err;
        }
      }
    };
  }

  var defaults = {
    aspectRatio: 16 / 10,
    width: 900,
    margin: {
      top: 30,
      right: 10,
      bottom: 30,
      left: 10
    }
  };
  function makeSankey (svg, settings, data) {
    // set the dimensions and margins of the graph
    var mergedSettings = defaults;
    var outerWidth = mergedSettings.width;
    var outerHeight = Math.ceil(outerWidth / mergedSettings.aspectRatio);
    var innerHeight = mergedSettings.innerHeight = outerHeight - mergedSettings.margin.top - mergedSettings.margin.bottom;
    var innerWidth = mergedSettings.innerWidth = outerWidth - mergedSettings.margin.left - mergedSettings.margin.right;
    var chartInner = svg.select("g.margin-offset");
    var dataLayer = chartInner.select(".data");
    var nonZeroNodes = [];
    data = {
      links: data.links.map(function (d) {
        return _objectSpread2({}, d);
      }).filter(function (d) {
        if (d.value > 0) {
          nonZeroNodes.push(d.source, d.target);
          return true;
        }

        return false;
      }),
      nodes: data.nodes.map(function (d) {
        return _objectSpread2({}, d);
      }).filter(function (d) {
        return nonZeroNodes.includes(d.node);
      })
    };

    function checkHasFourLevels() {
      var thisFlag;
      var hasFourLevels = false;
      var landNodes = ["USres_land", "cdnFromUS_land"];

      var _loop = function _loop(idx) {
        data.nodes.map(function (item) {
          if (item.name === landNodes[idx]) {
            if (item.sourceLinks.length > 0) {
              thisFlag = true;
            }
          }
        });
        hasFourLevels = hasFourLevels || thisFlag;
      };

      for (var idx = 0; idx < landNodes.length; idx++) {
        _loop(idx);
      }

      return hasFourLevels;
    }

    mergedSettings.innerHeight = outerHeight - mergedSettings.margin.top - mergedSettings.margin.bottom;
    var tooltipShiftX = 15; // amount to raise tooltip in y-dirn
    // Set the sankey diagram properties

    var sankey = d3.sankey().size([innerWidth, innerHeight]);
    var path = sankey.link();

    function make() {
      sankey.nodes(data.nodes).links(data.links).layout(32); // tooltip div

      var div = d3.select("body").append("div").attr("class", "tooltip").style("opacity", 0);

      if (dataLayer.empty()) {
        dataLayer = chartInner.append("g").attr("class", "data");
      } // add in the links
      // const link = dataLayer.append("g").selectAll(".link") // use only if dragmove is defined


      dataLayer.append("g").selectAll(".link").data(sankey.links()).enter().append("path").attr("class", "link").attr("d", path).style("stroke-width", function (d) {
        return Math.max(1, d.dy);
      }).sort(function (a, b) {
        return b.dy - a.dy;
      }).on("mousemove", function (d) {
        // Tooltip
        var sourceName = d.source.name;
        div.transition().style("opacity", .9);
        div.html("<b>" + i18next.t(sourceName, {
          ns: "modes"
        }) + "</b>" + "<br><br>" + "<table>" + "<tr>" + "<td><b>" + i18next.t(d.target.name, {
          ns: "modes"
        }) + ": </b></td>" + "<td style='padding: 5px 10px 5px 5px;'>" + settings.formatNum()(d.value) + " " + i18next.t("units", {
          ns: "modes_sankey"
        }) + "</td>" + "</tr>" + "</table>").style("left", d3.event.pageX + tooltipShiftX + "px").style("top", d3.event.pageY + "px");
      }).on("mouseout", function (d) {
        div.transition().style("opacity", 0);
      }); // add in the nodes

      var node = dataLayer.append("g").selectAll(".node").data(sankey.nodes()).enter().append("g").attr("class", function (d) {
        return "node " + d.name; // d.name to fill by class in css
      }).attr("transform", function (d) {
        return "translate(".concat(d.x || 0, ", ").concat(d.y || 0, ")");
      }); // .call(d3.drag()
      //     .subject(function(d) {
      //       return d;
      //     })
      //     .on("start", function() {
      //       this.parentNode.appendChild(this);
      //     })
      //     .on("drag", dragmove));

      node.on("mousemove", function (d) {
        var modeName = d.name.indexOf("other") !== -1 ? "".concat(i18next.t(d.name, {
          ns: "modes"
        }), "<sup>1</sup>") : i18next.t(d.name, {
          ns: "modes"
        });
        div.transition().style("opacity", .9);
        div.html("<b>".concat(modeName, "</b><br><br>\n                <table>\n                  <tr>\n                    <td><b> Total: </b></td>\n                    <td style='padding: 5px 10px 5px 5px;'> ").concat(settings.formatNum()(d.value), " ").concat(i18next.t("units", {
          ns: "modes_sankey"
        }), " </td>\n                    </tr>\n              </table>")).style("left", d3.event.pageX + tooltipShiftX + "px").style("top", d3.event.pageY + "px");
      }).on("mouseout", function (d) {
        div.transition().style("opacity", 0);
      }); // add the rectangles for the nodes

      node.append("rect").attr("height", function (d) {
        return d.dy;
      }).attr("width", sankey.nodeWidth()).style("stroke", function (d) {
        return d3.rgb(d.color).darker(2);
      }).text(function (d) {
        return i18next.t(d.name, {
          ns: "modes"
        }) + "\n" + settings.formatNum()(d.value);
      }); // add in the title for the nodes

      node.append("text").attr("x", function (d) {
        var hasFourLevels = checkHasFourLevels();

        if (d.level === 2 && hasFourLevels === true) {
          return 40;
        } else {
          return -6;
        }
      }).attr("y", function (d) {
        return d.dy / 2;
      }).attr("dy", ".35em").attr("text-anchor", function (d) {
        var hasFourLevels = checkHasFourLevels();

        if (d.level === 2 && hasFourLevels === true) {
          return "start";
        } else {
          return "end";
        }
      }).attr("transform", null).text(function (d) {
        return i18next.t(d.name, {
          ns: "modes"
        });
      }).filter(function (d) {
        return d.x < innerWidth / 2;
      }).attr("x", 6 + sankey.nodeWidth()).attr("text-anchor", "start").call(wrap, 200); // // footnote
      // addFootnote("USres_other");
      // addFootnote("cdnFromUS_other");
      //
      // function addFootnote(name) {
      //   if (d3.select(`.${name}`)) {
      //     d3.select(`.${name}`)
      //         .select("text")
      //         .text(i18next.t("Other", {ns: "modes"}))
      //         .append("tspan")
      //         .text("1")
      //         // .html('<a href= "http://google.com">' + 1 + "</a>")
      //         .style("font-size", "9px")
      //         .attr("dx", ".01em")
      //         .attr("dy", "-.3em");
      //   }
      // }
      // the function for moving the nodes
      // function dragmove(d) {
      //   d3.select(this)
      //       .attr("transform",
      //           "translate("
      //              + d.x + ","
      //              + (d.y = Math.max(
      //                  0, Math.min(innerHeight - d.dy, d3.event.y))
      //              ) + ")");
      //   sankey.relayout();
      //   link.attr("d", path);
      // }

      function wrap(text, width) {
        var xcoord = 40;
        text.each(function () {
          var text = d3.select(this);
          var words = text.text().split(/\s+/).reverse();
          var word;
          var line = [];
          var lineNumber = 0;
          var lineHeight = 1.1; // ems

          var y = text.attr("y");
          var dy = parseFloat(text.attr("dy")) - lineHeight / 2; // added this to shift all lines up

          var tspan = text.text(null).append("tspan").attr("class", "nowrap").attr("x", xcoord).attr("y", y).attr("dy", dy + "em");

          while (word = words.pop()) {
            line.push(word);
            tspan.text(line.join(" "));

            if (tspan.node().getComputedTextLength() > width) {
              line.pop();
              tspan.text(line.join(" "));
              line = [word]; // console.log("dy: ", dy)

              tspan = text.append("tspan").attr("class", "wordwrap").attr("x", xcoord).attr("y", y).attr("dy", function () {
                return ++lineNumber * lineHeight + dy + "em";
              }).text(word);
            }
          }
        });
      }
    } // end make()


    svg.attr("viewBox", "0 0 " + outerWidth + " " + outerHeight).attr("preserveAspectRatio", "xMidYMid meet").attr("role", "img").attr("aria-label", mergedSettings.altText);

    if (chartInner.empty()) {
      chartInner = svg.append("g").attr("class", "margin-offset").attr("transform", "translate(" + mergedSettings.margin.left + "," + mergedSettings.margin.top + ")");
    }

    d3.stcExt.addIEShim(svg, outerHeight, outerWidth);
    make();
  } // end makeSankey()

  var tableSettingsInit = {
    tableTitle: i18next.t("alt", {
      ns: "modes"
    }),
    y: {
      getValue: function getValue(d, key) {
        return d[key]; // Number, not a string, do not pass through i18next
      }
    },
    z: {
      getId: function getId(d) {
        return d.name;
      },
      getKeys: function getKeys(object) {
        var sett = this; // const keys = Object.keys(object[0]);

        var keys = ["name", "value"];

        if (keys.indexOf(sett.y.totalProperty) !== -1) {
          keys.splice(keys.indexOf(sett.y.totalProperty), 1);
        }

        return keys;
      },
      getKeyText: function getKeyText(key) {
        return i18next.t(key, {
          ns: "modes_sankey"
        });
      },
      getText: function getText(d) {
        var fnum = d.name === "USres_other" ? 1 : 2;
        var footnote = "<sup id='fn".concat(fnum, "-rf'><a class='fn-lnk' href='#fn1'><span class='wb-inv'>Footnote </span>1</a></sup>");
        return d.name.indexOf("other") !== -1 ? "".concat(i18next.t(d.name, {
          ns: "modes"
        })).concat(footnote) : i18next.t(d.name, {
          ns: "modes"
        });
      }
    }
  };

  var CopyButton = /*#__PURE__*/function () {
    function CopyButton(pNode, options) {
      _classCallCheck(this, CopyButton);

      this.pNode = pNode ? pNode : document.createElement("div");
      this.options = options ? options : {};
      this.nodes = {};
      this.data = null;
      this.instanceNumber = ++CopyButton.n;
      this["class"] = this.options["class"] || "";
      /* this.data = shall be an array (i.e called rowsArray) of arrays (i.e each is called row).
        each array on rowsArray represents a row on the table.
        this.data must be set/updated by the code that uses this button
        [
          ["title"]
          ["columna1" , "columna2" ,..., "columnaN"]
          ["value1Row1", "value2Row1",..., "valueNRowN"]
          ["value1Row2", "value2Row2",..., "valueNRowN"]
        ]
      */
    }

    _createClass(CopyButton, [{
      key: "build",
      value: function build(options) {
        if (options) this.options = options; // workAround;

        if (options.pNode) this.pNode = options.pNode; // workAround;

        if (options["class"]) this["class"] = options["class"]; // workAround;

        this.root = document.createElement("div");
        this.root.setAttribute("class", "copy-button button-" + this.instanceNumber + " " + this["class"]);
        this.pNode.appendChild(this.root);
        this.nodes.btnCopy = document.createElement("button");
        this.nodes.btnCopy.setAttribute("type", "button");
        this.nodes.btnCopy.setAttribute("class", "btn btn-primary copy button-" + this.instanceNumber + " " + this["class"]);
        this.nodes.btnCopy.setAttribute("title", this.options.title || "");
        this.root.appendChild(this.nodes.btnCopy);
        var icon = document.createElement("span");
        icon.setAttribute("class", "fa fa-clipboard clipboard button-" + this.instanceNumber + " " + this["class"]);
        this.nodes.btnCopy.appendChild(icon);
        var accessibility = document.createElement("span");
        accessibility.setAttribute("class", "wb-inv button-" + this.instanceNumber + " " + this["class"]);
        accessibility.innerHTML = this.options.accessibility || "";
        this.nodes.btnCopy.appendChild(accessibility);
        this.nodes.msgCopyConfirm = document.createElement("div");
        this.nodes.msgCopyConfirm.setAttribute("class", "copy-confirm button-" + this.instanceNumber + " " + this["class"]);
        this.nodes.msgCopyConfirm.setAttribute("aria-live", "polite");
        this.nodes.msgCopyConfirm.innerHTML = this.options.msgCopyConfirm || "";
        this.root.appendChild(this.nodes.msgCopyConfirm);
        this.nodes.btnCopy.addEventListener("click", this.onBtnClick.bind(this));
      }
    }, {
      key: "onBtnClick",
      value: function onBtnClick(ev) {
        this.copyData(this.data);
      }
    }, {
      key: "copyData",
      value: function copyData(lines) {
        var linesTemp = lines ? lines : [];
        this.clipboard(this.toCSV("\t", linesTemp), this.root);
        this.fade(this.nodes.msgCopyConfirm, true);
        setTimeout(function (ev) {
          this.fade(this.nodes.msgCopyConfirm, false);
        }.bind(this), 1500);
      }
    }, {
      key: "toCSV",
      value: function toCSV(separator, lines) {
        var csv = lines.map(function (line) {
          return line.join(separator);
        });
        return csv.join("\r\n");
      }
    }, {
      key: "clipboard",
      value: function clipboard(string, target) {
        if (this.isIE()) window.clipboardData.setData("Text", string);else {
          // Copying the string
          var aux = document.createElement("textarea");
          aux.textContent = string;
          target.appendChild(aux);
          aux.select();
          document.execCommand("copy");
          target.removeChild(aux);
        }
      }
    }, {
      key: "fade",
      value: function fade(node, visible) {
        var clss = ["copy-confirm button-" + this.instanceNumber + " " + this["class"]];
        var add = visible ? "fadeIn" : "fadeOut";
        clss.push(add);
        node.setAttribute("class", clss.join(" "));
      } // work around for when tables are destroyed

    }, {
      key: "appendTo",
      value: function appendTo(pNode) {
        this.pNode = pNode;
        this.pNode.appendChild(this.root);
        this.nodes.msgCopyConfirm.setAttribute("class", "copy-confirm button-" + this.instanceNumber + " " + this["class"]);
      }
    }, {
      key: "isIE",
      value: function isIE() {
        var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE ");

        if (msie > 0) {
          // IE 10 or older => return version number
          return parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)), 10);
        }

        var trident = ua.indexOf("Trident/");

        if (trident > 0) {
          // IE 11 => return version number
          var rv = ua.indexOf("rv:");
          return parseInt(ua.substring(rv + 3, ua.indexOf(".", rv)), 10);
        }

        var edge = ua.indexOf("Edge/");

        if (edge > 0) {
          // Edge (IE 12+) => return version number
          return parseInt(ua.substring(edge + 5, ua.indexOf(".", edge)), 10);
        } // other browser


        return false;
      }
    }]);

    return CopyButton;
  }();
  CopyButton.n = 0;

  var NodesTree = /*#__PURE__*/function () {
    function NodesTree() {
      _classCallCheck(this, NodesTree);

      this.tree = this.buildTree();
      this.data = null;
    }

    _createClass(NodesTree, [{
      key: "setData",
      value: function setData(data) {
        this.data = data;
        this.iterTree(this.tree, addData);
        this.tree.value = 0;
        this.tree.children.forEach(function (child) {
          this.tree.value += child.value;
        }.bind(this));

        function addData(tree) {
          var node = data.find(function (dataNode) {
            return dataNode.target == tree.target;
          });
          tree.value = node != undefined ? node.value : 0;
        }
      }
    }, {
      key: "iterTree",
      value: function iterTree(tree, fn) {
        var _this = this;

        fn(tree);

        if (tree.children.length > 0) {
          tree.children.forEach(function (branch) {
            return _this.iterTree(branch, fn);
          });
        }
      }
    }, {
      key: "toArray",
      value: function toArray() {
        var treeAsArray = [];
        this.iterTree(this.tree, fillArray);
        return treeAsArray;

        function fillArray(tree) {
          treeAsArray.push(cloneItem(tree));
        }

        function cloneItem(item) {
          var itemTemp = {};

          for (var _i = 0, _Object$entries = Object.entries(item); _i < _Object$entries.length; _i++) {
            var _Object$entries$_i = _slicedToArray(_Object$entries[_i], 1),
                key = _Object$entries$_i[0];

            itemTemp[key] = item[key];
          }

          return itemTemp;
        }
      }
    }, {
      key: "toLines",
      value: function toLines(title, columns) {
        var lines = [];
        var titleRow = [title];
        var columsRow = columns;
        var data = this.toArray();
        lines.push(titleRow, [], columsRow);
        data.forEach(function (item) {
          lines.push([i18next.t(item.name, {
            ns: "modes"
          }), item.value]);
        });
        return lines;
      }
    }, {
      key: "buildTree",
      value: function buildTree() {
        var tree = [{
          "node": 0,
          "target": 0,
          "level": 0,
          "value": null,
          "name": "intl",
          "children": [{
            "node": 1,
            "target": 1,
            "level": 1,
            "value": null,
            "name": "USres",
            "children": [{
              "node": 5,
              "target": 5,
              "level": 2,
              "value": null,
              "name": "USres_air",
              "children": []
            }, {
              "node": 6,
              "target": 6,
              "level": 2,
              "value": null,
              "name": "USres_marine",
              "children": []
            }, {
              "node": 7,
              "target": 7,
              "level": 2,
              "value": null,
              "name": "USres_land",
              "children": [{
                "node": 17,
                "target": 17,
                "level": 3,
                "value": null,
                "name": "USres_car",
                "children": []
              }, {
                "node": 18,
                "target": 18,
                "level": 3,
                "value": null,
                "name": "USres_bus",
                "children": []
              }, {
                "node": 19,
                "target": 19,
                "level": 3,
                "value": null,
                "name": "USres_train",
                "children": []
              }, {
                "node": 20,
                "target": 20,
                "level": 3,
                "value": null,
                "name": "USres_other",
                "children": []
              }]
            }]
          }, {
            "node": 2,
            "target": 2,
            "level": 1,
            "value": null,
            "name": "nonUSres",
            "children": [{
              "node": 8,
              "target": 8,
              "level": 2,
              "value": null,
              "name": "nonUSres_air",
              "children": []
            }, {
              "node": 9,
              "target": 9,
              "level": 2,
              "value": null,
              "name": "nonUSres_marine",
              "children": []
            }, {
              "node": 10,
              "target": 10,
              "level": 2,
              "value": null,
              "name": "nonUSres_land",
              "children": []
            }]
          }, {
            "node": 3,
            "target": 3,
            "level": 1,
            "value": null,
            "name": "cdnFromUS",
            "children": [{
              "node": 11,
              "target": 11,
              "level": 2,
              "value": null,
              "name": "cdnFromUS_air",
              "children": []
            }, {
              "node": 12,
              "target": 12,
              "level": 2,
              "value": null,
              "name": "cdnFromUS_marine",
              "children": []
            }, {
              "node": 13,
              "target": 13,
              "level": 2,
              "value": null,
              "name": "cdnFromUS_land",
              "children": [{
                "node": 21,
                "target": 21,
                "level": 3,
                "value": null,
                "name": "cdnFromUS_car",
                "children": []
              }, {
                "node": 22,
                "target": 22,
                "level": 3,
                "value": null,
                "name": "cdnFromUS_bus",
                "children": []
              }, {
                "node": 23,
                "target": 23,
                "level": 3,
                "value": null,
                "name": "cdnFromUS_train",
                "children": []
              }, {
                "node": 24,
                "target": 24,
                "level": 3,
                "value": null,
                "name": "cdnFromUS_other",
                "children": []
              }]
            }]
          }, {
            "node": 4,
            "target": 4,
            "level": 1,
            "value": null,
            "name": "cdnFromOther",
            "children": [{
              "node": 14,
              "target": 14,
              "level": 2,
              "value": null,
              "name": "cdnFromOther_air",
              "children": []
            }, {
              "node": 15,
              "target": 15,
              "level": 2,
              "value": null,
              "name": "cdnFromOther_marine",
              "children": []
            }, {
              "node": 16,
              "target": 16,
              "level": 2,
              "value": null,
              "name": "cdnFromOther_land",
              "children": []
            }]
          }]
        }];
        return tree[0]; // returns root of tree
      }
    }]);

    return NodesTree;
  }();

  function dropdownCheck (yearId, monthId, dateRange, selectedYear, selectedMonth, months) {
    var yearDropdown = $(yearId); // date dropdown creation

    yearDropdown.empty();

    if (months) {
      for (var i = Number(dateRange.min.substring(0, 4)); i <= Number(dateRange.max.substring(0, 4)); i++) {
        yearDropdown.append($("<option></option>").attr("value", i).html(i));
      }

      d3.select(yearId)._groups[0][0].value = selectedYear;
      var maxMonth = Number(dateRange.max.substring(5, 7));
      var maxYear = Number(dateRange.max.substring(0, 4)); // Disable months in dropdown menu that do not exist for selectedYear

      if (Number(selectedYear) === maxYear) {
        $("".concat(monthId, " > option")).each(function () {
          if (Number(this.value) > maxMonth) {
            this.disabled = true;
          }
        });
        var currentMonth = Number(d3.select(monthId)._groups[0][0].value);

        if (currentMonth > maxMonth) {
          selectedMonth = dateRange.max.substring(5, 7);
          d3.select(monthId)._groups[0][0].value = selectedMonth;
        }
      } else {
        // Enable all months
        d3.selectAll("".concat(monthId, " > option")).property("disabled", false);
      }
    } else {
      for (var _i = Number(dateRange.min); _i <= Number(dateRange.max); _i++) {
        yearDropdown.append($("<option></option>").attr("value", _i).html(_i));
      }

      d3.select(yearId)._groups[0][0].value = selectedYear;
    }

    return selectedMonth;
  }

  var webAPI = "https://www150.statcan.gc.ca/t1/wds/rest/getDataFromCubePidCoordAndLatestNPeriods";
  function dateRangeFn (minYear, frequency, productID, defaultCoord, granularity) {
    return new Promise(function (resolve, reject) {
      var mostRecentDate;
      var numberOfPeriods;
      var returnObject = {};
      var myData = [{
        "productId": productID,
        "coordinate": defaultCoord,
        "latestN": 1
      }];
      $.ajax({
        type: "post",
        url: webAPI,
        data: JSON.stringify(myData),
        dataType: "json",
        contentType: "application/json",
        success: function success(data, textStatus, jQxhr) {
          if (granularity === "year") {
            mostRecentDate = data[0].object.vectorDataPoint[0].refPer.substring(0, 4);
            numberOfPeriods = (Number(mostRecentDate) - minYear + 1) * frequency;
            returnObject.max = mostRecentDate;
            returnObject.numPeriods = numberOfPeriods;
          } else if (granularity === "month") {
            mostRecentDate = data[0].object.vectorDataPoint[0].refPer.substring(0, 7);
            numberOfPeriods = (Number(mostRecentDate.substring(0, 4)) - minYear) * frequency + Number(mostRecentDate.substring(5, 7));
            returnObject.max = mostRecentDate;
            returnObject.numPeriods = numberOfPeriods;
          }

          resolve(returnObject);
        },
        error: function error(jqXhr, textStatus, errorThrown) {
          reject(errorThrown);
        }
      });
    });
  }

  var ProductId = 24100041;
  var webAPI$1 = "https://www150.statcan.gc.ca/t1/wds/rest/getDataFromCubePidCoordAndLatestNPeriods";
  var namesToNode = {
    "intl": 0,
    "USres": 1,
    "nonUSres": 2,
    "cdnFromUS": 3,
    "cdnFromOther": 4,
    "USres_air": 5,
    "USres_marine": 6,
    "USres_land": 7,
    "nonUSres_air": 8,
    "nonUSres_marine": 9,
    "nonUSres_land": 10,
    "cdnFromUS_air": 11,
    "cdnFromUS_marine": 12,
    "cdnFromUS_land": 13,
    "cdnFromOther_air": 14,
    "cdnFromOther_marine": 15,
    "cdnFromOther_land": 16,
    "USres_car": 17,
    "USres_bus": 18,
    "USres_train": 19,
    "USres_other": 20,
    "cdnFromUS_car": 21,
    "cdnFromUS_bus": 22,
    "cdnFromUS_train": 23,
    "cdnFromUS_other": 24
  };
  var namesToSource = {
    "USres": 0,
    "nonUSres": 0,
    "cdnFromUS": 0,
    "cdnFromOther": 0,
    "USres_air": 1,
    "USres_marine": 1,
    "USres_land": 1,
    "nonUSres_air": 2,
    "nonUSres_marine": 2,
    "nonUSres_land": 2,
    "cdnFromUS_air": 3,
    "cdnFromUS_marine": 3,
    "cdnFromUS_land": 3,
    "cdnFromOther_air": 4,
    "cdnFromOther_marine": 4,
    "cdnFromOther_land": 4,
    "USres_car": 7,
    "USres_bus": 7,
    "USres_train": 7,
    "USres_other": 7,
    "cdnFromUS_car": 13,
    "cdnFromUS_bus": 13,
    "cdnFromUS_train": 13,
    "cdnFromUS_other": 13
  };
  var provinceToNum = {
    "CANADA": 1,
    "NL": 2,
    "PE": 14,
    "NS": 17,
    "NB": 28,
    "QC": 54,
    "ON": 113,
    "MB": 175,
    "SK": 195,
    "AB": 213,
    "BC": 224,
    "YK": 260,
    "NU": 266
  };
  var numToProvince = {
    1: "CANADA",
    2: "NL",
    14: "PE",
    17: "NS",
    28: "NB",
    54: "QC",
    113: "ON",
    175: "MB",
    195: "SK",
    213: "AB",
    224: "BC",
    260: "YK",
    266: "NU"
  };
  var numToChar = {
    1: "intl",
    3: "USres",
    4: "USres_car",
    15: "USres_train",
    18: "USres_bus",
    28: "USres_other",
    8: "USres_air",
    21: "USres_marine",
    35: "nonUSres",
    36: "nonUSres_land",
    39: "nonUSres_air",
    42: "nonUSres_marine",
    46: "cdnFromUS",
    47: "cdnFromUS_car",
    58: "cdnFromUS_train",
    61: "cdnFromUS_bus",
    71: "cdnFromUS_other",
    51: "cdnFromUS_air",
    64: "cdnFromUS_marine",
    78: "cdnFromOther",
    79: "cdnFromOther_land",
    80: "cdnFromOther_air",
    83: "cdnFromOther_marine"
  };
  var statusCodes = {
    1: "..",
    2: "0s",
    3: "A",
    4: "B",
    5: "C",
    6: "D",
    7: "E",
    8: "F"
  };
  var qi_F = 8;
  function apiCall (numPeriods, region) {
    return new Promise(function (resolve, reject) {
      // get coordinates for data
      var coordinateArray = coordinateTranslate(provinceToNum[region]);
      var yearRange = numPeriods;
      var returnObject;
      var myData = [];

      for (var i = 0; i < coordinateArray.length; i++) {
        myData.push({
          "productId": ProductId,
          "coordinate": coordinateArray[i],
          "latestN": yearRange
        });
      }

      $.support.cors = true;
      $.ajax({
        type: "post",
        url: webAPI$1,
        data: JSON.stringify(myData),
        dataType: "json",
        contentType: "application/json",
        success: function success(data, textStatus, jQxhr) {
          returnObject = buildSankeyNodes(data, yearRange);
          resolve(returnObject);
        },
        error: function error(jqXhr, textStatus, errorThrown) {
          reject(errorThrown);
        }
      });
    });
  }

  function coordinateTranslate(province) {
    var returnArray = [];

    for (var j in numToChar) {
      returnArray.push("".concat(province, ".").concat(j, ".0.0.0.0.0.0.0.0"));
    }

    return returnArray;
  }

  function buildSankeyNodes(data, yearRange) {
    var output = {};

    var _iterator = _createForOfIteratorHelper(data),
        _step;

    try {
      for (_iterator.s(); !(_step = _iterator.n()).done;) {
        var entry = _step.value;

        for (var year = 0; year < yearRange; year++) {
          var item = {};
          var datapoint = entry.object.vectorDataPoint[year];
          var returnValue = void 0;

          if (datapoint.statusCode != 1 && datapoint.securityLevelCode == 0 && datapoint.statusCode != qi_F) {
            returnValue = datapoint.value;
          } else if (datapoint.statusCode == "1") {
            returnValue = 0;
          } else {
            returnValue = statusCodes[datapoint.statusCode];
          }

          item.value = returnValue;
          var date = datapoint.refPer.substring(0, 7);
          var geo = numToProvince[entry.object.coordinate.split(".", 2)[0]];
          var characteristic = numToChar[entry.object.coordinate.split(".", 2)[1]];
          item.target = namesToNode[characteristic];
          item.source = namesToSource[characteristic];

          if (item.target !== 0) {
            if (output.hasOwnProperty(date)) {
              if (output[date].hasOwnProperty(geo)) {
                if (item.source === namesToNode["USres_land"]) {
                  if (output[date][geo].hasOwnProperty(namesToNode["USres_land"]) && !isNaN(Number(item.value))) {
                    if (isNaN(output[date][geo][namesToNode["USres_land"]].value)) {
                      output[date][geo][namesToNode["USres_land"]].value = item.value;
                    } else {
                      output[date][geo][namesToNode["USres_land"]].value += item.value;
                    }
                  } else {
                    output[date][geo][namesToNode["USres_land"]] = {};
                    output[date][geo][namesToNode["USres_land"]].target = namesToNode["USres_land"];
                    output[date][geo][namesToNode["USres_land"]].source = namesToNode["USres"];
                    output[date][geo][namesToNode["USres_land"]].value = item.value;
                  }

                  output[date][geo][item.target] = item;
                }

                if (item.source === namesToNode["cdnFromUS_land"]) {
                  if (output[date][geo].hasOwnProperty(namesToNode["cdnFromUS_land"]) && !isNaN(Number(item.value))) {
                    if (isNaN(output[date][geo][namesToNode["cdnFromUS_land"]].value)) {
                      output[date][geo][namesToNode["cdnFromUS_land"]].value = item.value;
                    } else {
                      output[date][geo][namesToNode["cdnFromUS_land"]].value += item.value;
                    }
                  } else {
                    output[date][geo][namesToNode["cdnFromUS_land"]] = {};
                    output[date][geo][namesToNode["cdnFromUS_land"]].target = namesToNode["cdnFromUS_land"];
                    output[date][geo][namesToNode["cdnFromUS_land"]].source = namesToNode["cdnFromUS"];
                    output[date][geo][namesToNode["cdnFromUS_land"]].value = item.value;
                  }

                  output[date][geo][item.target] = item;
                }

                output[date][geo][item.target] = item;
              } else {
                output[date][geo] = {};
                output[date][geo][item.target] = item;
              }
            } else {
              output[date] = {};
              output[date][geo] = {};
              output[date][geo][item.target] = item;
            }
          }
        }
      }
    } catch (err) {
      _iterator.e(err);
    } finally {
      _iterator.f();
    }

    for (var date in output) {
      for (var geo in output[date]) {
        output[date][geo] = Object.values(output[date][geo]);
      }

      output[date]["nodes"] = [{
        "node": 0,
        "name": "intl"
      }, {
        "node": 1,
        "name": "USres"
      }, {
        "node": 2,
        "name": "nonUSres"
      }, {
        "node": 3,
        "name": "cdnFromUS"
      }, {
        "node": 4,
        "name": "cdnFromOther"
      }, {
        "node": 5,
        "name": "USres_air"
      }, {
        "node": 6,
        "name": "USres_marine"
      }, {
        "node": 7,
        "name": "USres_land"
      }, {
        "node": 8,
        "name": "nonUSres_air"
      }, {
        "node": 9,
        "name": "nonUSres_marine"
      }, {
        "node": 10,
        "name": "nonUSres_land"
      }, {
        "node": 11,
        "name": "cdnFromUS_air"
      }, {
        "node": 12,
        "name": "cdnFromUS_marine"
      }, {
        "node": 13,
        "name": "cdnFromUS_land"
      }, {
        "node": 14,
        "name": "cdnFromOther_air"
      }, {
        "node": 15,
        "name": "cdnFromOther_marine"
      }, {
        "node": 16,
        "name": "cdnFromOther_land"
      }, {
        "node": 17,
        "name": "USres_car"
      }, {
        "node": 18,
        "name": "USres_bus"
      }, {
        "node": 19,
        "name": "USres_train"
      }, {
        "node": 20,
        "name": "USres_other"
      }, {
        "node": 21,
        "name": "cdnFromUS_car"
      }, {
        "node": 22,
        "name": "cdnFromUS_bus"
      }, {
        "node": 23,
        "name": "cdnFromUS_train"
      }, {
        "node": 24,
        "name": "cdnFromUS_other"
      }];
    }

    return output;
  }

  var TravellersProductId = 24100041;
  var timeoutTime = 3000;
  /* Copy Button and DataTree*/
  // -----------------------------------------------------------------------------

  var cButton = new CopyButton();
  var dataTree = new NodesTree(); // -----------------------------------------------------------------------------
  // Add number formatter to stackedArea settings file

  var thisLang = document.getElementsByTagName("html")[0].getAttribute("lang");
  var settingsAux = {
    formatNum: function formatNum() {
      var formatNumber;

      if (thisLang === "fr") {
        var locale = d3.formatLocale({
          decimal: ",",
          thousands: " ",
          grouping: [3]
        });
        formatNumber = locale.format(",d");
      } else {
        formatNumber = d3.format(",d");
      }

      var format = function format(d) {
        if (Number(d)) {
          return formatNumber(d);
        } else {
          return d;
        }
      };

      return format;
    }
  };

  var tableSettings = _objectSpread2(_objectSpread2({}, tableSettingsInit), settingsAux); // -----------------------------------------------------------------------------


  var selectedRegion = "CANADA";
  var selectedMonth;
  var selectedYear;
  var data = {};
  var minYear = 2010;
  var dateRange = {}; // global used on sankey

  var sankeyNodes = dataTree.toArray(); // ------------
  // SVGs

  var sankeyChart = d3.select("#sankeyGraph").append("svg").attr("id", "svg_sankeyChart");
  var table = d3.select(".tabledata");

  var loadData = function loadData(selectedYear, selectedMonth, cb) {
    if (!data[selectedYear + "-" + selectedMonth][selectedRegion]) {
      apiCall(dateRange.numPeriods, selectedRegion).then(function (apiData) {
        for (var date in data) {
          data[date][selectedRegion] = apiData[date][selectedRegion];
        }

        cb();
      });
    } else {
      cb();
    }
  };

  function uiHandler(event) {
    // clear any tooltips
    d3.selectAll(".tooltip").style("opacity", 0);

    if (event.target.id === "groups" || event.target.id === "month" || event.target.id === "year") {
      selectedRegion = document.getElementById("groups").value;
      selectedMonth = document.getElementById("month").value;
      selectedYear = document.getElementById("year").value; // clear any zeroFlag message

      if (d3.select("#zeroFlag").text() !== "") d3.select("#zeroFlag").text("");
      loadData(selectedYear, selectedMonth, function () {
        createDropdown();
        showData();
      });
    }
  }

  function showData() {
    var thisMonth = i18next.t(selectedMonth, {
      ns: "months"
    });
    var thisRegion = i18next.t(selectedRegion, {
      ns: "modesTable"
    });
    var thisData = data[selectedYear + "-" + selectedMonth][selectedRegion]; // Check that the sum of all nodes is not zero

    var travellerTotal = function travellerTotal() {
      return thisData.map(function (item) {
        return item.value;
      }).reduce(function (prev, next) {
        return prev + next;
      });
    };

    if (travellerTotal() === 0) {
      d3.selectAll("svg > *").remove();
      d3.select("#zeroFlag").text("".concat(i18next.t("noData", {
        ns: "modes_sankey"
      }), " ").concat(thisRegion, ",\n          ").concat(thisMonth, " ").concat(selectedYear));
    } else {
      d3.selectAll("svg > *").remove();
      makeSankey(sankeyChart, settingsAux, {
        nodes: sankeyNodes,
        links: data[selectedYear + "-" + selectedMonth][selectedRegion]
      });
    }

    var dataTable = data[selectedYear + "-" + selectedMonth][selectedRegion];
    dataTree.setData(dataTable);
    var auxArray = dataTree.toArray();
    auxArray.forEach(function (item) {});
    drawTable(table, tableSettings, auxArray);
    $(".wb-fnote").trigger("wb-init.wb-fnote");
    d3.select("#symbolLink").html("<a href=".concat(i18next.t("linkURL", {
      ns: "symbolLink"
    }), ">").concat(i18next.t("linkText", {
      ns: "symbolLink"
    }), "</a>"));
    updateTitles(); // ------------------copy button---------------------------------

    cButton.appendTo(document.getElementById("copy-button-container"));
    dataCopyButton(); // ---------------------------------------------------------------
  }
  /* -- update table title -- */


  function updateTitles() {
    var thisGeo = i18next.t(selectedRegion, {
      ns: "geography"
    });
    var thisMonth = i18next.t(selectedMonth, {
      ns: "months"
    });
    var thisTitle = i18next.t("tableTitle", {
      ns: "modesTable"
    }) + " " + thisGeo + ", " + thisMonth + " " + selectedYear + ", " + i18next.t("byType", {
      ns: "modesTable"
    });
    d3.select("#only-dt-tbl").text(thisTitle);
    d3.select("#table-caption").text(thisTitle);
  } // create year dropdown based on data


  function createDropdown() {
    var yearId = "#".concat("year");
    var monthId = "#".concat("month");
    selectedMonth = dropdownCheck(yearId, monthId, dateRange, selectedYear, selectedMonth, true);
  } // -----------------------------------------------------------------------------

  /* Copy Button*/


  function dataCopyButton() {
    var geo = i18next.t(selectedRegion, {
      ns: "modesTable"
    });
    var month = i18next.t(selectedMonth, {
      ns: "months"
    });
    var title = i18next.t("tableTitle", {
      ns: "modesTable"
    }) + geo + ", " + month + " " + selectedYear + ", " + i18next.t("byType", {
      ns: "modesTable"
    });
    var columns = [i18next.t("name", {
      ns: "modes_sankey"
    }), i18next.t("value", {
      ns: "modes_sankey"
    })];
    cButton.data = dataTree.toLines(title, columns);
  } // -----------------------------------------------------------------------------

  /* Initial page load */


  function pageInitWithData() {
    createDropdown();
    d3.select("#year")._groups[0][0].value = selectedYear;
    d3.select("#month")._groups[0][0].value = selectedMonth; // copy button options

    var cButtonOptions = {
      pNode: document.getElementById("copy-button-container"),
      title: i18next.t("CopyButton_Title", {
        ns: "CopyButton"
      }),
      msgCopyConfirm: i18next.t("CopyButton_Confirm", {
        ns: "CopyButton"
      }),
      accessibility: i18next.t("CopyButton_Title", {
        ns: "CopyButton"
      })
    }; // build nodes on copy button

    cButton.build(cButtonOptions);
    d3.select("#loading-gif").style("visibility", "hidden");
    showData();
  }

  function dateInit(dateFnResult) {
    dateRange.min = "2010-01";
    dateRange.max = dateFnResult.max;
    dateRange.numPeriods = dateFnResult.numPeriods;
    selectedYear = dateRange.max.substring(0, 4);
    selectedMonth = dateRange.max.substring(5, 7);
    var yearDropdown = $("#year");

    for (var i = minYear; i <= Number(dateRange.max.substring(0, 4)); i++) {
      yearDropdown.append($("<option></option>").attr("value", i).html(i));
    }

    d3.select("#year")._groups[0][0].value = Number(selectedYear);
  }

  var timeoutError = Symbol();

  var timeout = function timeout(prom, time, exception) {
    var timer;
    return Promise.race([prom, new Promise(function (_r, rej) {
      return timer = setTimeout(rej, time, exception);
    })])["finally"](function () {
      return clearTimeout(timer);
    });
  };

  i18n.load(["src/i18n"], function () {
    timeout(dateRangeFn(minYear, 12, TravellersProductId, "1.1.0.0.0.0.0.0.0.0", "month"), timeoutTime, timeoutError).then(function (result) {
      dateInit(result);
      apiCall(dateRange.numPeriods, selectedRegion).then(function (apiData) {
        data = apiData;
        pageInitWithData();
      });
    });
  });
  $(document).on("change", uiHandler);

}());
